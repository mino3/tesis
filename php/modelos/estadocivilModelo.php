<?php 

if ($peticionAjax) {
		# code...
		require_once "../core/mainModel.php";

	}else{
		require_once "./core/mainModel.php";
	}
	/**
	 * 
	 */
	class estadocivilModelo extends mainModel
	{
		protected function agregar_estadocivil_modelo($datos){
			$sql=mainModel::conectar()->prepare('INSERT INTO estadocivil(esci_id,esci_codigo,esci_nombre,esci_estado,esci_adminfecha)VALUES (:id,:codigo,:nombre,:estado,:adminfecha)');
			$sql->bindParam(':id',$datos['id']);
			$sql->bindParam(':codigo',$datos['codigo']);
			$sql->bindParam(':nombre',$datos['nombre']);
			$sql->bindParam(':estado',$datos['estado']);
			$sql->bindParam(':adminfecha',$datos['adminfecha']);

			$sql->execute();

			return $sql;

		}
		protected function eliminar_estadocivil_modelo($codigo){
			$query=self::conectar()->prepare("UPDATE estadocivil SET esci_estado='0' WHERE esci_codigo=:Codigo");
			$query->bindParam(':Codigo',$codigo);
			$query->execute();
			return $query;
		}	
		protected function datos_estadocivil_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM estadocivil WHERE esci_codigo=:Codigo");
				$query->bindParam(":Codigo",$codigo);				 				
			}elseif($tipo=="Conteo"){
				$query=mainModel::conectar()->prepare("SELECT esci_id FROM estadocivil");
			}
			$query->execute();
			return $query;

		}
		protected function actualizar_estadocivil_modelo($datos){
			$query=mainModel::conectar()->prepare("UPDATE estadocivil SET esci_nombre=:Nombre,esci_estado=:Estado WHERE esci_codigo=:Codigo");
			$query->bindParam(":Nombre",$datos['Nombre']);		
			$query->bindParam(":Estado",$datos['Estado']);	
			$query->bindParam(":Codigo",$datos['Codigo']);			
			$query->execute();
			return $query;	

		}	
	}