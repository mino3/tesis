<?php 

if ($peticionAjax) {
		# code...
		require_once "../core/mainModel.php";

	}else{
		require_once "./core/mainModel.php";
	}
/**
	 * 
	 */
	class becaModelo extends mainModel
	{
	protected function agregar_beca_modelo($datos){
	  $sql=mainModel::conectar()->prepare('INSERT INTO beca(be_id,be_codigo,be_nombre,be_descripcion,be_monto,be_estado,be_adminfecha) VALUES(:ID,:Codigo,:Nombre,:Descripcion,:Monto,:Estado,:AdminFecha)');
			$sql->bindParam(':ID',$datos['ID']);
			$sql->bindParam(':Codigo',$datos['Codigo']);
			$sql->bindParam(':Nombre',$datos['Nombre']);
			$sql->bindParam(':Descripcion',$datos['Descripcion']);
			$sql->bindParam(':Monto',$datos['Monto']);
			$sql->bindParam(':Estado',$datos['Estado']);
			$sql->bindParam(':AdminFecha',$datos['AdminFecha']);
			$sql->execute();
				return $sql;
		}
	protected function eliminar_beca_modelo($codigo){
			$query=self::conectar()->prepare("UPDATE beca SET be_estado='0' WHERE be_codigo=:Codigo");
			$query->bindParam(':Codigo',$codigo);
			$query->execute();
			return $query;
		}
	protected function datos_beca_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM beca WHERE be_codigo=:Codigo");
				$query->bindParam(":Codigo",$codigo);				 				
			}elseif($tipo=="Conteo"){
				$query=mainModel::conectar()->prepare("SELECT be_id FROM beca");
			}
			$query->execute();
			return $query;
		}
	protected function actualizar_beca_modelo($datos){
		$sql=mainModel::conectar()->prepare('UPDATE beca SET be_nombre=:Nombre,be_descripcion=:Descripcion,be_monto=:Monto,be_estado=:Estado WHERE be_codigo=:Codigo');	    
		$sql->bindParam(':Nombre',$datos['Nombre']);
		$sql->bindParam(':Descripcion',$datos['Descripcion']);
		$sql->bindParam(':Monto',$datos['Monto']);
		$sql->bindParam(':Estado',$datos['Estado']);
		$sql->bindParam(':Codigo',$datos['Codigo']);
		$sql->execute();
		return $sql;
		}

	}	