<?php 
	/**2
	 * 
	 */
	class vistasModelo
	{
		protected function obtener_vistas_modelo($vistas){
			$listaBlanca=["admin","adminlist","estudiante","home","login","periodo","profesor","salon","idioma","adminserach","cuenta","cuentalist","cuentasearch","idiomalist","idiomasearch","idiomaupdate",
			    "etnia","etnialist","etniasearch","etniaUp",
			    "discapacidad","discapacidadlist","discapacidadsearch","discapacidadUp",
		        "estadocivil","estadocivillist","estadocivilsearch","estadocivilUP",
		        "tiposangre","tiposangrelist","tiposangresearch","tiposangreUP",
		        "nacionalidad","nacionalidadlist","nacionalidadsearch","nacionUp",
		        "canton","cantonlist","cantonsearch","cantonUp",
		        "provincia","provincialist","provinciasearch","provUp",
		        "persona","personalist","personasearch",
		    		"mydata","myaccount",
		    	"jornada","jornadalist","jornadasearch","jornadaUp","jornadaUp",
		     	"beca","becalist","becasearch","becaUp",
		        "tipocolegio","tipocolegiolist","tipocolegiosearch","tipocolegioUp",
		         "tipobachillerato","tipobachilleratolist","tipobachilleratosearch","tipobachilleratoUp",
		        "familia","familialist","familiasearch","familiaUp","direccionUp","tipofamUp",
		        "sectoreconomico","sectoreconomicolist","sectoreconomicosearch","sectoreconomicoUp",
		        "tipofamilia","tipofamilialist","tipofamiliasearch","tipofamiliaUp",
		    	"modalidad","modalidadlist","modalidadsearch","modalidadUp",
		     "nivelestudio","nivelestudiolist","nivelestudiosearch","nivelestudioUp",
		 	"alumno","alumnolist","alumnosearch"];
			if (in_array($vistas, $listaBlanca)) {
				# code...
				if (is_file("./vistas/contenidos/".$vistas."-view.php")) {
					# code...
					$contenido="./vistas/contenidos/".$vistas."-view.php";
				}else{
					$contenido="login";
				}

			}elseif($vistas=="login"){
				$contenido="login";

			}elseif ($vistas=="index") {
				# code...
				$contenido="login";
			}else{
				$contenido="404";
			}
			return $contenido;
		}

	}
