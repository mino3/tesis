<?php 

if ($peticionAjax) {
		# code...
		require_once "../core/mainModel.php";

	}else{
		require_once "./core/mainModel.php";
	}

	/**
	 * 
	 */
	class discapacidadModelo extends mainModel
	{
		protected function agregar_discapacidad_modelo($datos){
			$sql=mainModel::conectar()->prepare('INSERT INTO discapacidad(dis_id,dis_codigo,dis_nombre,dis_descripcion,dis_carnetconadis,dis_estado,dis_adminfecha1,Porcetaje_por_id) VALUES(:id,:codigo,:nombre,:descripcion,:carnet,:estado,:adminfecha1,:Porcentaje)');
			   $sql->bindParam(':id',$datos['id']);
			   $sql->bindParam(':codigo',$datos['codigo']);
			   $sql->bindParam(':nombre',$datos['nombre']);
			   $sql->bindParam(':descripcion',$datos['descripcion']);
			   $sql->bindParam(':carnet',$datos['carnet']);
			   $sql->bindParam(':estado',$datos['estado']);
			   $sql->bindParam(':adminfecha1',$datos['adminfecha1']);
			   $sql->bindParam(':Porcentaje',$datos['Porcentaje']);
			   $sql->execute();
			   return $sql;
		}
		protected function eliminar_discapacidad_modelo($codigo){
			$query=self::conectar()->prepare("UPDATE discapacidad SET dis_estado='0' WHERE dis_codigo=:Codigo");
			$query->bindParam(':Codigo',$codigo);
			$query->execute();
			return $query;

		}

		protected function datos_discapacidad_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM discapacidad WHERE dis_codigo=:Codigo");
				$query->bindParam(":Codigo",$codigo);				 				
			}elseif($tipo=="Conteo"){
				$query=mainModel::conectar()->prepare("SELECT dis_id FROM discapacidad");
			}
			$query->execute();
			return $query;

		}
		protected function actualizar_discapacidad_modelo($datos){
			$query=mainModel::conectar()->prepare("UPDATE discapacidad SET dis_nombre=:Nombre, dis_descripcion=:Descripcion,dis_carnetconadis=:Carnet,dis_estado=:Estado,Porcetaje_por_id=:Porcentaje WHERE dis_codigo=:Codigo");
			$query->bindParam(":Nombre",$datos['Nombre']);	
			$query->bindParam(":Descripcion",$datos['Descripcion']);
			$query->bindParam(":Carnet",$datos['Carnet']);			
			$query->bindParam(":Estado",$datos['Estado']);
			$query->bindParam(":Porcentaje",$datos['Porcentaje']);		
			$query->bindParam(":Codigo",$datos['Codigo']);			
			$query->execute();
			return $query;	
		}
		
	}