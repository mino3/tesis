<?php 

if ($peticionAjax) {
		# code...
		require_once "../core/mainModel.php";

	}else{
		require_once "./core/mainModel.php";
	}

	/**
	 * 
	 */
	class etniaModelo extends mainModel
	{
		protected function agregar_etnia_modelo($datos){
			$sql=mainModel::conectar()->prepare('INSERT INTO etnia(et_id,et_codigo,et_nombre,et_descripcion,et_estado,et_adminfecha1) VALUES(:id,:codigo,:nombre,:descripcion,:estado,:adminfecha1)');
			   $sql->bindParam(':id',$datos['id']);
			   $sql->bindParam(':codigo',$datos['codigo']);
			   $sql->bindParam(':nombre',$datos['nombre']);
			   $sql->bindParam(':descripcion',$datos['descripcion']);
			   $sql->bindParam(':estado',$datos['estado']);
			   $sql->bindParam(':adminfecha1',$datos['adminfecha1']);
			   $sql->execute();

			   return $sql;
		}
		protected function eliminar_etnia_modelo($codigo){
			$query=self::conectar()->prepare("UPDATE etnia SET et_estado='0' WHERE et_codigo=:Codigo");
			$query->bindParam(':Codigo',$codigo);
			$query->execute();
			return $query;

		}

		protected function datos_etnia_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM etnia WHERE et_codigo=:Codigo");
				$query->bindParam(":Codigo",$codigo);				 				
			}elseif($tipo=="Conteo"){
				$query=mainModel::conectar()->prepare("SELECT et_id FROM etnia");
			}
			$query->execute();
			return $query;

		}
		protected function actualizar_etnia_modelo($datos){
			$query=mainModel::conectar()->prepare("UPDATE etnia SET et_nombre=:Nombre, et_descripcion=:Descripcion, et_estado=:Estado WHERE et_codigo=:Codigo");
			$query->bindParam(":Nombre",$datos['Nombre']);	
			$query->bindParam(":Descripcion",$datos['Descripcion']);	
			$query->bindParam(":Estado",$datos['Estado']);	
			$query->bindParam(":Codigo",$datos['Codigo']);			
			$query->execute();
			return $query;	

		}
		
	}