<?php 

if ($peticionAjax) {
		# code...
		require_once "../core/mainModel.php";

	}else{
		require_once "./core/mainModel.php";
	}
/**
 * 
 */
class tiposangreModelo extends mainModel
{
	protected function agregar_tiposangre_modelo($datos){
			$sql=mainModel::conectar()->prepare('INSERT INTO tiposangre(tisa_id,tisa_codigo,tisa_nombre,tisa_descripcion,tisa_estado,tisa_adminfecha1) VALUES (:id,:codigo,:nombre,:descripcion,:estado,:adminfecha1)');
			$sql->bindParam(':id',$datos['id']);
			$sql->bindParam(':codigo',$datos['codigo']);
			$sql->bindParam(':nombre',$datos['nombre']);
			$sql->bindParam(':descripcion',$datos['descripcion']);
			$sql->bindParam(':estado',$datos['estado']);
			$sql->bindParam(':adminfecha1',$datos['adminfecha1']);

			$sql->execute();

			return $sql;
	}
	protected function eliminar_tiposangre_modelo($codigo){
			$query=self::conectar()->prepare("UPDATE tiposangre SET tisa_estado='0' WHERE tisa_codigo=:Codigo");
			$query->bindParam(':Codigo',$codigo);
			$query->execute();
			return $query;

		}
		protected function datos_tiposangre_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM tiposangre WHERE tisa_codigo=:Codigo");
				$query->bindParam(":Codigo",$codigo);				 				
			}elseif($tipo=="Conteo"){
				$query=mainModel::conectar()->prepare("SELECT tisa_id FROM tiposangre");
			}
			$query->execute();
			return $query;

		}
		protected function actualizar_tiposangre_modelo($datos){
			$query=mainModel::conectar()->prepare("UPDATE tiposangre SET tisa_nombre=:Nombre, tisa_descripcion=:Descripcion,tisa_estado=:Estado WHERE tisa_codigo=:Codigo");
			$query->bindParam(":Nombre",$datos['Nombre']);	
			$query->bindParam(":Descripcion",$datos['Descripcion']);	
			$query->bindParam(":Estado",$datos['Estado']);	
			$query->bindParam(":Codigo",$datos['Codigo']);			
			$query->execute();
			return $query;	

		}
}