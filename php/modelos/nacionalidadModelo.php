<?php 

if ($peticionAjax) {
		# code...
		require_once "../core/mainModel.php";

	}else{
		require_once "./core/mainModel.php";
	}

	/**
	 * 
	 */
	class nacionalidadModelo extends mainModel
	{
		protected function agregar_nacionalidad_modelo($datos){
			$sql=mainModel::conectar()->prepare('INSERT INTO nacionalidad(nac_id,nac_codigo,nac_pais,nac_descripcion,nac_estado,nac_adminfecha1) VALUES (:id,:codigo,:pais,:descripcion,:estado,:adminfecha1)');
			$sql->bindParam(':id',$datos['id']);
			$sql->bindParam(':codigo',$datos['codigo']);
			$sql->bindParam(':pais',$datos['pais']);
			$sql->bindParam(':descripcion',$datos['descripcion']);
			$sql->bindParam(':estado',$datos['estado']);
			$sql->bindParam(':adminfecha1',$datos['adminfecha1']);

			$sql->execute();

			return $sql;
		}
		protected function eliminar_nacionalidad_modelo($codigo){
			$query=self::conectar()->prepare("UPDATE nacionalidad SET nac_estado='0' WHERE nac_codigo=:Codigo");
			$query->bindParam(':Codigo',$codigo);
			$query->execute();
			return $query;

		}

		protected function datos_nacionalidad_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM nacionalidad WHERE nac_codigo=:Codigo");
				$query->bindParam(":Codigo",$codigo);				 				
			}elseif($tipo=="Conteo"){
				$query=mainModel::conectar()->prepare("SELECT nac_id FROM nacionalidad");
			}
			$query->execute();
			return $query;

		}
		protected function actualizar_nacionalidad_modelo($datos){
			$query=mainModel::conectar()->prepare("UPDATE nacionalidad SET nac_pais=:Nombre, nac_descripcion=:Descripcion, nac_estado=:Estado WHERE nac_codigo=:Codigo");
			$query->bindParam(":Nombre",$datos['Nombre']);	
			$query->bindParam(":Descripcion",$datos['Descripcion']);	
			$query->bindParam(":Estado",$datos['Estado']);	
			$query->bindParam(":Codigo",$datos['Codigo']);			
			$query->execute();
			return $query;	

		}
		
	}