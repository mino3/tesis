<?php 

if ($peticionAjax) {
		# code...
		require_once "../core/mainModel.php";

	}else{
		require_once "./core/mainModel.php";
	}
/**
	 * 
	 */
	class tipocolegioModelo extends mainModel
	{
	protected function agregar_tipocolegio_modelo($datos){
	  $sql=mainModel::conectar()->prepare('INSERT INTO tipocolegio(tico_id,tico_codigo,tico_nombre,tico_descripcion,tico_estado,tico_adminfecha1) VALUES(:ID,:Codigo,:Nombre,:Descripcion,:Estado,:AdminFecha)');
			$sql->bindParam(':ID',$datos['ID']);
			$sql->bindParam(':Codigo',$datos['Codigo']);
			$sql->bindParam(':Nombre',$datos['Nombre']);
			$sql->bindParam(':Descripcion',$datos['Descripcion']);
			$sql->bindParam(':Estado',$datos['Estado']);
			$sql->bindParam(':AdminFecha',$datos['AdminFecha']);
			$sql->execute();
				return $sql;
		}
	protected function eliminar_tipocolegio_modelo($codigo){
			$query=self::conectar()->prepare("UPDATE tipocolegio SET tico_estado='0' WHERE tico_codigo=:Codigo");
			$query->bindParam(':Codigo',$codigo);
			$query->execute();
			return $query;
		}
	protected function datos_tipocolegio_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM tipocolegio WHERE tico_codigo=:Codigo");
				$query->bindParam(":Codigo",$codigo);				 				
			}elseif($tipo=="Conteo"){
				$query=mainModel::conectar()->prepare("SELECT tico_id FROM tipocolegio");
			}
			$query->execute();
			return $query;
		}
	protected function actualizar_tipocolegio_modelo($datos){
		$sql=mainModel::conectar()->prepare('UPDATE tipocolegio SET tico_nombre=:Nombre,tico_descripcion=:Descripcion,tico_estado=:Estado WHERE tico_codigo=:Codigo');	    
		$sql->bindParam(':Nombre',$datos['Nombre']);
		$sql->bindParam(':Descripcion',$datos['Descripcion']);
		$sql->bindParam(':Estado',$datos['Estado']);		
		$sql->bindParam(':Codigo',$datos['Codigo']);
		$sql->execute();
		return $sql;
		}

	}	