<?php 

if ($peticionAjax) {
		# code...
		require_once "../core/mainModel.php";

	}else{
		require_once "./core/mainModel.php";
	}
/**
	 * 
	 */
	class tipobachilleratoModelo extends mainModel
	{
	protected function agregar_tipobachillerato_modelo($datos){
	  $sql=mainModel::conectar()->prepare('INSERT INTO tipobachillerato(tiba_id,tiba_codigo,tiba_nombre,tiba_descripcion,tiba_estado,tiba_adminfecha1) VALUES(:ID,:Codigo,:Nombre,:Descripcion,:Estado,:AdminFecha)');
			$sql->bindParam(':ID',$datos['ID']);
			$sql->bindParam(':Codigo',$datos['Codigo']);
			$sql->bindParam(':Nombre',$datos['Nombre']);
			$sql->bindParam(':Descripcion',$datos['Descripcion']);
			$sql->bindParam(':Estado',$datos['Estado']);
			$sql->bindParam(':AdminFecha',$datos['AdminFecha']);
			$sql->execute();
				return $sql;
		}
	protected function eliminar_tipobachillerato_modelo($codigo){
			$query=self::conectar()->prepare("UPDATE tipobachillerato SET tiba_estado='0' WHERE tiba_codigo=:Codigo");
			$query->bindParam(':Codigo',$codigo);
			$query->execute();
			return $query;
		}
	protected function datos_tipobachillerato_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM tipobachillerato WHERE tiba_codigo=:Codigo");
				$query->bindParam(":Codigo",$codigo);				 				
			}elseif($tipo=="Conteo"){
				$query=mainModel::conectar()->prepare("SELECT tiba_id FROM tipobachillerato");
			}
			$query->execute();
			return $query;
		}
	protected function actualizar_tipobachillerato_modelo($datos){
		$sql=mainModel::conectar()->prepare('UPDATE tipobachillerato SET tiba_nombre=:Nombre,tiba_descripcion=:Descripcion,tiba_estado=:Estado WHERE tiba_codigo=:Codigo');	    
		$sql->bindParam(':Nombre',$datos['Nombre']);
		$sql->bindParam(':Descripcion',$datos['Descripcion']);
		$sql->bindParam(':Estado',$datos['Estado']);		
		$sql->bindParam(':Codigo',$datos['Codigo']);
		$sql->execute();
		return $sql;
		}

	}	