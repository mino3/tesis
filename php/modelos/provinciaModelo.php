<?php 

if ($peticionAjax) {
		# code...
	require_once "../core/mainModel.php";

}else{
	require_once "./core/mainModel.php";
}

	/**
	 * 
	 */
	class provinciaModelo extends mainModel
	{
		protected function agregar_provincia_modelo($datos){
			$sql=mainModel::conectar()->prepare('INSERT INTO provincia(prov_id,prov_codigo,prov_nombre,prov_descripcion,prov_estado,prov_adminfecha1) VALUES (:id,:codigo,:nombre,:descripcion,:estado,:adminfecha1)');

			$sql->bindParam(':id',$datos['id']);
			$sql->bindParam(':codigo',$datos['codigo']);
			$sql->bindParam(':nombre',$datos['nombre']);
			$sql->bindParam(':descripcion',$datos['descripcion']);
			$sql->bindParam(':estado',$datos['estado']);
			$sql->bindParam(':adminfecha1',$datos['adminfecha1']);

			$sql->execute();

			return $sql;
		}
		protected function eliminar_provincia_modelo($codigo){
			$query=self::conectar()->prepare("UPDATE provincia SET prov_estado='0' WHERE prov_codigo=:Codigo");
			$query->bindParam(':Codigo',$codigo);
			$query->execute();
			return $query;

		}

		protected function datos_provincia_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM provincia WHERE prov_codigo=:Codigo");
				$query->bindParam(":Codigo",$codigo);				 				
			}elseif($tipo=="Conteo"){
				$query=mainModel::conectar()->prepare("SELECT prov_id FROM provincia");
			}
			$query->execute();
			return $query;

		}
		protected function actualizar_provincia_modelo($datos){
			$query=mainModel::conectar()->prepare("UPDATE provincia SET prov_nombre=:Nombre, prov_descripcion=:Descripcion, prov_estado=:Estado WHERE prov_codigo=:Codigo");
			$query->bindParam(":Nombre",$datos['Nombre']);	
			$query->bindParam(":Descripcion",$datos['Descripcion']);	
			$query->bindParam(":Estado",$datos['Estado']);	
			$query->bindParam(":Codigo",$datos['Codigo']);			
			$query->execute();
			return $query;	

		}

	}