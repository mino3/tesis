<?php 

if ($peticionAjax) {
		# code...
		require_once "../core/mainModel.php";

	}else{
		require_once "./core/mainModel.php";
	}
/**
 * 
 */
class nivelestudioModelo extends mainModel
{
	protected function agregar_nivelestudio_modelo($datos){
		$sql=mainModel::conectar()->prepare('INSERT INTO nivelestudio(ni_id,ni_codigo,ni_descripcion,ni_tiempo,ni_estado,ni_adminfecha1) VALUES(:ID,:Codigo,:Descripcion,:Tiempo,:Estado,:AdminFecha)');
		$sql->bindParam(':ID',$datos['ID']);
		$sql->bindParam(':Codigo',$datos['Codigo']);		
		$sql->bindParam(':Descripcion',$datos['Descripcion']);
		$sql->bindParam(':Tiempo',$datos['Tiempo']);
		$sql->bindParam(':Estado',$datos['Estado']);
		$sql->bindParam(':AdminFecha',$datos['AdminFecha']);
		$sql->execute();
		return $sql;
	}
	protected function eliminar_nivelestudio_modelo($codigo){
			$query=self::conectar()->prepare("UPDATE nivelestudio SET ni_estado='0' WHERE ni_codigo=:Codigo");
			$query->bindParam(':Codigo',$codigo);
			$query->execute();
			return $query;
		}
	protected function datos_nivelestudio_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM nivelestudio WHERE ni_codigo=:Codigo");
				$query->bindParam(":Codigo",$codigo);				 				
			}elseif($tipo=="Conteo"){
				$query=mainModel::conectar()->prepare("SELECT ni_id FROM nivelestudio");
			}
			$query->execute();
			return $query;
		}
	protected function actualizar_nivelestudio_modelo($datos)	{
	$sql=mainModel::conectar()->prepare('UPDATE nivelestudio SET ni_descripcion=:Descripcion,ni_tiempo=:Tiempo,ni_estado=:Estado WHERE ni_codigo=:Codigo');	
		$sql->bindParam(':Descripcion',$datos['Descripcion']);
		$sql->bindParam(':Tiempo',$datos['Tiempo']);
		$sql->bindParam(':Estado',$datos['Estado']);
		$sql->bindParam(':Codigo',$datos['Codigo']);
		$sql->execute();
		return $sql;
		}

	
}
