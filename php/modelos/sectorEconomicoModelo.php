<?php 

if ($peticionAjax) {
		# code...
		require_once "../core/mainModel.php";

	}else{
		require_once "./core/mainModel.php";
	}
/**
	 * 
	 */
	class sectorEconomicoModelo extends mainModel
	{
	protected function agregar_sectorEconomico_modelo($datos){
	  $sql=mainModel::conectar()->prepare('INSERT INTO sectoreconomico(sec_id,sec_codigo,sec_nombre,sec_descripcion,sec_estado,sec_adminfecha) VALUES(:ID,:Codigo,:Nombre,:Descripcion,:Estado,:AdminFecha)');
			$sql->bindParam(':ID',$datos['ID']);
			$sql->bindParam(':Codigo',$datos['Codigo']);
			$sql->bindParam(':Nombre',$datos['Nombre']);
			$sql->bindParam(':Descripcion',$datos['Descripcion']);
			$sql->bindParam(':Estado',$datos['Estado']);
			$sql->bindParam(':AdminFecha',$datos['AdminFecha']);
			$sql->execute();
				return $sql;
		}
	protected function eliminar_sectorEconomico_modelo($codigo){
			$query=self::conectar()->prepare("UPDATE sectoreconomico SET sec_estado='0' WHERE sec_codigo=:Codigo");
			$query->bindParam(':Codigo',$codigo);
			$query->execute();
			return $query;
		}
	protected function datos_sectorEconomico_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM sectoreconomico WHERE sec_codigo=:Codigo");
				$query->bindParam(":Codigo",$codigo);				 				
			}elseif($tipo=="Conteo"){
				$query=mainModel::conectar()->prepare("SELECT sec_id FROM sectoreconomico");
			}
			$query->execute();
			return $query;
		}
	protected function actualizar_sectorEconomico_modelo($datos){
		$sql=mainModel::conectar()->prepare('UPDATE sectoreconomico SET sec_nombre=:Nombre,sec_descripcion=:Descripcion,sec_estado=:Estado WHERE sec_codigo=:Codigo');	    
		$sql->bindParam(':Nombre',$datos['Nombre']);
		$sql->bindParam(':Descripcion',$datos['Descripcion']);
		$sql->bindParam(':Estado',$datos['Estado']);		
		$sql->bindParam(':Codigo',$datos['Codigo']);
		$sql->execute();
		return $sql;
		}

	}	