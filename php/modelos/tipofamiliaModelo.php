<?php 

if ($peticionAjax) {
		# code...
		require_once "../core/mainModel.php";

	}else{
		require_once "./core/mainModel.php";
	}
/**
	 * 
	 */
	class tipofamiliaModelo extends mainModel
	{
	protected function agregar_tipofamilia_modelo($datos){
	  $sql=mainModel::conectar()->prepare('INSERT INTO tipofamilia(tifa_id,tifa_codigo,tifa_nombre,tifa_descripcion,tifa_estado,tifa_adminfecha1) VALUES(:ID,:Codigo,:Nombre,:Descripcion,:Estado,:AdminFecha)');
			$sql->bindParam(':ID',$datos['ID']);
			$sql->bindParam(':Codigo',$datos['Codigo']);
			$sql->bindParam(':Nombre',$datos['Nombre']);
			$sql->bindParam(':Descripcion',$datos['Descripcion']);
			$sql->bindParam(':Estado',$datos['Estado']);
			$sql->bindParam(':AdminFecha',$datos['AdminFecha']);
			$sql->execute();
				return $sql;
		}
	protected function eliminar_tipofamilia_modelo($codigo){
			$query=self::conectar()->prepare("UPDATE tipofamilia SET tifa_estado='0' WHERE tifa_codigo=:Codigo");
			$query->bindParam(':Codigo',$codigo);
			$query->execute();
			return $query;
		}
	protected function datos_tipofamilia_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM tipofamilia WHERE tifa_codigo=:Codigo");
				$query->bindParam(":Codigo",$codigo);				 				
			}elseif($tipo=="Conteo"){
				$query=mainModel::conectar()->prepare("SELECT tifa_id FROM tipofamilia");
			}
			$query->execute();
			return $query;
		}
	protected function actualizar_tipofamilia_modelo($datos){
		$sql=mainModel::conectar()->prepare('UPDATE tipofamilia SET tifa_nombre=:Nombre,tifa_descripcion=:Descripcion,tifa_estado=:Estado WHERE tifa_codigo=:Codigo');	    
		$sql->bindParam(':Nombre',$datos['Nombre']);
		$sql->bindParam(':Descripcion',$datos['Descripcion']);
		$sql->bindParam(':Estado',$datos['Estado']);		
		$sql->bindParam(':Codigo',$datos['Codigo']);
		$sql->execute();
		return $sql;
		}

	}	