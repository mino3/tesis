<?php 

if ($peticionAjax) {
		# code...
		require_once "../core/mainModel.php";

	}else{
		require_once "./core/mainModel.php";
	}
/**
 * 
 */
class modalidadModelo extends mainModel
{
	protected function agregar_modalidad_modelo($datos){
		$sql=mainModel::conectar()->prepare('INSERT INTO modalidad(mod_id,mod_codigo,mod_nombre,mod_descripcion,mod_estado,mod_adminfecha1) VALUES(:ID,:Codigo,:Nombre,:Descripcion,:Estado,:AdminFeccha)');
		$sql->bindParam(':ID',$datos['ID']);
		$sql->bindParam(':Codigo',$datos['Codigo']);
		$sql->bindParam(':Nombre',$datos['Nombre']);
		$sql->bindParam(':Descripcion',$datos['Descripcion']);
		$sql->bindParam(':Estado',$datos['Estado']);
		$sql->bindParam(':AdminFeccha',$datos['AdminFeccha']);
		$sql->execute();
		return $sql;
	}
	protected function eliminar_modalidad_modelo($codigo){
			$query=self::conectar()->prepare("UPDATE modalidad SET mod_estado='0' WHERE mod_codigo=:Codigo");
			$query->bindParam(':Codigo',$codigo);
			$query->execute();
			return $query;
		}
	protected function datos_modalidad_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM modalidad WHERE mod_codigo=:Codigo");
				$query->bindParam(":Codigo",$codigo);				 				
			}elseif($tipo=="Conteo"){
				$query=mainModel::conectar()->prepare("SELECT mod_id FROM modalidad");
			}
			$query->execute();
			return $query;
		}
	protected function actualizar_modalidad_modelo($datos)	{
	$sql=mainModel::conectar()->prepare('UPDATE modalidad SET mod_nombre=:Nombre,mod_descripcion=:Descripcion,mod_estado=:Estado WHERE mod_codigo=:Codigo');	    
		$sql->bindParam(':Nombre',$datos['Nombre']);
		$sql->bindParam(':Descripcion',$datos['Descripcion']);
		$sql->bindParam(':Estado',$datos['Estado']);
		$sql->bindParam(':Codigo',$datos['Codigo']);
		$sql->execute();
		return $sql;
		}

	
}
