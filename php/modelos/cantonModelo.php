<?php 

if ($peticionAjax) {
		# code...
		require_once "../core/mainModel.php";

	}else{
		require_once "./core/mainModel.php";
	}
/**
 * 
 */
class cantonModelo extends mainModel
{
	
	protected function agregar_canton_modelo($datos){
		$sql=mainModel::conectar()->prepare('INSERT INTO canton(can_id,can_codigo,can_nombre,can_descripcion,can_estado,can_adminfecha1) VALUES (:id,:codigo,:nombre,:descripcion,:estado,:adminfecha1)');

		$sql->bindParam(':id',$datos['id']);
		$sql->bindParam(':codigo',$datos['codigo']);
		$sql->bindParam(':nombre',$datos['nombre']);
		$sql->bindParam(':descripcion',$datos['descripcion']);
		$sql->bindParam(':estado',$datos['estado']);
		$sql->bindParam(':adminfecha1',$datos['adminfecha1']);

		$sql->execute();

		return $sql;
	}
	protected function eliminar_canton_modelo($codigo){
			$query=self::conectar()->prepare("UPDATE canton SET can_estado='0' WHERE can_codigo=:Codigo");
			$query->bindParam(':Codigo',$codigo);
			$query->execute();
			return $query;

		}

		protected function datos_canton_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM canton WHERE can_codigo=:Codigo");
				$query->bindParam(":Codigo",$codigo);				 				
			}elseif($tipo=="Conteo"){
				$query=mainModel::conectar()->prepare("SELECT can_id FROM canton");
			}
			$query->execute();
			return $query;

		}
		protected function actualizar_canton_modelo($datos){
			$query=mainModel::conectar()->prepare("UPDATE canton SET can_nombre=:Nombre, can_descripcion=:Descripcion, can_estado=:Estado WHERE can_codigo=:Codigo");
			$query->bindParam(":Nombre",$datos['Nombre']);	
			$query->bindParam(":Descripcion",$datos['Descripcion']);	
			$query->bindParam(":Estado",$datos['Estado']);	
			$query->bindParam(":Codigo",$datos['Codigo']);			
			$query->execute();
			return $query;	

		}
}