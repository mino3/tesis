<?php 

if ($peticionAjax) {
		# code...
		require_once "../core/mainModel.php";

	}else{
		require_once "./core/mainModel.php";
	}
/**
 * 
 */
class jornadaModelo extends mainModel
{
	protected function agregar_jornada_modelo($datos){
		$sql=mainModel::conectar()->prepare('INSERT INTO jornadas(jor_id,jor_codigo,jor_nombre,jor_descripcion,jor_estado,jor_adminfecha1) VALUES(:ID,:Codigo,:Nombre,:Descripcion,:Estado,:AdminFeccha)');
		$sql->bindParam(':ID',$datos['ID']);
		$sql->bindParam(':Codigo',$datos['Codigo']);
		$sql->bindParam(':Nombre',$datos['Nombre']);
		$sql->bindParam(':Descripcion',$datos['Descripcion']);
		$sql->bindParam(':Estado',$datos['Estado']);
		$sql->bindParam(':AdminFeccha',$datos['AdminFeccha']);
		$sql->execute();
		return $sql;
	}
	protected function eliminar_jornada_modelo($codigo){
			$query=self::conectar()->prepare("UPDATE jornadas SET jor_estado='0' WHERE jor_codigo=:Codigo");
			$query->bindParam(':Codigo',$codigo);
			$query->execute();
			return $query;
		}
	protected function datos_jornada_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM jornadas WHERE jor_codigo=:Codigo");
				$query->bindParam(":Codigo",$codigo);				 				
			}elseif($tipo=="Conteo"){
				$query=mainModel::conectar()->prepare("SELECT jor_id FROM jornadas");
			}
			$query->execute();
			return $query;
		}
	protected function actualizar_jornada_modelo($datos)	{
	$sql=mainModel::conectar()->prepare('UPDATE jornadas SET jor_nombre=:Nombre,jor_descripcion=:Descripcion,jor_estado=:Estado WHERE jor_codigo=:Codigo');	    
		$sql->bindParam(':Nombre',$datos['Nombre']);
		$sql->bindParam(':Descripcion',$datos['Descripcion']);
		$sql->bindParam(':Estado',$datos['Estado']);
		$sql->bindParam(':Codigo',$datos['Codigo']);
		$sql->execute();
		return $sql;
		}

	
}
