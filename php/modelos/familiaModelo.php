<?php 

if ($peticionAjax) {
		# code...
		require_once "../core/mainModel.php";

	}else{
		require_once "./core/mainModel.php";
	}
	/**
	 * 
	 */
	class familiaModelo extends mainModel{
		protected function agregar_persona_modelo($datos){
          $sql=mainModel::conectar()->prepare('INSERT INTO persona(per_id,per_codigo,per_primernombre,per_segundonombre,per_primerapellido,per_segundoapellido,per_fechanacimiento,per_telefono,per_celular,per_correo,per_fecharegistro,per_discapacidad,per_estado,per_adminfecha1,Etnia_et_id,EstadioCivil_esci_id,TipoSangre_tisa_id,Discapacidad_dis_id,Nacionalidad_nac_id,Provincia_prov_id,Canton_can_id)VALUES(:ID,:Codigo,:PNombre,:SNombre,:PApellido,:SApellido,:FechaN,:Telefono,:Celular,:Correo,:FechaR,:DiscapacidadSN,:Estado,:AdminFch1,:Etnia,:EstadoCivil,:TipoSangre,:Discapacidad,:Nacionalidad,:Provincia,:Canton)');
			$sql->bindParam(':ID',$datos['ID']);
			$sql->bindParam(':Codigo',$datos['Codigo']);
			$sql->bindParam(':PNombre',$datos['PNombre']);
			$sql->bindParam(':SNombre',$datos['SNombre']);
			$sql->bindParam(':PApellido',$datos['PApellido']);
			$sql->bindParam(':SApellido',$datos['SApellido']);
			$sql->bindParam(':FechaN',$datos['FechaN']);
			$sql->bindParam(':Telefono',$datos['Telefono']);
			$sql->bindParam(':Celular',$datos['Celular']);
			$sql->bindParam(':Correo',$datos['Correo']);
			$sql->bindParam(':FechaR',$datos['FechaR']);
			$sql->bindParam(':DiscapacidadSN',$datos['DiscapacidadSN']);
			$sql->bindParam(':Estado',$datos['Estado']);
			$sql->bindParam(':AdminFch1',$datos['AdminFch1']);
			$sql->bindParam(':Etnia',$datos['Etnia']);
			$sql->bindParam(':EstadoCivil',$datos['EstadoCivil']);
			$sql->bindParam(':TipoSangre',$datos['TipoSangre']);
			$sql->bindParam(':Discapacidad',$datos['Discapacidad']);
			$sql->bindParam(':Nacionalidad',$datos['Nacionalidad']);
			$sql->bindParam(':Provincia',$datos['Provincia']);
			$sql->bindParam(':Canton',$datos['Canton']);
			$sql->execute();
			return $sql;
		}
		 protected function domicilio_persona_modelo($datos){
	    $sql=mainModel::conectar()->prepare('INSERT INTO direccion(dir_id,dir_codigo,dir_calleprincipal,dir_callesecundaria,dir_numerocasa,dir_sector,dir_codigoPostal,dir_referencia,dir_estado,dir_adminfecha,Persona_per_id) VALUES (:ID,:Codigo,:Principal,:Secundaria,:NumCasa,:Sector,:Postal,:Referencia,:Estado,:AdminFecha,:Persona)');
	    	$sql->bindParam(':ID',$datos['ID']);
	    	$sql->bindParam(':Codigo',$datos['Codigo']);
	    	$sql->bindParam(':Principal',$datos['Principal']);
	    	$sql->bindParam(':Secundaria',$datos['Secundaria']);
	    	$sql->bindParam(':NumCasa',$datos['NumCasa']);
	    	$sql->bindParam(':Sector',$datos['Sector']);
	    	$sql->bindParam(':Postal',$datos['Postal']);
	    	$sql->bindParam(':Referencia',$datos['Referencia']);
	    	$sql->bindParam(':Estado',$datos['Estado']);
	    	$sql->bindParam(':AdminFecha',$datos['AdminFecha']);
	    	$sql->bindParam(':Persona',$datos['Persona']);
	    	$sql->execute();
			return $sql;
	    }
	protected function agregar_familia_modelo($datos){
		$sql=mainModel::conectar()->prepare('INSERT INTO familia(fa_id,fa_codigo,fa_estado,fa_adminfecha1,Persona_per_id,TipoFamilia_tifa_id) VALUES(:ID,:Codigo,:Estado,:AdminFecha,:Persona,:TipoFamilia)');
		$sql->bindParam(':ID',$datos['ID']);
		$sql->bindParam(':Codigo',$datos['Codigo']);
		$sql->bindParam(':Estado',$datos['Estado']);
		$sql->bindParam(':AdminFecha',$datos['AdminFecha']);
		$sql->bindParam(':Persona',$datos['Persona']);
		$sql->bindParam(':TipoFamilia',$datos['TipoFamilia']);
		$sql->execute();
		return $sql;
	}
	protected function eliminar_familia_modelo($codigo){
			$query=mainModel::conectar()->prepare("UPDATE familia SET fa_estado='0' WHERE fa_codigo=:Codigo");
			$query->bindParam(':Codigo',$codigo);
			$query->execute();
			return $query;

	}
	protected function datos_familia_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM persona WHERE per_codigo=:Codigo");
				 $query->bindParam(":Codigo",$codigo);				 
			
		}elseif($tipo=="Conteo"){
			$query=mainModel::conectar()->prepare("SELECT per_id FROM persona WHERE per_id!='1'");
		}
		$query->execute();
		return $query;
		
	}
	
	protected function datos_direccion_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM direccion WHERE Persona_per_id=:Codigo");
				 $query->bindParam(":Codigo",$codigo);				 
			
		}elseif($tipo=="Conteo"){
			$query=mainModel::conectar()->prepare("SELECT per_id FROM direccion WHERE dir_id!='1'");
		}
		$query->execute();
		return $query;
		
	}

	//actulizacion de persona
	protected function datos_fam_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM familia WHERE fa_codigo=:Codigo");
				 $query->bindParam(":Codigo",$codigo);				 
			
		}elseif($tipo=="Conteo"){
			$query=mainModel::conectar()->prepare("SELECT fa_id FROM familia WHERE fa_id!='1'");
		}
		$query->execute();
		return $query;
		
	}
	protected function actualizar_familia_modelo($datos){
		$sql=mainModel::conectar()->prepare('UPDATE familia SET fa_estado=:Estado,TipoFamilia_tifa_id=:TipoFamilia WHERE fa_codigo=:Codigo');
		$sql->bindParam(':Estado',$datos['Estado']);
		$sql->bindParam(':TipoFamilia',$datos['TipoFamilia']);
		$sql->bindParam(':Codigo',$datos['Codigo']);
		$sql->execute();
			return $sql;
	}
	protected function actualizar_persona_modelo($datos){
	    	$sql=mainModel::conectar()->prepare('UPDATE persona SET per_primernombre=:PNombre,per_segundonombre=:SNombre,per_primerapellido=:PApellido,per_segundoapellido=:SApellido,per_fechanacimiento=:FechaN,per_telefono=:Telefono,per_celular=:Celular,per_correo=:Correo,per_fecharegistro=:FechaR,per_discapacidad=:DiscapacidadSN,per_estado=:Estado,per_adminfecha1=:AdminFch1,Etnia_et_id=:Etnia,EstadioCivil_esci_id=:EstadoCivil,TipoSangre_tisa_id=:TipoSangre,Discapacidad_dis_id=:Discapacidad,Nacionalidad_nac_id=:Nacionalidad,Provincia_prov_id=:Provincia,Canton_can_id=:Canton
	    		 WHERE per_codigo=:Codigo');	    	
	    	$sql->bindParam(':PNombre',$datos['PNombre']);
			$sql->bindParam(':SNombre',$datos['SNombre']);
			$sql->bindParam(':PApellido',$datos['PApellido']);
			$sql->bindParam(':SApellido',$datos['SApellido']);
			$sql->bindParam(':FechaN',$datos['FechaN']);
			$sql->bindParam(':Telefono',$datos['Telefono']);
			$sql->bindParam(':Celular',$datos['Celular']);
			$sql->bindParam(':Correo',$datos['Correo']);
			$sql->bindParam(':FechaR',$datos['FechaR']);
			$sql->bindParam(':DiscapacidadSN',$datos['DiscapacidadSN']);
			$sql->bindParam(':Estado',$datos['Estado']);
			$sql->bindParam(':AdminFch1',$datos['AdminFch1']);
			$sql->bindParam(':Etnia',$datos['Etnia']);
			$sql->bindParam(':EstadoCivil',$datos['EstadoCivil']);
			$sql->bindParam(':TipoSangre',$datos['TipoSangre']);
			$sql->bindParam(':Discapacidad',$datos['Discapacidad']);
			$sql->bindParam(':Nacionalidad',$datos['Nacionalidad']);
			$sql->bindParam(':Provincia',$datos['Provincia']);
			$sql->bindParam(':Canton',$datos['Canton']);			
			$sql->bindParam(':Codigo',$datos['Codigo']);
			$sql->execute();
			return $sql;
	    }
	    protected function actualizar_direccion_modelo($datos){
	    	$sql=mainModel::conectar()->prepare('UPDATE direccion SET dir_calleprincipal=:Principal,dir_callesecundaria=:Secundaria,dir_numerocasa=:NumCasa,dir_sector=:Sector,dir_codigoPostal=:Postal,dir_referencia=:Referencia,dir_estado=:Estado WHERE Persona_per_id=:Codigo');

	    	$sql->bindParam(':Principal',$datos['Principal']);
	    	$sql->bindParam(':Secundaria',$datos['Secundaria']);
	    	$sql->bindParam(':NumCasa',$datos['NumCasa']);
	    	$sql->bindParam(':Sector',$datos['Sector']);
	    	$sql->bindParam(':Postal',$datos['Postal']);
	    	$sql->bindParam(':Referencia',$datos['Referencia']);
	    	$sql->bindParam(':Estado',$datos['Estado']);
	    	$sql->bindParam(':Codigo',$datos['Codigo']);
	    	$sql->execute();
			return $sql;
	    	


	    }
}