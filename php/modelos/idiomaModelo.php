<?php 
if ($peticionAjax) {
		# code...
	require_once "../core/mainModel.php";

}else{
	require_once "./core/mainModel.php";
}

	/**
	 * 
	 */
	class idiomaModelo extends mainModel
	{
		protected function agregar_idioma_modelo($datos){
			$sql=mainModel::conectar()->prepare('INSERT INTO idioma(idi_id,idi_codigo,idi_nombre,idi_descripcion,idi_estado,idi_adminfecha1) VALUES (:id,:codigo,:nombre,:descripcion,:estado,:adminfecha1)');

			$sql->bindParam(':id',$datos['id']);
			$sql->bindParam(':codigo',$datos['codigo']);
			$sql->bindParam(':nombre',$datos['nombre']);
			$sql->bindParam(':descripcion',$datos['descripcion']);
			$sql->bindParam(':estado',$datos['estado']);
			$sql->bindParam(':adminfecha1',$datos['adminfecha1']);
			$sql->execute();

			return $sql;
		}
		protected function eliminar_idioma_modelo($codigo){			
			$query=mainModel::conectar()->prepare("UPDATE idioma SET idi_estado='0' WHERE idi_codigo=:Codigo");
			$query->bindParam(':Codigo',$codigo);
			$query->execute();
			return $query;

		}
		protected function datos_idioma_modelo($tipo,$codigo){
			if ($tipo=="Unico") {
				$query=mainModel::conectar()->prepare("SELECT * FROM idioma WHERE idi_codigo=:Codigo");
				$query->bindParam(":Codigo",$codigo);				 				
			}elseif($tipo=="Conteo"){
				$query=mainModel::conectar()->prepare("SELECT idi_id FROM idioma");
			}
			$query->execute();
			return $query;

		}
		protected function actualizar_idioma_modelo($datos){
			$query=mainModel::conectar()->prepare("UPDATE idioma SET idi_nombre=:Nombre, idi_descripcion=:Descripcion, idi_estado=:Estado WHERE idi_codigo=:Codigo");
			$query->bindParam(":Nombre",$datos['Nombre']);	
			$query->bindParam(":Descripcion",$datos['Descripcion']);	
			$query->bindParam(":Estado",$datos['Estado']);	
			$query->bindParam(":Codigo",$datos['Codigo']);			
			$query->execute();
			return $query;	

		}

	}