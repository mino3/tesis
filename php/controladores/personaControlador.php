<?php 

if ($peticionAjax) {
		# code...
	require_once "../modelos/personaModelo.php";

}else{
	require_once "./modelos/personaModelo.php";
}

/**
 * 
 */
class personaControlador extends personaModelo
{
	
	public function agregar_persona_controlador(){
		$cedula=mainModel::limpiar_cadena($_POST['dni-reg']);
		$Pnombre=mainModel::limpiar_cadena($_POST['1nombre-reg']);
		$Snombre=mainModel::limpiar_cadena($_POST['2nombre-reg']);
		$PApel=mainModel::limpiar_cadena($_POST['1ape-reg']);
		$SApel=mainModel::limpiar_cadena($_POST['2ape-reg']);
		$FechaN=mainModel::limpiar_cadena($_POST['fechaN-reg']);
		$Telf=mainModel::limpiar_cadena($_POST['telf-reg']);
		$Cell=mainModel::limpiar_cadena($_POST['celu-reg']);
		$email=mainModel::limpiar_cadena($_POST['email-reg']);
		$fecharegistro=date("Y-m-d h:i:s a");
		$DiscaSN=mainModel::limpiar_cadena($_POST['optionsDiscapacidad']);
		$estado=mainModel::limpiar_cadena($_POST['optionsEstado']);
		$adminfecha1=date("Y-m-d h:i:s a");
		$etnia=mainModel::limpiar_cadena($_POST['asetnia']);
		$estaCivil=mainModel::limpiar_cadena($_POST['ascivil']);
		$tipoSangre=mainModel::limpiar_cadena($_POST['asSangre']);
		$discapacidad=mainModel::limpiar_cadena($_POST['asDiscapa']);
		$nacionalidad=mainModel::limpiar_cadena($_POST['asNacion']);
		$canton=mainModel::limpiar_cadena($_POST['asCanton']);
		$provincia=mainModel::limpiar_cadena($_POST['asProvinc']);

		//informacionDomiciliaria
		//codigo
		$principal=mainModel::limpiar_cadena($_POST['CPrinci-reg']);
		$secundaria=mainModel::limpiar_cadena($_POST['CSecu-reg']);
		$numcasa=mainModel::limpiar_cadena($_POST['numcasa-reg']);
		$sector=mainModel::limpiar_cadena($_POST['sector-reg']);
		$postal="00000";
		$referencia=mainModel::limpiar_cadena($_POST['refer-reg']);
		//estado
		$adminfecha=date("Y-m-d h:i:s a");
		//persona

		

		if (mainModel::validarCedula($cedula)) {
			$consulta1=mainModel::ejecutar_consulta_simple("SELECT per_codigo FROM persona WHERE per_codigo='$cedula'");
			if ($consulta1->rowCount()==1) {
				$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio Un Error Inesperado",
					"Texto"=> "Cedula de Identidad ya se encuentra Registrada",
					"Tipo"=> "error"
				];
			}
			
			$consulta2=mainModel::ejecutar_consulta_simple("SELECT per_id FROM persona");
			$CodPer=($consulta2->rowCount())+1;
			$codigo=mainModel::generrar_codigo_aleatorio("PRS",2,$CodPer);
			$dataPersona=[
				'ID'=>$CodPer,
				'Codigo'=>$codigo,
				'PNombre'=>$Pnombre,
				'SNombre'=>$Snombre,
				'PApellido'=>$PApel,
				'SApellido'=>$SApel,
				'FechaN'=>$FechaN,
				'Telefono'=>$Telf,
				'Celular'=>$Cell,
				'Correo'=>$email,
				'FechaR'=>$fecharegistro,
				'DiscapacidadSN'=>$DiscaSN,
				'Estado'=>$estado,
				'AdminFch1'=>$adminfecha1,
				'Etnia'=>$etnia,
				'EstadoCivil'=>$estaCivil,
				'TipoSangre'=>$tipoSangre,
				'Discapacidad'=>$discapacidad,
				'Nacionalidad'=>$nacionalidad,
				'Provincia'=>$provincia,
				'Canton'=>$canton				
			];
			$consulta3=mainModel::ejecutar_consulta_simple("SELECT dir_id FROM direccion");
			$numero=($consulta3->rowCount())+1;
			$codigo=mainModel::generrar_codigo_aleatorio("DRC",2,$numero);
			$dataDomicilio=[
				'ID'=>$numero,
				'Codigo'=>$codigo,
				'Principal'=>$principal,
				'Secundaria'=>$secundaria,
				'NumCasa'=>$numcasa,
				'Sector'=>$sector,
				'Postal'=>$postal,
				'Referencia'=>$referencia,
				'Estado'=>$estado,
				'AdminFecha'=>$adminfecha,
				'Persona'=>$CodPer
			];
			if (personaModelo::agregar_persona_modelo($dataPersona)) {
						# code...
				if (personaModelo::domicilio_persona_modelo($dataDomicilio)) {
							# code...
					$alerta=[
						"Alerta"=> "limpiar",
						"Titulo"=> "DATOS INGRESADOS  ",
						"Texto"=> "PERSONA REGISTRADA!",
						"Tipo"=> "success"
					];
				}else{
					$alerta=[
						"Alerta"=> "simple",
						"Titulo"=> "OCUARRIO UN ERROR INESPERADO",
						"Texto"=> "PERSONA NO REGISTRADA!",
						"Tipo"=> "error"
					];

				}
				
			}else{	
				$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "OCUARRIO UN ERROR INESPERADO",
					"Texto"=> "PERSONA NO REGISTRADA!",
					"Tipo"=> "error"
				];
			}


			
		//validacion de la cedula 	
		}else{
			$alerta=[
				"Alerta"=> "simple",
				"Titulo"=> "INCORRECTO  ".$cedula,
				"Texto"=> "Cedula inCORRECTO",
				"Tipo"=> "error"
			];
		}
		return mainModel::sweet_alert($alerta);
	}
	public function paginador_persona_controlador($pagina,$registros,$privilegio,$codigo,$busqueda){
			$pagina=mainModel::limpiar_cadena($pagina);
			$registros=mainModel::limpiar_cadena($registros);
			$privilegio=mainModel::limpiar_cadena($privilegio);
			$codigo=mainModel::limpiar_cadena($codigo);
			$busqueda==mainModel::limpiar_cadena($busqueda);
			$tabla="";
			//operador ternario (codicion)  el uno solo muestra el primer paginador 
			$pagina= (isset($pagina) && $pagina>0) ? (int) $pagina: 1;
		//comprobar cuantos registros queremos ver
			$inicio= ($pagina>0) ? (($pagina*$registros)-$registros) : 0 ;
			//validar cuando utilizamos el de buscar
				if (isset($busqueda) && $busqueda!="") {
					$consulta="SELECT SQL_CALC_FOUND_ROWS * FROM persona WHERE (per_codigo LIKE '%$busqueda%' OR per_nombre LIKE '%$busqueda%')  ORDER BY per_nombre ASC LIMIT $inicio,$registros";
					//validar el directorio cuando es busqueda
					$paginaUrl="personasearch";
				}else{					
					$consulta="SELECT SQL_CALC_FOUND_ROWS s.per_codigo,s.per_primernombre,s.per_segundonombre,s.per_primerapellido,s.per_segundoapellido,s.per_fechanacimiento,s.per_telefono,s.per_celular,s.per_correo,s.per_estado,s.per_discapacidad, s.Etnia_et_id,g.et_nombre, s.EstadioCivil_esci_id, e.esci_nombre, s.TipoSangre_tisa_id ,t.tisa_nombre,s.Discapacidad_dis_id, d.dis_nombre, s.Nacionalidad_nac_id, n.nac_pais, s.Canton_can_id,c.can_nombre, s.Provincia_prov_id,p.prov_nombre FROM persona s 
						INNER JOIN etnia g ON s.Etnia_et_id=g.et_id
						INNER JOIN estadocivil e ON s.EstadioCivil_esci_id=e.esci_id
						INNER JOIN tiposangre t ON s.TipoSangre_tisa_id=t.tisa_id
						INNER JOIN discapacidad d ON s.Discapacidad_dis_id=d.dis_id
						INNER JOIN nacionalidad n On s.Nacionalidad_nac_id=n.nac_id
						INNER JOIN canton c ON s.Canton_can_id=c.can_id
						INNER JOIN provincia p ON s.Provincia_prov_id=p.prov_id
						ORDER BY per_primernombre ASC LIMIT $inicio,$registros";
					//validar el directorio cuando es lista
					$paginaUrl="personalist";
				}

			//1*5-5     0-4 5-9
			// hereda una conexion a la BD
			$conexion = mainModel::conectar();
			// CALCULAR LOS REGISTROS DE LA TABLA
			$datos= $conexion->query($consulta);
			// toma los valores de la consulta
			$datos=$datos->fetchAll();
			//SELECCION TODAS FILA ENCOONTRADAS
			$total=$conexion->query("SELECT FOUND_ROWS()");
			$total= (int) $total->fetchColumn();

			//total de paginas o paginador
			//ceil toma los enteros 
			//100reg / 15 = 6.66 paginas pero ceil redondea
			$Npaginas=ceil($total/$registros);
				//empiesa la tabla 
			$tabla.='<div class="table-responsive">
				<table class="table table-hover text-center">
					<thead>
					<tr>
					<th class="text-center">#</th>
					 <th class="text-center">Codigo</th>
					<th class="text-center">Nombres</th>
					<th class="text-center">Apellidos</th>
					<th class="text-center">Fecha Nacimiento</th>
					<th class="text-center">Telefono</th>
					<th class="text-center">Celular</th>
					<th class="text-center">Correo</th>
					<th class="text-center">Discapacidad S/N</th>
					<th class="text-center">Estado</th>
					<th class="text-center">Etnia</th>
					<th class="text-center">EstadoCivil</th>
					<th class="text-center">TipoSangre</th>
					<th class="text-center">Discapacidad</th>
					<th class="text-center">Nacionalidad</th>
					<th class="text-center">Provincia</th>
					<th class="text-center">Canton</th>

					';	
					if ($privilegio<=2) {
						# code...
						$tabla.='						
					<th class="text-center">Actualizar </th>';
					}if ($privilegio==1) {
						# code...
						$tabla.='
						
					<th class="text-center">Delete</th>';
					}
					
					$tabla.='</tr>
					</thead>
				<tbody>
			';
			if ($total>=1 && $pagina<=$Npaginas) {
				# code...
				$contador=$inicio+1;
				foreach ($datos as $rows) {
					$tabla.='
							<tr>
							<td>'.$contador.'</td>
							<td>'.$rows['per_codigo'].'</td>
					<td>'.$rows['per_primernombre']." ".$rows['per_segundonombre'].'</td>
				<td>'.$rows['per_primerapellido']." ".$rows['per_segundoapellido'].'</td>	
				<td>'.$rows['per_fechanacimiento'].'</td>
				<td>'.$rows['per_telefono'].'</td>
				<td>'.$rows['per_celular'].'</td>
				<td>'.$rows['per_correo'].'</td>
				<td>'.$rows['per_discapacidad'].'</td>
				<td>'.$rows['per_estado'].'</td>
				<td>'.$rows['et_nombre'].'</td>
				<td>'.$rows['esci_nombre'].'</td>
				<td>'.$rows['tisa_nombre'].'</td>
				<td>'.$rows['dis_nombre'].'</td>
				<td>'.$rows['nac_pais'].'</td>
				<td>'.$rows['prov_nombre'].'</td>
				<td>'.$rows['can_nombre'].'</td>									
							';
								if ($privilegio<=2) {
									# code...
								
							$tabla.='
							<td><a href="'.SERVERURL.'personaUP/persona/'.mainModel::encryption($rows['per_codigo']).'/" class="btn btn-success btn-raised btn-xs"><i class="zmdi zmdi-refresh"></i></a></td>							
							';
							}
							if ($privilegio==1) {
								# code...
							
							$tabla.='<td>
								<form action="'.SERVERURL.'ajax/personaAjax.php" method="POST" class="FormularioAjax" data-form="delete" entype="multipart/form-data" autocomplete="off">
									<input type="hidden" name="codigo-del" value="'.mainModel::encryption($rows['per_codigo']).'"> 
									<input type="hidden" name="privilegio-admin" value="'.mainModel::encryption($privilegio).'"> 
									<button type="submit" class="btn btn-danger btn-raised btn-xs">
										<i class="zmdi zmdi-delete"></i>
									</button>
									<div class="RespuestaAjax"></div>
								</form>
							</td>';
							}
							$tabla.='</tr>';
				$contador++;	
				}
			}else{
				if ($total>=1) {
					# code...
						$tabla.='
					<tr>
						<td colspan="5">
							<a href="'.SERVERURL.$paginaUrl.'/" class="btn btn-sm btn-info btn-raised">
							  Haga click aqui para recargar listado
							</a>
						</td>
					</tr>
				';
				}else{
					$tabla.='
					<tr>
					   <td colspan="5">No hay registro en el sistema</td>
					</tr>
				';	
				}
				
			}
			// termina la tabla 
       $tabla.='</tbody></table></div>	
			';	
			if ($total>=1 && $pagina<=$Npaginas) {
				$tabla.='
				<nav class="text-center">
					<ul class="pagination pagination-sm">
				';
				if ($pagina==1) {
					$tabla.='
			<li class="disabled"><a><i class = "zmdi zmdi-arrow-left"> </i></a></li>';
				}else{
					// validar los paginadores
					$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.($pagina-1).'/"><i class = "zmdi zmdi-arrow-left"> </i></a></li>';

				}
				// numeros de la paginacion del medio 123
					for($i=1; $i<=$Npaginas; $i++){
						if ($pagina==$i) {
							$tabla.='
			<li class="active"><a href="'.SERVERURL.$paginaUrl.'/'.$i.'/">'.$i.'</a></li>';
						}else{
							$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.$i.'/">'.$i.'</a></li>';

						}
					}
				// valida el ultimo paginador
				if ($pagina==$Npaginas) {
					$tabla.='
			<li class="disabled"><a><i class = "zmdi zmdi-arrow-right"> </i></a></li>';
				}else{
					// validar los paginadores
					$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.($pagina+1).'/"><i class = "zmdi zmdi-arrow-right"> </i></a></li>';

				}
				$tabla.='
					</ul>
				</nav>
				';
			}
			return $tabla;
		}
	


	public function eliminar_persona_controlador(){
			$codigo=mainModel::decryption($_POST['codigo-del']);
			$adminprivilegio=mainModel::decryption($_POST['privilegio-admin']);

			$codigo=mainModel::limpiar_cadena($codigo);
			$adminprivilegio=mainModel::limpiar_cadena($adminprivilegio);
				// 1 si tiene control total
			if ($adminprivilegio==1) {
				# code...
				//valida que no se elimine el primer administrador
				$query1=mainModel::ejecutar_consulta_simple("SELECT per_id FROM persona WHERE per_id='$codigo'");
				//array de datos de admin para ver el id
				$datosAdmin=$query1->fetch();
				if ($datosAdmin['per_id']!=1) {
					# code...
					$DelIdio=personaModelo::eliminar_persona_modelo($codigo);
					
					if ($DelIdio->rowCount()>=1) {
						# code...
						//$DelCuenta=mainModel::eliminar_cuenta($codigo);
						$alerta=[
					"Alerta"=> "recargar",
					"Titulo"=> "PERSONA ELIMINADO",
					"Texto"=> "PERSONA Eliminado",
					"Tipo"=> "success"
				];
						
					}else{
							$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "No podemos eliminar este PERSONA en este momento",
					"Tipo"=> "error"
				];

					}
				}else{
					$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "No poedemos eliminar LA PERSONA principal del sistema",
					"Tipo"=> "error"
				];

				}

			}else{
				$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "Tu no tienes los permisos necesarios para realizar esta operacion",
					"Tipo"=> "error"
				];

			}
			return mainModel::sweet_alert($alerta);
	}
	public function datos_persona_controlador($tipo,$codigo){
			$codigo=mainModel::decryption($codigo);
			$tipo=mainModel::limpiar_cadena($tipo);

			return estadocivilModelo::datos_estadocivil_modelo($tipo,$codigo);
		}
	


}