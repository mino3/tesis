<?php 
if ($peticionAjax) {
		# code...
	require_once "../modelos/etniaModelo.php";

}else{
	require_once "./modelos/etniaModelo.php";
}

/**
 * 
 */
class etniaControlador extends etniaModelo
{
	public function agregar_etnia_controlador(){
		$id=mainModel::limpiar_cadena($_POST['id-reg']);
		$nombre=mainModel::limpiar_cadena($_POST['nombre-reg']);
		$descripcion=mainModel::limpiar_cadena($_POST['desc-reg']);
		$estado=mainModel::limpiar_cadena($_POST['optionsEstado']);
		$adminfecha1=mainModel::limpiar_cadena($_POST['fecha-reg']);

		$consulta1=mainModel::ejecutar_consulta_simple("SELECT et_id FROM etnia WHERE et_id='$id'");
		if ($consulta1->rowCount()>=1) {
			# code...
			$alerta=[
						"Alerta"=> "simple",
						"Titulo"=> "Ocurrio un error inesperado",
						"Texto"=> "El ID que ingreso Ya existe",
						"Tipo"=> "error"
					];
		}else{  
		$consulta2=mainModel::ejecutar_consulta_simple("SELECT et_nombre FROM etnia WHERE et_nombre='$nombre'");
			if ($consulta2->rowCount()>=1) {
				# code...
				$alerta=[
								"Alerta"=> "simple",
								"Titulo"=> "Ocurrio un error inesperado",
								"Texto"=> "El etnia que ingreso Ya existe",
								"Tipo"=> "error"
							];
			}else{
				            $consulta3=mainModel::ejecutar_consulta_simple("SELECT et_id FROM etnia");
							$numero=($consulta3->rowCount())+1;
							$codigo=mainModel::generrar_codigo_aleatorio("ET",2,$numero);

							$dataET=[
								'id'=>$numero,
								'codigo'=>$codigo,
								'nombre'=>$nombre,
								'descripcion'=>$descripcion,
								'estado'=>$estado,
								'adminfecha1'=>$adminfecha1
							];
						
								$alerta=[
										"Alerta"=> "limpiar",
										"Titulo"=> "Etnia registrado",
										"Texto"=> "Etnia se registro",
										"Tipo"=> "succes" 
									];	
							$guardaretnia=etniaModelo::agregar_etnia_modelo($dataET);
			}

		}
		 return mainModel::sweet_alert($alerta);
	}

	public function paginador_etnia_controlador($pagina,$registros,$privilegio,$codigo,$busqueda){
			$pagina=mainModel::limpiar_cadena($pagina);
			$registros=mainModel::limpiar_cadena($registros);
			$privilegio=mainModel::limpiar_cadena($privilegio);
			$codigo=mainModel::limpiar_cadena($codigo);
			$busqueda==mainModel::limpiar_cadena($busqueda);
			$tabla="";
			//operador ternario (codicion)  el uno solo muestra el primer paginador 
			$pagina= (isset($pagina) && $pagina>0) ? (int) $pagina: 1;
		//comprobar cuantos registros queremos ver
			$inicio= ($pagina>0) ? (($pagina*$registros)-$registros) : 0 ;
			//validar cuando utilizamos el de buscar
				if (isset($busqueda) && $busqueda!="") {
					$consulta="SELECT SQL_CALC_FOUND_ROWS * FROM etnia WHERE (et_codigo LIKE '%$busqueda%' OR et_nombre LIKE '%$busqueda%')  ORDER BY et_nombre ASC LIMIT $inicio,$registros";
					//validar el directorio cuando es busqueda
					$paginaUrl="etniasearch";
				}else{
					$consulta="SELECT SQL_CALC_FOUND_ROWS * FROM etnia WHERE et_id != '$codigo'  ORDER BY et_nombre ASC LIMIT $inicio,$registros";
					//validar el directorio cuando es lista
					$paginaUrl="etnialist";
				}

			//1*5-5     0-4 5-9
			// hereda una conexion a la BD
			$conexion = mainModel::conectar();
			// CALCULAR LOS REGISTROS DE LA TABLA
			$datos= $conexion->query($consulta);
			// toma los valores de la consulta
			$datos=$datos->fetchAll();
			//SELECCION TODAS FILA ENCOONTRADAS
			$total=$conexion->query("SELECT FOUND_ROWS()");
			$total= (int) $total->fetchColumn();

			//total de paginas o paginador
			//ceil toma los enteros 
			//100reg / 15 = 6.66 paginas pero ceil redondea
			$Npaginas=ceil($total/$registros);
				//empiesa la tabla 
			$tabla.='<div class="table-responsive">
				<table class="table table-hover text-center">
					<thead>
					<tr>
					<th class="text-center">#</th>
					 <th class="text-center">Codigo</th>
					<th class="text-center">Nombre</th>
					<th class="text-center">Descripcion</th>
					<th class="text-center">estado</th>
					';	
					if ($privilegio<=2) {
						# code...
						$tabla.='						
					<th class="text-center">Actualizar </th>';
					}if ($privilegio==1) {
						# code...
						$tabla.='
						
					<th class="text-center">Delete</th>';
					}
					
					$tabla.='</tr>
					</thead>
				<tbody>
			';
			if ($total>=1 && $pagina<=$Npaginas) {
				# code...
				$contador=$inicio+1;
				foreach ($datos as $rows) {
					$tabla.='
							<tr>
							<td>'.$contador.'</td>
							<td>'.$rows['et_codigo'].'</td>
							<td>'.$rows['et_nombre'].'</td>
							<td>'.$rows['et_descripcion'].'</td>
							<td>'.$rows['et_estado'].'</td>					
							';
								if ($privilegio<=2) {
									# code...
								
							$tabla.='
							<td><a href="'.SERVERURL.'etniaUp/etnia/'.mainModel::encryption($rows['et_codigo']).'/" class="btn btn-success btn-raised btn-xs"><i class="zmdi zmdi-refresh"></i></a></td>							
							';
							}
							if ($privilegio==1) {
								# code...
							
							$tabla.='<td>
								<form action="'.SERVERURL.'ajax/etniaAjax.php" method="POST" class="FormularioAjax" data-form="delete" entype="multipart/form-data" autocomplete="off">
									<input type="hidden" name="codigo-del" value="'.mainModel::encryption($rows['et_codigo']).'"> 
									<input type="hidden" name="privilegio-admin" value="'.mainModel::encryption($privilegio).'"> 
									<button type="submit" class="btn btn-danger btn-raised btn-xs">
										<i class="zmdi zmdi-delete"></i>
									</button>
									<div class="RespuestaAjax"></div>
								</form>
							</td>';
							}
							$tabla.='</tr>';
				$contador++;	
				}
			}else{
				if ($total>=1) {
					# code...
						$tabla.='
					<tr>
						<td colspan="5">
							<a href="'.SERVERURL.$paginaUrl.'/" class="btn btn-sm btn-info btn-raised">
							  Haga click aqui para recargar listado
							</a>
						</td>
					</tr>
				';
				}else{
					$tabla.='
					<tr>
					   <td colspan="5">No hay registro en el sistema</td>
					</tr>
				';	
				}
				
			}
			// termina la tabla 
       $tabla.='</tbody></table></div>	
			';	
			if ($total>=1 && $pagina<=$Npaginas) {
				$tabla.='
				<nav class="text-center">
					<ul class="pagination pagination-sm">
				';
				if ($pagina==1) {
					$tabla.='
			<li class="disabled"><a><i class = "zmdi zmdi-arrow-left"> </i></a></li>';
				}else{
					// validar los paginadores
					$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.($pagina-1).'/"><i class = "zmdi zmdi-arrow-left"> </i></a></li>';

				}
				// numeros de la paginacion del medio 123
					for($i=1; $i<=$Npaginas; $i++){
						if ($pagina==$i) {
							$tabla.='
			<li class="active"><a href="'.SERVERURL.$paginaUrl.'/'.$i.'/">'.$i.'</a></li>';
						}else{
							$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.$i.'/">'.$i.'</a></li>';

						}
					}
				// valida el ultimo paginador
				if ($pagina==$Npaginas) {
					$tabla.='
			<li class="disabled"><a><i class = "zmdi zmdi-arrow-right"> </i></a></li>';
				}else{
					// validar los paginadores
					$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.($pagina+1).'/"><i class = "zmdi zmdi-arrow-right"> </i></a></li>';

				}
				$tabla.='
					</ul>
				</nav>
				';
			}
			return $tabla;
		}
	


	public function eliminar_etnia_controlador(){
			$codigo=mainModel::decryption($_POST['codigo-del']);
			$adminprivilegio=mainModel::decryption($_POST['privilegio-admin']);

			$codigo=mainModel::limpiar_cadena($codigo);
			$adminprivilegio=mainModel::limpiar_cadena($adminprivilegio);
				// 1 si tiene control total
			if ($adminprivilegio==1) {
				# code...
				//valida que no se elimine el primer administrador
				$query1=mainModel::ejecutar_consulta_simple("SELECT et_id FROM etnia WHERE et_id='$codigo'");
				//array de datos de admin para ver el id
				$datosAdmin=$query1->fetch();
				if ($datosAdmin['et_id']!=1) {
					# code...
					$DelIdio=etniaModelo::eliminar_etnia_modelo($codigo);
					
					if ($DelIdio->rowCount()>=1) {
						# code...
						//$DelCuenta=mainModel::eliminar_cuenta($codigo);
						$alerta=[
					"Alerta"=> "recargar",
					"Titulo"=> "ETNIA ELIMINADO",
					"Texto"=> "ETNIA Eliminado",
					"Tipo"=> "success"
				];
						
					}else{
							$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "No podemos eliminar este etnia en este momento",
					"Tipo"=> "error"
				];

					}
				}else{
					$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "No poedemos eliminar el etnia principal del sistema",
					"Tipo"=> "error"
				];

				}

			}else{
				$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "Tu no tienes los permisos necesarios para realizar esta operacion",
					"Tipo"=> "error"
				];

			}
			return mainModel::sweet_alert($alerta);
	}

	public function datos_etnia_controlador($tipo,$codigo){
			$codigo=mainModel::decryption($codigo);
			$tipo=mainModel::limpiar_cadena($tipo);

			return etniaModelo::datos_etnia_modelo($tipo,$codigo);
		}
		public function actualizar_etnia_controlador(){
		//codigo de la cuenta
		//$codigo=
		$cuenta=mainModel::decryption($_POST['codigo-up']);
		$nombre=mainModel::limpiar_cadena($_POST['nombre-up']);
		$descripcion=mainModel::limpiar_cadena($_POST['desc-up']);
		$estado=mainModel::limpiar_cadena($_POST['optionsEstado-up']);

			$query1=mainModel::ejecutar_consulta_simple("SELECT * FROM etnia WHERE et_codigo='$cuenta'");
			//tiene todos los datos del etnia
			$datosetnia=$query1->fetch();		
			if ($nombre!=$datosetnia['et_nombre']) {
		$consulta1=mainModel::ejecutar_consulta_simple("SELECT et_nombre FROM etnia WHERE et_nombre='$nombre'");
				//cuantos registros se afectan
				if ($consulta1->rowCount()==1) {
					$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado". $nombre,
					"Texto"=> "NOMBRE QUE ACABA DE INGRESAR YA SE ENCUENTRA REGISTRADO",
					"Tipo"=> "error"
				];
				return mainModel::sweet_alert($alerta);
				//detiene la ejecucion de la consultas
				exit();
				}
			}
			$dataetnia=[				
				"Nombre"=>$nombre,
				"Descripcion"=>$descripcion,
				"Estado"=>$estado,
				"Codigo"=>$cuenta
			];
			if (etniaModelo::actualizar_etnia_modelo($dataetnia)) {
				$alerta=[
					"Alerta"=> "recargar",
					"Titulo"=> "DATOS ACTUALIZADOS!",
					"Texto"=> "DATOS ACTUALIZADOS CON EXITO!", 
					"Tipo"=> "success"
				];
			}else{
					$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "NO HEMOS PODIDO ACTUALIZAR, por favor intente nuevamente",
					"Tipo"=> "error"
				];

			}
			return mainModel::sweet_alert($alerta);

	}
	
}