<?php 

if ($peticionAjax) {
		# code...
	require_once "../modelos/administradorModelo.php";

}else{
	require_once "./modelos/administradorModelo.php";
}
	/**
	 * 
	 */
	class administradorControlador extends administradorModelo
	{
		// controlador para agregar administrador
		public function agregar_administrador_controlador(){

			$dni=mainModel::limpiar_cadena($_POST['dni-reg']);
			$nombre=mainModel::limpiar_cadena($_POST['nombre-reg']);
			$apellido=mainModel::limpiar_cadena($_POST['ape-reg']);
			$telefono=mainModel::limpiar_cadena($_POST['tel-reg']);
			$direccion=mainModel::limpiar_cadena($_POST['direc-reg']);

			$usuario=mainModel::limpiar_cadena($_POST['usuario-reg']);
			$password1=mainModel::limpiar_cadena($_POST['password1-reg']);
			$password2=mainModel::limpiar_cadena($_POST['password2-reg']);
			$email=mainModel::limpiar_cadena($_POST['email-reg']);
			$genero=mainModel::limpiar_cadena($_POST['optionsGenero']);
			// desencripta el privilegio revisar admin view radio value
			$privilegio=mainModel::decryption($_POST['optionsPrivilegio']);
			$privilegio=mainModel::limpiar_cadena($privilegio);

			if ($genero=="Masculino") {
				# code...
				$foto="male3avatar.png";
			}else{
				$foto="femaleavatar.png";
			}
			//comprar nivel de 1 - 3
			if ($privilegio<1 || $privilegio>3) {
				$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "El nivel de privilegio que intenta asignar es incorrecto",
					"Tipo"=> "error"
				];
				
			}else{
				if ($password1!=$password2) {
				# code...
				$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "Las contraseñas ingresadas no coinciden, por favor intente nuevamente",
					"Tipo"=> "error"
				];
			}else{
				$consulta1=mainModel::ejecutar_consulta_simple("SELECT AdminDNI FROM admin WHERE AdminDNI='$dni'");
				if ($consulta1->rowCount()>=1) {
					# code...
					$alerta=[
						"Alerta"=> "simple",
						"Titulo"=> "Ocurrio un error inesperado",
						"Texto"=> "El DNI que ingreso Ya existe",
						"Tipo"=> "error"
					];
				}
				else{
					if ($email!="") {
						# code...
						$consulta2=mainModel::ejecutar_consulta_simple("SELECT CuantaEmail FROM cuenta WHERE CuentaEmail='$email'");
						$ec=$consulta2->rowCount();

					}else{
						$ec=0;
					}
					if ($ec>=1) {
						# code...
						$alerta=[
							"Alerta"=> "simple",
							"Titulo"=> "Ocurrio un error inesperado",
							"Texto"=> "El EMAIL que ingreso Ya existe",
							"Tipo"=> "error"
						];
					}else{
						$consulta3=mainModel::ejecutar_consulta_simple("SELECT CuentaUsuario FROM cuenta WHERE CuentaUsuario='$usuario'");
						if ($consulta3->rowCount()>=1) {
							# code...
							$alerta=[
								"Alerta"=> "simple",
								"Titulo"=> "Ocurrio un error inesperado",
								"Texto"=> "El USUARIO que ingreso Ya existe",
								"Tipo"=> "error"
							];
						}else{
							/////////////////////////////////////
							$consulta4=mainModel::ejecutar_consulta_simple("SELECT id FROM cuenta");
							$numero=($consulta4->rowCount())+1;
							$codigo=mainModel::generrar_codigo_aleatorio("AC",2,$numero);

							$clave=mainModel::encryption($password1);

							$dataAC=[
								'Codigo'=>$codigo,
								'Privilegio'=>$privilegio,
								'Usuario'=>$usuario,
								'Clave'=>$clave,
								'Email'=>$email,
								'Estado'=>'Activo',
								'Tipo'=>'Administrador',
								'Genero'=>$genero,
								'Foto'=>$foto
							];
							$guardarCuenta=mainModel::agregar_cuenta($dataAC);
							///////////////////////
							if ($guardarCuenta->rowCount()>=1) {
								# code...
								$dataAD=[
									'DNI'=>$dni,
									'Nombre'=>$nombre,
									'Apellido'=>$apellido,
									'Telefono'=>$telefono,
									'Direccion'=>$direccion,
									'Codigo'=>$codigo

								];
								$guardaAdmin=administradorModelo::agregar_administrador_modelo($dataAD);

								if ($guardaAdmin->rowCount()>=1) {
									# code...
									$alerta=[
										"Alerta"=> "limpiar",
										"Titulo"=> "Administrador registrado",
										"Texto"=> "Administrador se registro",
										"Tipo"=> "succes"
									];
								}else{
									mainModel::eliminar_cuenta($codigo);
									$alerta=[
										"Alerta"=> "simple",
										"Titulo"=> "Ocurrio un error inesperado",
										"Texto"=> "El no hemos podido ingresar el administrador",
										"Tipo"=> "error"
									];

								}

							}else{
								$alerta=[
									"Alerta"=> "simple",
									"Titulo"=> "Ocurrio un error inesperado",
									"Texto"=> "El no hemos podido ingresar el administrador final " .$dni ." ". $nombre ." ". $apellido ." ". $telefono ." ". $direccion
									."cod cuenta ". $codigo ,
									"Tipo"=> "error"
								];

							}
						}
					}

				}
			}

			}
			
			return mainModel::sweet_alert($alerta);
		}	

		// controlador para paginar administradores
						// numer pagina / cantidad registros / cod de administrador
		public function paginador_administrador_controlador($pagina,$registros,$privilegio,$codigo,$busqueda){
			$pagina=mainModel::limpiar_cadena($pagina);
			$registros=mainModel::limpiar_cadena($registros);
			$privilegio=mainModel::limpiar_cadena($privilegio);
			$codigo=mainModel::limpiar_cadena($codigo);
			$busqueda==mainModel::limpiar_cadena($busqueda);
			$tabla="";
			//operador ternario (codicion)  el uno solo muestra el primer paginador 
			$pagina= (isset($pagina) && $pagina>0) ? (int) $pagina: 1;
		//comprobar cuantos registros queremos ver
			$inicio= ($pagina>0) ? (($pagina*$registros)-$registros) : 0 ;
			//validar cuando utilizamos el de buscar
				if (isset($busqueda) && $busqueda!="") {
					$consulta="SELECT SQL_CALC_FOUND_ROWS * FROM admin WHERE ((CuentaCodigo != '$codigo' AND id!='1') AND (AdminDNI LIKE '%$busqueda%' OR AdminNombre LIKE '%$busqueda%' OR AdminApellido LIKE '%$busqueda%' OR AdminTelefono LIKE '%$busqueda%'))  ORDER BY AdminNombre ASC LIMIT $inicio,$registros";
					//validar el directorio cuando es busqueda
					$paginaUrl="adminserach";
				}else{
					$consulta="SELECT SQL_CALC_FOUND_ROWS * FROM admin WHERE CuentaCodigo != '$codigo' AND id!='1' ORDER BY AdminNombre ASC LIMIT $inicio,$registros";
					//validar el directorio cuando es lista
					$paginaUrl="adminlist";
				}

			//1*5-5     0-4 5-9
			// hereda una conexion a la BD
			$conexion = mainModel::conectar();
			// CALCULAR LOS REGISTROS DE LA TABLA
			$datos= $conexion->query($consulta);
			// toma los valores de la consulta
			$datos=$datos->fetchAll();
			//SELECCION TODAS FILA ENCOONTRADAS
			$total=$conexion->query("SELECT FOUND_ROWS()");
			$total= (int) $total->fetchColumn();

			//total de paginas o paginador
			//ceil toma los enteros 
			//100reg / 15 = 6.66 paginas pero ceil redondea
			$Npaginas=ceil($total/$registros);
				//empiesa la tabla 
			$tabla.='<div class="table-responsive">
				<table class="table table-hover text-center">
					<thead>
					<tr>
					<th class="text-center">#</th>
					<th class="text-center">DNI</th>
					<th class="text-center">Nombre</th>
					<th class="text-center">Apellido</th>
					<th class="text-center">Telefono</th>
					<th class="text-center">Direccion</th>';	
					if ($privilegio<=2) {
						# code...
						$tabla.='
						<th class="text-center">Ac.Cuenta</th>
					<th class="text-center">Ac.Datos </th>';
					}if ($privilegio==1) {
						# code...
						$tabla.='
						
					<th class="text-center">Delete</th>';
					}
					
					

					$tabla.='</tr>
					</thead>
				<tbody>
			';
			if ($total>=1 && $pagina<=$Npaginas) {
				# code...
				$contador=$inicio+1;
				foreach ($datos as $rows) {
					$tabla.='
							<tr>
							<td>'.$contador.'</td>
							<td>'.$rows['AdminDNI'].'</td>
							<td>'.$rows['AdminNombre'].'</td>
							<td>'.$rows['AdminApellido'].'</td>
							<td>'.$rows['AdminTelefono'].'</td>
							<td>'.$rows['AdminDireccion'].'</td>
							';
								if ($privilegio<=2) {
									# code...
								
							$tabla.='
							<td><a href="'.SERVERURL.'myaccount/admin/'.mainModel::encryption($rows['CuentaCodigo']).'/" class="btn btn-success btn-raised btn-xs"><i class="zmdi zmdi-refresh"></i></a></td>
							<td><a href="'.SERVERURL.'mydata/admin/'.mainModel::encryption($rows['CuentaCodigo']).'/" class="btn btn-success btn-raised btn-xs"><i class="zmdi zmdi-refresh"></i></a></td>
							';
							}
							if ($privilegio==1) {
								# code...
							
							$tabla.='<td>
								<form action="'.SERVERURL.'ajax/administradorAjax.php" method="POST" class="FormularioAjax" data-form="delete" entype="multipart/form-data" autocomplete="off">
									<input type="hidden" name="codigo-del" value="'.mainModel::encryption($rows['CuentaCodigo']).'"> 
									<input type="hidden" name="privilegio-admin" value="'.mainModel::encryption($privilegio).'"> 
									<button type="submit" class="btn btn-danger btn-raised btn-xs">
										<i class="zmdi zmdi-delete"></i>
									</button>
									<div class="RespuestaAjax"></div>
								</form>
							</td>';
							}
							$tabla.='</tr>';
				$contador++;	
				}
			}else{
				if ($total>=1) {
					# code...
						$tabla.='
					<tr>
						<td colspan="5">
							<a href="'.SERVERURL.$paginaUrl.'/" class="btn btn-sm btn-info btn-raised">
							  Haga click aqui para recargar listado
							</a>
						</td>
					</tr>
				';
				}else{
					$tabla.='
					<tr>
					   <td colspan="5">No hay registro en el sistema</td>
					</tr>
				';	
				}
				
			}
			// termina la tabla 
       $tabla.='</tbody></table></div>	
			';	
			if ($total>=1 && $pagina<=$Npaginas) {
				$tabla.='
				<nav class="text-center">
					<ul class="pagination pagination-sm">
				';
				if ($pagina==1) {
					$tabla.='
			<li class="disabled"><a><i class = "zmdi zmdi-arrow-left"> </i></a></li>';
				}else{
					// validar los paginadores
					$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.($pagina-1).'/"><i class = "zmdi zmdi-arrow-left"> </i></a></li>';

				}
				// numeros de la paginacion del medio 123
					for($i=1; $i<=$Npaginas; $i++){
						if ($pagina==$i) {
							$tabla.='
			<li class="active"><a href="'.SERVERURL.$paginaUrl.'/'.$i.'/">'.$i.'</a></li>';
						}else{
							$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.$i.'/">'.$i.'</a></li>';

						}
					}
				// valida el ultimo paginador
				if ($pagina==$Npaginas) {
					$tabla.='
			<li class="disabled"><a><i class = "zmdi zmdi-arrow-right"> </i></a></li>';
				}else{
					// validar los paginadores
					$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.($pagina+1).'/"><i class = "zmdi zmdi-arrow-right"> </i></a></li>';

				}
				$tabla.='
					</ul>
				</nav>
				';
			}
			return $tabla;
		}
		public function eliminar_administrador_controlador(){
			$codigo=mainModel::decryption($_POST['codigo-del']);
			$adminprivilegio=mainModel::decryption($_POST['privilegio-admin']);

			$codigo=mainModel::limpiar_cadena($codigo);
			$adminprivilegio=mainModel::limpiar_cadena($adminprivilegio);
				// 1 si tiene control total
			if ($adminprivilegio==1) {
				# code...
				//valida que no se elimine el primer administrador
				$query1=mainModel::ejecutar_consulta_simple("SELECT id FROM admin WHERE CuentaCodigo='$codigo'");
				//array de datos de admin para ver el id
				$datosAdmin=$query1->fetch();
				if ($datosAdmin['id']!=1) {
					# code...
					$DelAdmin=administradorModelo::eliminar_administrador_modelo($codigo);
					mainModel::eliminar_bitacora($codigo);
					if ($DelAdmin->rowCount()>=1) {
						# code...
						$DelCuenta=mainModel::eliminar_cuenta($codigo);
						if ($DelCuenta->rowCount()==1) {
							# code...
									$alerta=[
					"Alerta"=> "recargar",
					"Titulo"=> "Administrador Eliminado",
					"Texto"=> "El administrador fue eliminado con exito del sistema",
					"Tipo"=> "success"
					];

						}else{
							$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "No podemos eliminar esta cuenta en este momento",
					"Tipo"=> "error"
					];
						}
					}else{
							$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "No podemos eliminar este administrador en este momento",
					"Tipo"=> "error"
				];

					}
				}else{
					$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "No poedemos eliminar el administrador principal del sistema",
					"Tipo"=> "error"
				];

				}

			}else{
				$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "Tu no tienes los permisos necesarios para realizar esta operacion",
					"Tipo"=> "error"
				];

			}
			return mainModel::sweet_alert($alerta);
		}
		// tipo: conteo o unico codido : cuentacodigo
		public function datos_administrador_controlador($tipo,$codigo){
			$codigo=mainModel::decryption($codigo);
			$tipo=mainModel::limpiar_cadena($tipo);

			return administradorModelo::datos_administrador_modelo($tipo,$codigo);
		}
		public function actualizar_administrador_controlador(){
		//codigo de la cuenta
		$cuenta=mainModel::decryption($_POST['cuenta-up']);
		    $dni=mainModel::limpiar_cadena($_POST['dni-up']);
			$nombre=mainModel::limpiar_cadena($_POST['nombre-up']);
			$apellido=mainModel::limpiar_cadena($_POST['ape-up']);
			$telefono=mainModel::limpiar_cadena($_POST['tel-up']);
			$direccion=mainModel::limpiar_cadena($_POST['direc-up']);

			$query1=mainModel::ejecutar_consulta_simple("SELECT * FROM admin WHERE CuentaCodigo='$cuenta'");
			//tiene todos los datos del administrador

			$datosAdmin=$query1->fetch();
			
			if ($dni!=$datosAdmin['AdminDNI']) {
				$consulta1=mainModel::ejecutar_consulta_simple("SELECT AdminDNI FROM Admin WHERE AdminDNI='$dni'");
				//cuantos registros se afectan
				if ($consulta1->rowCount()==1) {
					$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "DNI QUE ACABA DE INGRESAR YA ENCUENTRA REGISTRADO",
					"Tipo"=> "error"
				];
				return mainModel::sweet_alert($alerta);
				//detiene la ejecucion de la consultas
				exit();
				}
			}
			$dataAd=[
				"DNI"=>$dni,
				"Nombre"=>$nombre,
				"Apellido"=>$apellido,
				"Telefono"=>$telefono,
				"Direccion"=>$direccion,
				"Codigo"=>$cuenta
			];
			if (administradorModelo::actualizar_administrador_modelo($dataAd)) {
				$alerta=[
					"Alerta"=> "recargar",
					"Titulo"=> "DATOS ACTUALIZADOS!",
					"Texto"=> "DATOS ACTUALIZADOS CON EXITO!",
					"Tipo"=> "success"
				];
			}else{
					$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "NO HEMOS PODIDO ACTUALIZAR TUS DATOS, por favor intente nuevamente",
					"Tipo"=> "error"
				];

			}
			return mainModel::sweet_alert($alerta);

	}

	}