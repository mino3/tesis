<?php 

if ($peticionAjax) {
		# code...
	require_once "../modelos/familiaModelo.php";
}else{
	require_once "./modelos/familiaModelo.php";
}
/**
 * 
 */
class familiaControlador extends familiaModelo
{

	public function agregar_persona_controlador(){
		$cedula=mainModel::limpiar_cadena($_POST['dni-reg']);
		$Pnombre=mainModel::limpiar_cadena($_POST['1nombre-reg']);
		$Snombre=mainModel::limpiar_cadena($_POST['2nombre-reg']);
		$PApel=mainModel::limpiar_cadena($_POST['1ape-reg']);
		$SApel=mainModel::limpiar_cadena($_POST['2ape-reg']);
		$FechaN=mainModel::limpiar_cadena($_POST['fechaN-reg']);
		$Telf=mainModel::limpiar_cadena($_POST['telf-reg']);
		$Cell=mainModel::limpiar_cadena($_POST['celu-reg']);
		$email=mainModel::limpiar_cadena($_POST['email-reg']);
		$fecharegistro=date("Y-m-d h:i:s a");
		$DiscaSN=mainModel::limpiar_cadena($_POST['optionsDiscapacidad']);
		$estado=mainModel::limpiar_cadena($_POST['optionsEstado']);
		$adminfecha1=date("Y-m-d h:i:s a");
		$etnia=mainModel::limpiar_cadena($_POST['asetnia']);
		$estaCivil=mainModel::limpiar_cadena($_POST['ascivil']);
		$tipoSangre=mainModel::limpiar_cadena($_POST['asSangre']);
		$discapacidad=mainModel::limpiar_cadena($_POST['asDiscapa']);
		$nacionalidad=mainModel::limpiar_cadena($_POST['asNacion']);
		$canton=mainModel::limpiar_cadena($_POST['asCanton']);
		$provincia=mainModel::limpiar_cadena($_POST['asProvinc']);

		//informacionDomiciliaria
		//codigo
		$principal=mainModel::limpiar_cadena($_POST['CPrinci-reg']);
		$secundaria=mainModel::limpiar_cadena($_POST['CSecu-reg']);
		$numcasa=mainModel::limpiar_cadena($_POST['numcasa-reg']);
		$sector=mainModel::limpiar_cadena($_POST['sector-reg']);
		$postal="00000";
		$referencia=mainModel::limpiar_cadena($_POST['refer-reg']);
		//estado
		$adminfecha=date("Y-m-d h:i:s a");
		//persona

		//***********TIPO FAMILIA************//
		$tipofamilia=mainModel::limpiar_cadena($_POST['asTipofamilia']);

		

		if (mainModel::validarCedula($cedula)) {
			$consulta1=mainModel::ejecutar_consulta_simple("SELECT per_codigo FROM persona WHERE per_codigo='$cedula'");
			if ($consulta1->rowCount()==1) {
				$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio Un Error Inesperado",
					"Texto"=> "Cedula de Identidad ya se encuentra Registrada",
					"Tipo"=> "error"
				];
			}
			
			$consulta2=mainModel::ejecutar_consulta_simple("SELECT per_id FROM persona");
			$CodPer=($consulta2->rowCount())+1;
			//$codigo=mainModel::generrar_codigo_aleatorio("PRS",2,$CodPer);
			$dataPersona=[
				'ID'=>$CodPer,
				'Codigo'=>$cedula,
				'PNombre'=>$Pnombre,
				'SNombre'=>$Snombre,
				'PApellido'=>$PApel,
				'SApellido'=>$SApel,
				'FechaN'=>$FechaN,
				'Telefono'=>$Telf,
				'Celular'=>$Cell,
				'Correo'=>$email,
				'FechaR'=>$fecharegistro,
				'DiscapacidadSN'=>$DiscaSN,
				'Estado'=>$estado,
				'AdminFch1'=>$adminfecha1,
				'Etnia'=>$etnia,
				'EstadoCivil'=>$estaCivil,
				'TipoSangre'=>$tipoSangre,
				'Discapacidad'=>$discapacidad,
				'Nacionalidad'=>$nacionalidad,
				'Provincia'=>$provincia,
				'Canton'=>$canton				
			];
			$consulta3=mainModel::ejecutar_consulta_simple("SELECT dir_id FROM direccion");
			$numero=($consulta3->rowCount())+1;
			$codigo=mainModel::generrar_codigo_aleatorio("DRC",2,$numero);
			$dataDomicilio=[
				'ID'=>$numero,
				'Codigo'=>$codigo,
				'Principal'=>$principal,
				'Secundaria'=>$secundaria,
				'NumCasa'=>$numcasa,
				'Sector'=>$sector,
				'Postal'=>$postal,
				'Referencia'=>$referencia,
				'Estado'=>$estado,
				'AdminFecha'=>$adminfecha,
				'Persona'=>$CodPer
			];
			//******FAMILIA***********
			$consulta4=mainModel::ejecutar_consulta_simple("SELECT fa_id FROM familia");
			$numero=($consulta4->rowCount())+1;
			$codigo=mainModel::generrar_codigo_aleatorio("FAM",2,$numero);
			$dataFamilia=[
				'ID'=>$numero,
				'Codigo'=>$codigo,
				'Estado'=>$estado,
				'AdminFecha'=>$adminfecha1,
				'Persona'=>$CodPer,
				'TipoFamilia'=>$tipofamilia
			];
			if (familiaModelo::agregar_persona_modelo($dataPersona)) {
						# code...
				if (familiaModelo::domicilio_persona_modelo($dataDomicilio) ) {
							# code...
					if ( familiaModelo::agregar_familia_modelo($dataFamilia)) {
						# code...
					$alerta=[
						"Alerta"=> "limpiar",
						"Titulo"=> "DATOS INGRESADOS  ",
						"Texto"=> "PERSONA REGISTRADA!",
						"Tipo"=> "success"
					];
				}else{
					$alerta=[
						"Alerta"=> "simple",
						"Titulo"=> "DATOS NO INGRESADOS  ",
						"Texto"=> "PERSONA  NO REGISTRADA!",
						"Tipo"=> "error"
					];
				}

				}else{
					$alerta=[
						"Alerta"=> "simple",
						"Titulo"=> "OCUARRIO UN ERROR INESPERADO",
						"Texto"=> "DOMICILIO NO REGISTRADA!",
						"Tipo"=> "error"
					];

				}
				
			}else{	
				$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "OCUARRIO UN ERROR INESPERADO",
					"Texto"=> "PERSONA NO REGISTRADA!",
					"Tipo"=> "error"
				];
			}





			
		//validacion de la cedula 	
		}else{
			$alerta=[
				"Alerta"=> "simple",
				"Titulo"=> "INCORRECTO  ".$cedula,
				"Texto"=> "Cedula inCORRECTO",
				"Tipo"=> "error"
			];
		}
		return mainModel::sweet_alert($alerta);
	}

	public function paginador_persona_controlador($pagina,$registros,$privilegio,$codigo,$busqueda){
			$pagina=mainModel::limpiar_cadena($pagina);
			$registros=mainModel::limpiar_cadena($registros);
			$privilegio=mainModel::limpiar_cadena($privilegio);
			$codigo=mainModel::limpiar_cadena($codigo);
			$busqueda==mainModel::limpiar_cadena($busqueda);
			$tabla="";
			//operador ternario (codicion)  el uno solo muestra el primer paginador 
			$pagina= (isset($pagina) && $pagina>0) ? (int) $pagina: 1;
		//comprobar cuantos registros queremos ver
			$inicio= ($pagina>0) ? (($pagina*$registros)-$registros) : 0 ;
			//validar cuando utilizamos el de buscar
				if (isset($busqueda) && $busqueda!="") {
					//$consulta="SELECT SQL_CALC_FOUND_ROWS * FROM persona WHERE (per_codigo LIKE '%$busqueda%' OR per_nombre LIKE '%$busqueda%')  ORDER BY per_nombre ASC LIMIT $inicio,$registros";
					//validar el directorio cuando es busqueda
					$consulta="SELECT SQL_CALC_FOUND_ROWS f.fa_codigo,f.Persona_per_id,s.per_codigo,s.per_primernombre,s.per_segundonombre,s.per_primerapellido,s.per_segundoapellido,s.per_fechanacimiento,s.per_telefono,s.per_celular,s.per_correo,r.tifa_nombre,s.per_estado,s.per_discapacidad, s.Etnia_et_id,g.et_nombre, s.EstadioCivil_esci_id, e.esci_nombre, s.TipoSangre_tisa_id ,t.tisa_nombre,s.Discapacidad_dis_id, d.dis_nombre, s.Nacionalidad_nac_id, n.nac_pais, s.Canton_can_id,c.can_nombre, s.Provincia_prov_id,p.prov_nombre,f.TipoFamilia_tifa_id,f.fa_estado from familia f  
					 INNER JOIN persona s ON f.Persona_per_id=s.per_id
  INNER JOIN tipofamilia r ON f.TipoFamilia_tifa_id=r.tifa_id
  INNER JOIN etnia g ON s.Etnia_et_id=g.et_id
  INNER JOIN estadocivil e ON s.EstadioCivil_esci_id=e.esci_id
  INNER JOIN tiposangre t ON s.TipoSangre_tisa_id=t.tisa_id
  INNER JOIN discapacidad d ON s.Discapacidad_dis_id=d.dis_id
  INNER JOIN nacionalidad n On s.Nacionalidad_nac_id=n.nac_id
  INNER JOIN canton c ON s.Canton_can_id=c.can_id
  INNER JOIN provincia p ON s.Provincia_prov_id=p.prov_id WHERE(s.per_codigo LIKE '%$busqueda%' OR s.per_fechanacimiento LIKE '%$busqueda%' OR r.tifa_nombre LIKE '%$busqueda%' OR d.dis_nombre LIKE'%$busqueda%' OR n.nac_pais LIKE '%$busqueda%' OR c.can_nombre LIKE'%$busqueda%' OR f.fa_estado LIKE'%$busqueda%' OR g.et_nombre LIKE '%$busqueda%' OR e.esci_nombre LIKE '%$busqueda%' OR t.tisa_nombre LIKE'%$busqueda%' OR p.prov_nombre LIKE '%$busqueda%') ORDER BY s.per_primernombre ASC LIMIT $inicio,$registros";

					$paginaUrl="familiasearch";
				}else{					
					$consulta="SELECT SQL_CALC_FOUND_ROWS f.fa_codigo,f.Persona_per_id,s.per_codigo,s.per_id,s.per_primernombre,s.per_segundonombre,s.per_primerapellido,s.per_segundoapellido,s.per_fechanacimiento,s.per_telefono,s.per_celular,s.per_correo,r.tifa_nombre,s.per_estado,s.per_discapacidad, s.Etnia_et_id,g.et_nombre, s.EstadioCivil_esci_id, e.esci_nombre, s.TipoSangre_tisa_id ,t.tisa_nombre,s.Discapacidad_dis_id, d.dis_nombre, s.Nacionalidad_nac_id, n.nac_pais, s.Canton_can_id,c.can_nombre, s.Provincia_prov_id,p.prov_nombre,f.TipoFamilia_tifa_id,f.fa_estado from familia f  
  INNER JOIN persona s ON f.Persona_per_id=s.per_id
  INNER JOIN tipofamilia r ON f.TipoFamilia_tifa_id=r.tifa_id
  INNER JOIN etnia g ON s.Etnia_et_id=g.et_id
  INNER JOIN estadocivil e ON s.EstadioCivil_esci_id=e.esci_id
  INNER JOIN tiposangre t ON s.TipoSangre_tisa_id=t.tisa_id
  INNER JOIN discapacidad d ON s.Discapacidad_dis_id=d.dis_id
  INNER JOIN nacionalidad n On s.Nacionalidad_nac_id=n.nac_id
  INNER JOIN canton c ON s.Canton_can_id=c.can_id
  INNER JOIN provincia p ON s.Provincia_prov_id=p.prov_id
						
						ORDER BY per_primernombre ASC LIMIT $inicio,$registros";
					//validar el directorio cuando es lista
					$paginaUrl="familialist";
				}

			//1*5-5     0-4 5-9
			// hereda una conexion a la BD
			$conexion = mainModel::conectar();
			// CALCULAR LOS REGISTROS DE LA TABLA
			$datos= $conexion->query($consulta);
			// toma los valores de la consulta
			$datos=$datos->fetchAll();
			//SELECCION TODAS FILA ENCOONTRADAS
			$total=$conexion->query("SELECT FOUND_ROWS()");
			$total= (int) $total->fetchColumn();

			//total de paginas o paginador
			//ceil toma los enteros 
			//100reg / 15 = 6.66 paginas pero ceil redondea
			$Npaginas=ceil($total/$registros);
				//empiesa la tabla 
			$tabla.='<div class="table-responsive">
				<table class="table table-hover text-center">
					<thead>
					<tr>
					<th class="text-center">#</th>
					 <th class="text-center">Codigo</th>
					<th class="text-center">Nombres</th>
					<th class="text-center">Apellidos</th>
					<th class="text-center">Fecha Nacimiento</th>
					<th class="text-center">Telefono</th>
					<th class="text-center">Celular</th>
					<th class="text-center">Correo</th>
					<th class="text-center">Tipo Representante</th>
					<th class="text-center">Discapacidad S/N</th>
					<th class="text-center">Estado</th>
					<th class="text-center">Etnia</th>
					<th class="text-center">EstadoCivil</th>
					<th class="text-center">TipoSangre</th>
					<th class="text-center">Discapacidad</th>
					<th class="text-center">Nacionalidad</th>
					<th class="text-center">Provincia</th>
					<th class="text-center">Canton</th>

					';	
					if ($privilegio<=2) {
						# code...
						$tabla.='						
					<th class="text-center">Actualizar Datos Personales </th>
					<th class="text-center">Actualizar Datos Tipo familia </th>
					<th class="text-center">Ver Datos Direccion </th>

					';


					}if ($privilegio==1) {
						# code...
						$tabla.='
						
					<th class="text-center">Delete</th>';
					}
					
					$tabla.='</tr>
					</thead>
				<tbody>
			';
			if ($total>=1 && $pagina<=$Npaginas) {
				# code...
				$contador=$inicio+1;
				foreach ($datos as $rows) {
					$tabla.='
							<tr>
			<td>'.$contador.'</td>
			<td>'.$rows['per_codigo'].'</td>
			<td>'.$rows['per_primernombre']." ".$rows['per_segundonombre'].'</td>
			<td>'.$rows['per_primerapellido']." ".$rows['per_segundoapellido'].'</td>	
				<td>'.$rows['per_fechanacimiento'].'</td>
				<td>'.$rows['per_telefono'].'</td>
				<td>'.$rows['per_celular'].'</td>
				<td>'.$rows['per_correo'].'</td>
				<td>'.$rows['tifa_nombre'].'</td>
				<td>'.$rows['per_discapacidad'].'</td>
				<td>'.$rows['fa_estado'].'</td>
				<td>'.$rows['et_nombre'].'</td>
				<td>'.$rows['esci_nombre'].'</td>
				<td>'.$rows['tisa_nombre'].'</td>
				<td>'.$rows['dis_nombre'].'</td>
				<td>'.$rows['nac_pais'].'</td>
				<td>'.$rows['prov_nombre'].'</td>
				<td>'.$rows['can_nombre'].'</td>									
							';
								if ($privilegio<=2) {
									# code...
								
							$tabla.='
							<td><a href="'.SERVERURL.'familiaUp/familia/'.mainModel::encryption($rows['per_codigo']).'/" class="btn btn-success btn-raised btn-xs"><i class="zmdi zmdi-refresh"></i></a></td>
							<td><a href="'.SERVERURL.'tipofamUp/tipofamilia/'.mainModel::encryption($rows['fa_codigo']).'/" class="btn btn-success btn-raised btn-xs"><i class="zmdi zmdi-view-dashboard"> </i></a></td>
							<td><a href="'.SERVERURL.'direccionUp/direccion/'.mainModel::encryption($rows['per_id']).'/" class="btn btn-success btn-raised btn-xs"><i class="zmdi zmdi-view-dashboard"> </i></a></td>

							';
							}
							if ($privilegio==1) {
								# code...
							
							$tabla.='<td>
								<form action="'.SERVERURL.'ajax/familiaAjax.php" method="POST" class="FormularioAjax" data-form="delete" entype="multipart/form-data" autocomplete="off">
									<input type="hidden" name="codigo-del" value="'.mainModel::encryption($rows['fa_codigo']).'"> 
									<input type="hidden" name="privilegio-admin" value="'.mainModel::encryption($privilegio).'"> 
									<button type="submit" class="btn btn-danger btn-raised btn-xs">
										<i class="zmdi zmdi-delete"></i>
									</button>
									<div class="RespuestaAjax"></div>
								</form>
							</td>';
							}
							$tabla.='</tr>';
				$contador++;	
				}
			}else{
				if ($total>=1) {
					# code...
						$tabla.='
					<tr>
						<td colspan="5">
							<a href="'.SERVERURL.$paginaUrl.'/" class="btn btn-sm btn-info btn-raised">
							  Haga click aqui para recargar listado
							</a>
						</td>
					</tr>
				';
				}else{
					$tabla.='
					<tr>
					   <td colspan="5">No hay registro en el sistema</td>
					</tr>
				';	
				}
				
			}
			// termina la tabla 
       $tabla.='</tbody></table></div>	
			';	
			if ($total>=1 && $pagina<=$Npaginas) {
				$tabla.='
				<nav class="text-center">
					<ul class="pagination pagination-sm">
				';
				if ($pagina==1) {
					$tabla.='
			<li class="disabled"><a><i class = "zmdi zmdi-arrow-left"> </i></a></li>';
				}else{
					// validar los paginadores
					$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.($pagina-1).'/"><i class = "zmdi zmdi-arrow-left"> </i></a></li>';

				}
				// numeros de la paginacion del medio 123
					for($i=1; $i<=$Npaginas; $i++){
						if ($pagina==$i) {
							$tabla.='
			<li class="active"><a href="'.SERVERURL.$paginaUrl.'/'.$i.'/">'.$i.'</a></li>';
						}else{
							$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.$i.'/">'.$i.'</a></li>';

						}
					}
				// valida el ultimo paginador
				if ($pagina==$Npaginas) {
					$tabla.='
			<li class="disabled"><a><i class = "zmdi zmdi-arrow-right"> </i></a></li>';
				}else{
					// validar los paginadores
					$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.($pagina+1).'/"><i class = "zmdi zmdi-arrow-right"> </i></a></li>';

				}
				$tabla.='
					</ul>
				</nav>
				';
			}
			return $tabla;
		}
		public function eliminar_familia_controlador(){
			$codigo=mainModel::decryption($_POST['codigo-del']);
			$adminprivilegio=mainModel::decryption($_POST['privilegio-admin']);

			$codigo=mainModel::limpiar_cadena($codigo);
			$adminprivilegio=mainModel::limpiar_cadena($adminprivilegio);
				// 1 si tiene control total
			if ($adminprivilegio==1) {
				# code...
				//valida que no se elimine el primer administrador
				$query1=mainModel::ejecutar_consulta_simple("SELECT fa_id FROM familia WHERE fa_id='$codigo'");
				//array de datos de admin para ver el id
				$datosAdmin=$query1->fetch();
				if ($datosAdmin['fa_id']!=1) {
					# code...
					$DelIdio=familiaModelo::eliminar_familia_modelo($codigo);
					
					if ($DelIdio->rowCount()>=1) {
						# code...
						//$DelCuenta=mainModel::eliminar_cuenta($codigo);
						$alerta=[
					"Alerta"=> "recargar",
					"Titulo"=> "REPRESENTANTE ELIMINADO",
					"Texto"=> "REPRESENTANTE Eliminado",
					"Tipo"=> "success"
				];
						
					}else{
							$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "No podemos eliminar este REPRESENTANTE en este momento",
					"Tipo"=> "error"
				];

					}
				}else{
					$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "No podemos eliminar el REPRESENTANTE principal del sistema",
					"Tipo"=> "error"
				];

				}

			}else{
				$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "Tu no tienes los permisos necesarios para realizar esta operacion",
					"Tipo"=> "error"
				];

			}
			return mainModel::sweet_alert($alerta);
	}
	public function datos_familia_controlador($tipo,$codigo){
			$codigo=mainModel::decryption($codigo);
			$tipo=mainModel::limpiar_cadena($tipo);

			return familiaModelo::datos_familia_modelo($tipo,$codigo);
		}
	public function datos_direccion_controlador($tipo,$codigo){
			$codigo=mainModel::decryption($codigo);
			$tipo=mainModel::limpiar_cadena($tipo);

			return familiaModelo::datos_direccion_modelo($tipo,$codigo);
		}	
	public function actulizar_persona_controlador(){
		$cuenta=mainModel::decryption($_POST['cuenta-up']);
		$cedula=mainModel::limpiar_cadena($_POST['dni-up']);
		$Pnombre=mainModel::limpiar_cadena($_POST['1nombre-up']);
		$Snombre=mainModel::limpiar_cadena($_POST['2nombre-up']);
		$PApel=mainModel::limpiar_cadena($_POST['1ape-up']);
		$SApel=mainModel::limpiar_cadena($_POST['2ape-up']);
		$FechaN=mainModel::limpiar_cadena($_POST['fechaN-up']);
		$Telf=mainModel::limpiar_cadena($_POST['telf-up']);
		$Cell=mainModel::limpiar_cadena($_POST['celu-up']);
		$email=mainModel::limpiar_cadena($_POST['email-up']);
		$fecharegistro=date("Y-m-d h:i:s a");
		$DiscaSN=mainModel::limpiar_cadena($_POST['optionsDiscapacidad-up']);
		$estado=mainModel::limpiar_cadena($_POST['optionsEstado-up']);
		$adminfecha1=date("Y-m-d h:i:s a");
		$etnia=mainModel::limpiar_cadena($_POST['asetnia-up']);
		$estaCivil=mainModel::limpiar_cadena($_POST['ascivil-up']);
		$tipoSangre=mainModel::limpiar_cadena($_POST['asSangre-up']);
		$discapacidad=mainModel::limpiar_cadena($_POST['asDiscapa-up']);
		$nacionalidad=mainModel::limpiar_cadena($_POST['asNacion-up']);
		$canton=mainModel::limpiar_cadena($_POST['asCanton-up']);
		$provincia=mainModel::limpiar_cadena($_POST['asProvinc-up']);

		if (mainModel::validarCedula($cedula)) {
			$query1=mainModel::ejecutar_consulta_simple("SELECT * FROM persona WHERE per_codigo='$cuenta'");
			//tiene todos los datos del administrador
			$datosPersona=$query1->fetch();			
			if ($cedula!=$datosPersona['per_codigo']) {
				$consulta1=mainModel::ejecutar_consulta_simple("SELECT per_codigo FROM persona WHERE per_codigo='$cedula'");
				//cuantos registros se afectan
				if ($consulta1->rowCount()==1) {
					$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "DNI QUE ACABA DE INGRESAR YA ENCUENTRA REGISTRADO",
					"Tipo"=> "error"
				];
				return mainModel::sweet_alert($alerta);
				//detiene la ejecucion de la consultas
				exit();
				}
			}
				if ($email!=$datosPersona['per_correo']) {
						$consulta2=mainModel::ejecutar_consulta_simple("SELECT per_correo FROM persona WHERE per_correo='$email'");
						$ec=$consulta2->rowCount();

					}else{
						$ec=0;
					}
					if ($ec>=1) {
						# code...
						$alerta=[
							"Alerta"=> "simple",
							"Titulo"=> "Ocurrio un error inesperado",
							"Texto"=> "El EMAIL que ingreso Ya existe",
							"Tipo"=> "error"
						];
						return mainModel::sweet_alert($alerta);
				//detiene la ejecucion de la consultas
				exit();
					}else{
			$dataPersona=[				
				'PNombre'=>$Pnombre,
				'SNombre'=>$Snombre,
				'PApellido'=>$PApel,
				'SApellido'=>$SApel,
				'FechaN'=>$FechaN,
				'Telefono'=>$Telf,
				'Celular'=>$Cell,
				'Correo'=>$email,
				'FechaR'=>$fecharegistro,
				'DiscapacidadSN'=>$DiscaSN,
				'Estado'=>$estado,
				'AdminFch1'=>$adminfecha1,
				'Etnia'=>$etnia,
				'EstadoCivil'=>$estaCivil,
				'TipoSangre'=>$tipoSangre,
				'Discapacidad'=>$discapacidad,
				'Nacionalidad'=>$nacionalidad,
				'Provincia'=>$provincia,
				'Canton'=>$canton,
				'Codigo'=>$cedula				
			];			
			if (familiaModelo::actualizar_persona_modelo($dataPersona)) {
				# code...
				$alerta=[
				"Alerta"=> "limpiar",
				"Titulo"=> "DATOS ACTUALIZADOS" .$cedula,
				"Texto"=> "Datos Actualizados!".$cuenta,
				"Tipo"=> "success"
			];
			}else{
				$alerta=[
				"Alerta"=> "simple",
				"Titulo"=> "DATOS NO ACTUALIZADOS",
				"Texto"=> "Datos No Actualizados!",
				"Tipo"=> "error"
			];

			}
			}
	}else{
			$alerta=[
				"Alerta"=> "simple",
				"Titulo"=> "INCORRECTO  ".$cedula,
				"Texto"=> "Cedula inCORRECTO",
				"Tipo"=> "error"
			];
		}
		return mainModel::sweet_alert($alerta);
 }
 public function actualizar_direccion_controlador(){
 		$cuenta=mainModel::decryption($_POST['direccion-up']);
		$principal=mainModel::limpiar_cadena($_POST['CPrinci-up']);
		$secundaria=mainModel::limpiar_cadena($_POST['CSecu-up']);
		$numcasa=mainModel::limpiar_cadena($_POST['numcasa-up']);
		$sector=mainModel::limpiar_cadena($_POST['sector-up']);
		$postal="00000";
		$referencia=mainModel::limpiar_cadena($_POST['refer-up']);
		$estado=mainModel::limpiar_cadena($_POST['optionsEstado-up']);
		//estado
			$dataDomicilio=[						
				'Principal'=>$principal,
				'Secundaria'=>$secundaria,
				'NumCasa'=>$numcasa,
				'Sector'=>$sector,
				'Postal'=>$postal,
				'Referencia'=>$referencia,
				'Estado'=>$estado,								
				'Codigo'=>$cuenta
			];
			if (familiaModelo::actualizar_direccion_modelo($dataDomicilio)) {
				$alerta=[
					"Alerta"=> "recargar",
					"Titulo"=> "DATOS ACTUALIZADOS!",
					"Texto"=> "DATOS ACTUALIZADOS CON EXITO!", 
					"Tipo"=> "success"
				];
			}else{
					$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "NO HEMOS PODIDO ACTUALIZAR, por favor intente nuevamente",
					"Tipo"=> "error"
				];

			}
			return mainModel::sweet_alert($alerta);

	}
	public function datos_fam_controlador($tipo,$codigo){
			$codigo=mainModel::decryption($codigo);
			$tipo=mainModel::limpiar_cadena($tipo);

			return familiaModelo::datos_fam_modelo($tipo,$codigo);
		}
	 public function actualizar_familia_controlador(){
 		$cuenta=mainModel::decryption($_POST['codigo-up']);
 		$tipofam=mainModel::limpiar_cadena($_POST['asTipofamilia-up']);
 		$estado=mainModel::limpiar_cadena($_POST['optionsEstado-up']);

 		 $datatipofam=[
 		 	'Estado'=>$estado,
 		 	'TipoFamilia'=>$tipofam,
 		 	'Codigo'=>$cuenta
 		 ];
 		 echo $estado." ".$tipofam." ".$cuenta;
 		 if (familiaModelo::actualizar_familia_modelo($datatipofam)) {
 		 	$alerta=[
					"Alerta"=> "recargar",
					"Titulo"=> "DATOS ACTUALIZADOS!",
					"Texto"=> "DATOS ACTUALIZADOS CON EXITO!", 
					"Tipo"=> "success"
				];
 		 }else{
 		 	$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "NO HEMOS PODIDO ACTUALIZAR, por favor intente nuevamente",
					"Tipo"=> "error"];
 		 }
 		 return mainModel::sweet_alert($alerta);
 	}		
}