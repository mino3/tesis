<?php 
if ($peticionAjax) {
		# code...
	require_once "../modelos/jornadaModelo.php";

}else{
	require_once "./modelos/jornadaModelo.php";
}
/**
 * 
 */
class jornadaControlador extends jornadaModelo
{
	public function agregar_jornada_controlador()	{
		$nombre=mainModel::limpiar_cadena($_POST['nombre-reg']);
		$descripcion=mainModel::limpiar_cadena($_POST['desc-reg']);
		$estado=mainModel::limpiar_cadena($_POST['optionsEstado']);
		$fecha=date("Y-m-d h:i:s a");

		$consulta1=mainModel::ejecutar_consulta_simple("SELECT jor_nombre FROM jornadas WHERE jor_nombre='$nombre'");
		if ($consulta1->rowCount()>=1) {
			$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "La Jornada que ingreso Ya existe",
					"Tipo"=> "error"
			];
		}else{
			$consulta2=mainModel::ejecutar_consulta_simple("SELECT jor_id FROM jornadas");
					$numero=($consulta2->rowCount())+1;
					$codigo=mainModel::generrar_codigo_aleatorio("JOR",2,$numero);

				$dataJornada=[
					'ID'=>$numero,
					'Codigo'=>$codigo,
					'Nombre'=>$nombre,
					'Descripcion'=>$descripcion,
					'Estado'=>$estado,
					'AdminFeccha'=>$fecha
				];
				if (jornadaModelo::agregar_jornada_modelo($dataJornada)) {
								# code...
								$alerta=[
										"Alerta"=> "limpiar",
										"Titulo"=> "Jornada registrado",
										"Texto"=> "Jornada se registro",
										"Tipo"=> "success" 
									];	
							}else{
								$alerta=[
										"Alerta"=> "limpiar",
										"Titulo"=> "Jornada no registrado",
										"Texto"=> "Jornada no registro",
										"Tipo"=> "success" 
									];	

							}
		}
		return mainModel::sweet_alert($alerta);
	}
	public function paginador_jornada_controlador($pagina,$registros,$privilegio,$codigo,$busqueda){
			$pagina=mainModel::limpiar_cadena($pagina);
			$registros=mainModel::limpiar_cadena($registros);
			$privilegio=mainModel::limpiar_cadena($privilegio);
			$codigo=mainModel::limpiar_cadena($codigo);
			$busqueda==mainModel::limpiar_cadena($busqueda);
			$tabla="";
			//operador ternario (codicion)  el uno solo muestra el primer paginador 
			$pagina= (isset($pagina) && $pagina>0) ? (int) $pagina: 1;
		//comprobar cuantos registros queremos ver
			$inicio= ($pagina>0) ? (($pagina*$registros)-$registros) : 0 ;
			//validar cuando utilizamos el de buscar
				if (isset($busqueda) && $busqueda!="") {
					$consulta="SELECT SQL_CALC_FOUND_ROWS * FROM jornadas WHERE (jor_codigo LIKE '%$busqueda%' OR jor_nombre LIKE '%$busqueda%')  ORDER BY jor_nombre ASC LIMIT $inicio,$registros";
					//validar el directorio cuando es busqueda
					$paginaUrl="jornadasearch";
				}else{
					$consulta="SELECT SQL_CALC_FOUND_ROWS * FROM jornadas WHERE jor_id != '$codigo'  ORDER BY jor_nombre ASC LIMIT $inicio,$registros";
					//validar el directorio cuando es lista
					$paginaUrl="jornadalist";
				}

			//1*5-5     0-4 5-9
			// hereda una conexion a la BD
			$conexion = mainModel::conectar();
			// CALCULAR LOS REGISTROS DE LA TABLA
			$datos= $conexion->query($consulta);
			// toma los valores de la consulta
			$datos=$datos->fetchAll();
			//SELECCION TODAS FILA ENCOONTRADAS
			$total=$conexion->query("SELECT FOUND_ROWS()");
			$total= (int) $total->fetchColumn();

			//total de paginas o paginador
			//ceil toma los enteros 
			//100reg / 15 = 6.66 paginas pero ceil redondea
			$Npaginas=ceil($total/$registros);
				//empiesa la tabla 
			$tabla.='<div class="table-responsive">
				<table class="table table-hover text-center">
					<thead>
					<tr>
					<th class="text-center">#</th>
					 <th class="text-center">Codigo</th>
					<th class="text-center">Nombre</th>
					<th class="text-center">Descripcion</th>					
					<th class="text-center">Estado</th>
					';	
					if ($privilegio<=2) {
						# code...
						$tabla.='						
					<th class="text-center">Actualizar </th>';
					}if ($privilegio==1) {
						# code...
						$tabla.='
						
					<th class="text-center">Delete</th>';
					}
					
					$tabla.='</tr>
					</thead>
				<tbody>
			';
			if ($total>=1 && $pagina<=$Npaginas) {
				# code...
				$contador=$inicio+1;
				foreach ($datos as $rows) {
					$tabla.='
							<tr>
							<td>'.$contador.'</td>
							<td>'.$rows['jor_codigo'].'</td>
							<td>'.$rows['jor_nombre'].'</td>
							<td>'.$rows['jor_descripcion'].'</td>						
							<td>'.$rows['jor_estado'].'</td>					
							';
								if ($privilegio<=2) {
									# code...
								
							$tabla.='
							<td><a href="'.SERVERURL.'jornadaUp/jornada/'.mainModel::encryption($rows['jor_codigo']).'/" class="btn btn-success btn-raised btn-xs"><i class="zmdi zmdi-refresh"></i></a></td>							
							';
							}
							if ($privilegio==1) {
								# code...
							
							$tabla.='<td>
								<form action="'.SERVERURL.'ajax/jornadaAjax.php" method="POST" class="FormularioAjax" data-form="delete" entype="multipart/form-data" autocomplete="off">
									<input type="hidden" name="codigo-del" value="'.mainModel::encryption($rows['jor_codigo']).'"> 
									<input type="hidden" name="privilegio-admin" value="'.mainModel::encryption($privilegio).'"> 
									<button type="submit" class="btn btn-danger btn-raised btn-xs">
										<i class="zmdi zmdi-delete"></i>
									</button>
									<div class="RespuestaAjax"></div>
								</form>
							</td>';
							}
							$tabla.='</tr>';
				$contador++;	
				}
			}else{
				if ($total>=1) {
					# code...
						$tabla.='
					<tr>
						<td colspan="5">
							<a href="'.SERVERURL.$paginaUrl.'/" class="btn btn-sm btn-info btn-raised">
							  Haga click aqui para recargar listado
							</a>
						</td>
					</tr>
				';
				}else{
					$tabla.='
					<tr>
					   <td colspan="5">No hay registro en el sistema</td>
					</tr>
				';	
				}
				
			}
			// termina la tabla 
       $tabla.='</tbody></table></div>	
			';	
			if ($total>=1 && $pagina<=$Npaginas) {
				$tabla.='
				<nav class="text-center">
					<ul class="pagination pagination-sm">
				';
				if ($pagina==1) {
					$tabla.='
			<li class="disabled"><a><i class = "zmdi zmdi-arrow-left"> </i></a></li>';
				}else{
					// validar los paginadores
					$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.($pagina-1).'/"><i class = "zmdi zmdi-arrow-left"> </i></a></li>';

				}
				// numeros de la paginacion del medio 123
					for($i=1; $i<=$Npaginas; $i++){
						if ($pagina==$i) {
							$tabla.='
			<li class="active"><a href="'.SERVERURL.$paginaUrl.'/'.$i.'/">'.$i.'</a></li>';
						}else{
							$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.$i.'/">'.$i.'</a></li>';

						}
					}
				// valida el ultimo paginador
				if ($pagina==$Npaginas) {
					$tabla.='
			<li class="disabled"><a><i class = "zmdi zmdi-arrow-right"> </i></a></li>';
				}else{
					// validar los paginadores
					$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.($pagina+1).'/"><i class = "zmdi zmdi-arrow-right"> </i></a></li>';

				}
				$tabla.='
					</ul>
				</nav>
				';
			}
			return $tabla;
		}
	


	public function eliminar_jornada_controlador(){			
			$codigo=mainModel::decryption($_POST['codigo-del']);
			$adminprivilegio=mainModel::decryption($_POST['privilegio-admin']);

			$codigo=mainModel::limpiar_cadena($codigo);
			$adminprivilegio=mainModel::limpiar_cadena($adminprivilegio);
				// 1 si tiene control total
			if ($adminprivilegio==1) {
				# code...
				//valida que no se elimine el primer administrador
				$query1=mainModel::ejecutar_consulta_simple("SELECT jor_id FROM jornadas WHERE jor_id='$codigo'");
				//array de datos de admin para ver el id
				$datosAdmin=$query1->fetch();
				if ($datosAdmin['jor_id']!=1) {
					# code...
					$DelIdio=jornadaModelo::eliminar_jornada_modelo($codigo);
					
					if ($DelIdio->rowCount()>=1) {
						# code...
						//$DelCuenta=mainModel::eliminar_cuenta($codigo);
						$alerta=[
					"Alerta"=> "recargar",
					"Titulo"=> "Jornada ELIMINADO",
					"Texto"=> "Jornada Eliminado",
					"Tipo"=> "success"
				];
						
					}else{
							$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "No podemos eliminar este Jornada en este momento",
					"Tipo"=> "error"
				];

					}
				}else{
					$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "No poedemos eliminar el Jornada principal del sistema",
					"Tipo"=> "error"
				];

				}

			}else{
				$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "Tu no tienes los permisos necesarios para realizar esta operacion",
					"Tipo"=> "error"
				];

			}
			return mainModel::sweet_alert($alerta);
	}
	public function datos_jornada_controlador($tipo,$codigo){
			$codigo=mainModel::decryption($codigo);
			$tipo=mainModel::limpiar_cadena($tipo);

			return jornadaModelo::datos_jornada_modelo($tipo,$codigo);
		}
	public function actualizar_jornada_controlador(){
		$cuenta=mainModel::decryption($_POST['codigo-up']);
		$nombre=mainModel::limpiar_cadena($_POST['nombre-up']);
		$descripcion=mainModel::limpiar_cadena($_POST['desc-up']);
		$estado=mainModel::limpiar_cadena($_POST['optionsEstado-up']);
		$query1=mainModel::ejecutar_consulta_simple("SELECT * FROM jornadas WHERE jor_codigo='$cuenta'");
			//tiene todos los datos del jornadas
			$datosjornada=$query1->fetch();		
			if ($nombre!=$datosjornada['jor_nombre']) {
		$consulta1=mainModel::ejecutar_consulta_simple("SELECT jor_nombre FROM jornadas WHERE jor_nombre='$nombre'");
				//cuantos registros se afectan
				if ($consulta1->rowCount()==1) {
					$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado". $nombre,
					"Texto"=> "NOMBRE QUE ACABA DE INGRESAR YA SE ENCUENTRA REGISTRADO",
					"Tipo"=> "error"
				];
				return mainModel::sweet_alert($alerta);
				//detiene la ejecucion de la consultas
				exit();
				}
			}
			$datajornada=[				
				"Nombre"=>$nombre,
				"Descripcion"=>$descripcion,				
				"Estado"=>$estado,
				"Codigo"=>$cuenta
			];
			if (jornadaModelo::actualizar_jornada_modelo($datajornada)) {
				$alerta=[
					"Alerta"=> "recargar",
					"Titulo"=> "DATOS ACTUALIZADOS!",
					"Texto"=> "DATOS ACTUALIZADOS CON EXITO!", 
					"Tipo"=> "success"
				];
			}else{
					$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "NO HEMOS PODIDO ACTUALIZAR, por favor intente nuevamente",
					"Tipo"=> "error"
				];

			}
			return mainModel::sweet_alert($alerta);
	}

	
}