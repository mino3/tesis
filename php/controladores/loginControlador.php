<?php 
if ($peticionAjax) {
		# code...
		require_once "../modelos/loginModelo.php";

	}else{
		require_once "./modelos/loginModelo.php";
	}
	/**
	 * 
	 */
	class loginControlador extends loginModelo
	{
		public function inciar_sesion_controlador(){
			$usuario=mainModel::limpiar_cadena($_POST['usuario']);
			$clave=mainModel::limpiar_cadena($_POST['clave']);

			$clave=mainModel::encryption($clave);



			$datoslogin=[
					"Usuario"=>$usuario,
					"Clave"=>$clave
			];

			$datosCuenta=loginModelo::inciar_sesion_modelo($datoslogin);
			if ($datosCuenta->rowCount()==1) {
				# code...
				$row=$datosCuenta->fetch();
				$fechaActual=date("Y-m-d");
				$yearActual=date("Y");
				$horaActual=date("h:i:s a");

				$consulta1=mainModel::ejecutar_consulta_simple("SELECT id FROM bitacora");
				$numero=($consulta1->rowCount())+1;

				$codigoB=mainModel::generrar_codigo_aleatorio("CB",2,$numero);

				$datosBitacora=[
						"Codigo"=>$codigoB,
						"Fecha"=>$fechaActual,
						"HoraIncio"=>$horaActual,
						"HoraFinal"=>"sin registrro",
						"Tipo"=>$row['CuentaTipo'],
						"Year"=>$yearActual,
						"Cuenta"=>$row['CuentaCodigo']
				];
				$insertarBitacora=mainModel::guardar_bitacora($datosBitacora); 
				if ($insertarBitacora->rowCount()>=1) {

					if ($row['CuentaTipo']=="Administrador") {
						$query1=mainModel::ejecutar_consulta_simple("SELECT * FROM admin 	WHERE CuentaCodigo='".$row['CuentaCodigo']."'");
					}else{
						//$query1=mainModel::ejecutar_consulta_simple("SELECT * FROM persona 	WHERE CuentaCodigo='".$row['CuentaCodigo']."'");

					}
					if ($query1->rowCount()==1) {
						session_start(['name'=>'SE']);
						$UserData=$query1->fetch();

					if ($row['CuentaTipo']=="Administrador") {
						$_SESSION['nombre_se']=$UserData['AdminNombre'];
						$_SESSION['apellido_se']=$UserData['AdminApellido'];
					}else{
						$_SESSION['nombre_se']=$UserData['per_primernombre'];
						$_SESSION['apellido_se']=$UserData['per_primerapellido'];
					}
						$_SESSION['usuario_se']=$row['CuentaUsuario'];
						$_SESSION['tipo_se']=$row['CuentaTipo'];
						$_SESSION['privilegio_se']=$row['CuentaPrivilegio'];
						$_SESSION['foto_se']=$row['CuentaFoto'];
						$_SESSION['token_se']=md5(uniqid(mt_rand(),true));
						$_SESSION['codigo_cuenta_se']=$row['CuentaCodigo'];
						$_SESSION['codigo_bitacora_se']=$codigoB;

						if ($row['CuentaTipo']=="Administrador") {
							# code...
							$url=SERVERURL."home/";
						}else{
							$url=SERVERURL."canton/";
						}

						return $urLocation='<script> window.location="'.$url.'" </script>';
					}else{
						$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "No hemos podido Iniciar sessión por problemas Técnicos, por favor intente nueva mente  " ,
					"Tipo"=> "error"
				];
				return mainModel::sweet_alert($alerta);


					}
					



				}else{
					$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "No hemos podido Iniciar sessión por problemas Técnicos, por favor intente nueva mente  " ,
					"Tipo"=> "error"
				];
				return mainModel::sweet_alert($alerta);

				}
			}else{
				$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "El nombre de usuario y contraseña no son correctos o la su cuenta puede estar deshabilitada ",
					"Tipo"=> "error"
				];
				return mainModel::sweet_alert($alerta);

			}
		}

		public function forzar_cierre_session_controlador(){			
			session_unset();
			session_destroy();
			$redirect='<script> window.location.href="'.SERVERURL.'" </script>';
			return $redirect;
			//return header("Location: ".SERVERURL);
			
		}
		public function cerrar_sesion_controlador(){
				session_start(['name'=>'SE']);
				$token=mainModel::decryption($_GET['Token']);				
				//muestra hora minuto segundo y am o pm
				$hora=date("h:i:s a");
				$datos=[
					"Usuario"=>$_SESSION['usuario_se'],
					"Token_S"=>$_SESSION['token_se'],
					"Token"=>$token,
					"Codigo"=>$_SESSION['codigo_bitacora_se'],
					"Hora"=>$hora
				];
				return loginModelo::cerrar_sesion_modelo($datos);

		}
		public function redireccionar_usuario_controlador($tipo){
			if ($tipo="Administrador") {
				$redirect='<script> window.location.href="'.SERVERURL.'home/" </script>';
			}else{
				//reditect
			}
			return $redirect;
		}

	}