<?php 
// mainModel esta el modelo de la cuenta
if ($peticionAjax) {
		# code...
	require_once "../core/mainModel.php";

}else{
	require_once "./core/mainModel.php";
}
	/**
	 * 
	 */
	class cuentaControlador extends mainModel
	{
		
		public function datos_cuenta_controlador($codigo,$tipo){
			$codigo=mainModel::decryption($codigo);
			$tipo=mainModel::limpiar_cadena($tipo);
			//admin o user 
			if ($tipo=="admin") {
				$tipo="Administrador";
			}else{
				$tipo="Cliente";
		    }


			return mainModel::datos_cuenta($codigo,$tipo);
		}
		public function actualizar_controlador(){
			$CuentaCodigo=mainModel::decryption($_POST['CodigoCuenta-up']);
			$CuentaTipo=mainModel::decryption($_POST['TipoCuenta-up']);

			$query1=mainModel::ejecutar_consulta_simple("SELECT * FROM cuenta WHERE CuentaCodigo='$CuentaCodigo'");
			//array de datos de tabla cuenta
			$DatosCuenta=$query1->fetch();

			$user=mainModel::limpiar_cadena($_POST['user-log']);
			$password=mainModel::limpiar_cadena($_POST['password-log']);
			$password=mainModel::encryption($password);
			 //comprobar el user y pass
			if ($user!="" && $password!="") {
				if (isset($_POST['privilegio-up'])) {
					$login=mainModel::ejecutar_consulta_simple("SELECT id FROM cuenta WHERE CuentaUsuario='$user' AND CuentaClave='$password'");
				}else{
			 			//usuario para actualizar su propia cuenta
					$login=mainModel::ejecutar_consulta_simple("SELECT id FROM cuenta WHERE CuentaUsuario='$user' AND CuentaClave='$password' AND CuentaCodigo='$CuentaCodigo'");
				}

				if ($login->rowCount()==0) {
					$alerta=[
						"Alerta"=> "simple",
						"Titulo"=> "Ocurrio un error inesperado",
						"Texto"=> "El nombre de usuario y clave que acaba de ingresar no coinciden con los datos de su cuenta",
						"Tipo"=> "error"
					];
					return mainModel::sweet_alert($alerta);
					exit();
				}

			}else{
				$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "Para actualizar los datos de la cuenta debe ingresar el nombre de usuario y clave, por favor ingrese los datos Intente nuevamente",
					"Tipo"=> "error"
				];
				return mainModel::sweet_alert($alerta);
				exit();

			}
			 // VERIFICAR USUARIO 
			$CuentaUsuario=mainModel::limpiar_cadena($_POST['usuario-up']);

			if ($CuentaUsuario!=$DatosCuenta['CuentaUsuario']) {
			 	// viene del formulario cuenta usuario post[usuario up]
				$query2=mainModel::ejecutar_consulta_simple("SELECT CuentaUsuario FROM cuenta WHERE CuentaUsuario='$CuentaUsuario'");
				if ($query2->rowCount()>=1) {
					$alerta=[
						"Alerta"=> "simple",
						"Titulo"=> "Ocurrio un error inesperado",
						"Texto"=> "El nombre de usuario que acaba de ingresar ya se encuentra registrado en el sistema",
						"Tipo"=> "error"
					];
					return mainModel::sweet_alert($alerta);
					exit();
				}

			}
			 // verificar email
			$CuentaEmail=mainModel::limpiar_cadena($_POST['email-up']);

			if ($CuentaEmail!=$DatosCuenta['CuentaEmail']) {
			 	// viene del formulario cuenta usuario post[usuario up]
				$query3=mainModel::ejecutar_consulta_simple("SELECT CuentaEmail FROM cuenta WHERE CuentaEmail='$CuentaEmail'");
				if ($query3->rowCount()>=1) {
					$alerta=[
						"Alerta"=> "simple",
						"Titulo"=> "Ocurrio un error inesperado",
						"Texto"=> "El email de usuario que acaba de ingresar ya se encuentra registrado en el sistema",
						"Tipo"=> "error"
					];
					return mainModel::sweet_alert($alerta);
					exit();
				}

			}

			 // GENERO

			$CuentaGenero=mainModel::limpiar_cadena($_POST['optionsGenero-up']);
			if (isset($_POST['optionsEstado-up'])) {
				$CuentaEstado=mainModel::limpiar_cadena($_POST['optionsEstado-up']);
			}else{
				$CuentaEstado=$DatosCuenta['CuentaEstado'];
			}
			if ($CuentaTipo=="admin") {
			 	//si viene definido desde el formulario el privilegio
				if (isset($_POST['optionsPrivilegio-up'])) {
					$CuentaPrivilegio=mainModel::decryption($_POST['optionsPrivilegio-up']);
				}else{
					$CuentaPrivilegio=$DatosCuenta['CuentaPrivilegio'];
				}
				if ($CuentaGenero=="Masculino") {
			 			$CuentaFoto="male3avatar.png";
				}else{
					$CuentaFoto="femaleavatar.png";
				}
			}else{
				$CuentaPrivilegio=$DatosCuenta['CuentaPrivilegio'];
				// clientes 
				if ($CuentaGenero=="Masculino") {
			 			$CuentaFoto="male3avatar.png";
				}else{
					$CuentaFoto="femaleavatar.png";
				}
			}


			// VERIFICACION CAMBIO DE CLAVE
			$passwordN1=mainModel::limpiar_cadena($_POST['newPassword1-up']);
			$passwordN2=mainModel::limpiar_cadena($_POST['newPassword2-up']);

			if ($passwordN1!="" || $passwordN2!="") {

				if ($passwordN1==$passwordN2) {
					$CuentaClave=mainModel::encryption($passwordN1);
				}else{
					$alerta=[
						"Alerta"=> "simple",
						"Titulo"=> "Ocurrio un error inesperado",
						"Texto"=> "Las nuevas contraseñas no coinciden, por favor verifique los datos ",
						"Tipo"=> "error"
					];
					return mainModel::sweet_alert($alerta);
					exit();

				}
				
			}else{
				$CuentaClave=$DatosCuenta['CuentaClave'];
			}
			// enviando los datos al modelo
			$datosUpdate=[
				"CuentaPrivilegio"=>$CuentaPrivilegio,
				"CuentaCodigo"=>$CuentaCodigo,
				"CuentaUsuario"=>$CuentaUsuario,
				"CuentaClave"=>$CuentaClave,
				"CuentaEmail"=>$CuentaEmail,
				"CuentaEstado"=>$CuentaEstado,
				"CuentaGenero"=>$CuentaGenero,
				"CuentaFoto"=>$CuentaFoto
			];

			if (mainModel::actualizar_cuenta($datosUpdate)) {
				if (!isset($_POST['privilegio-up'])) {
					session_start(['name'=>'SE']);
					$_SESSION['usuario_se']=$CuentaUsuario;
					$_SESSION['foto_se']=$CuentaFoto;
				}
				$alerta=[
						"Alerta"=> "recargar",
						"Titulo"=> "DATOS ACTUALIZADOS",
						"Texto"=> "CON EXITO!",
						"Tipo"=> "success"
					];
				
			}else{
				$alerta=[
						"Alerta"=> "simple",
						"Titulo"=> "Ocurrio un error inesperado",
						"Texto"=> "No se pudo actualizar los datos de la cuenta",
						"Tipo"=> "error"
					];		

			}
			return mainModel::sweet_alert($alerta);


		}
	}