<?php 
if ($peticionAjax) {
		# code...
	require_once "../modelos/discapacidadModelo.php";

}else{
	require_once "./modelos/discapacidadModelo.php";
}
/**
 * 
 */
class discapacidadControlador extends discapacidadModelo
{
		public function agregar_discapacidad_controlador(){
		$nombre=mainModel::limpiar_cadena($_POST['nombre-reg']);
		$descripcion=mainModel::limpiar_cadena($_POST['desc-reg']);
		$carnet=mainModel::limpiar_cadena($_POST['canet-reg']);
		$porcentaje=mainModel::limpiar_cadena($_POST['porcentaje']);
		$estado=mainModel::limpiar_cadena($_POST['optionsEstado']);		
		$fechaActual=date("Y-m-d h:i:s a");			
		
		$consulta1=mainModel::ejecutar_consulta_simple("SELECT dis_nombre FROM discapacidad WHERE dis_nombre='$nombre'");
			if ($consulta1->rowCount()>=1) {		
				$alerta=[
								"Alerta"=> "simple",
								"Titulo"=> "Ocurrio un error inesperado",
								"Texto"=> "La Discapacidad que ingreso Ya existe",
								"Tipo"=> "error"
							];
			}else{
				        $consulta3=mainModel::ejecutar_consulta_simple("SELECT dis_id FROM discapacidad");
							$numero=($consulta3->rowCount())+1;
							$codigo=mainModel::generrar_codigo_aleatorio("DIS",2,$numero);

							$dataDis=[
								'id'=>$numero,
								'codigo'=>$codigo,
								'nombre'=>$nombre,
								'descripcion'=>$descripcion,
								'carnet'=>$carnet,								
								'estado'=>$estado,
								'adminfecha1'=>$fechaActual,
								'Porcentaje'=>$porcentaje

							];					
							if (discapacidadModelo::agregar_discapacidad_modelo($dataDis)) {
								# code...
								$alerta=[
										"Alerta"=> "limpiar",
										"Titulo"=> "Discapacidad registrado",
										"Texto"=> "Discapacidad se registro",
										"Tipo"=> "success" 
									];	
							}else{
								$alerta=[
										"Alerta"=> "limpiar",
										"Titulo"=> "Discapacidad no registrado",
										"Texto"=> "Discapacidad no registro",
										"Tipo"=> "success" 
									];	

							}

			}
		 return mainModel::sweet_alert($alerta);
	}
	public function paginador_discapacidad_controlador($pagina,$registros,$privilegio,$codigo,$busqueda){
			$pagina=mainModel::limpiar_cadena($pagina);
			$registros=mainModel::limpiar_cadena($registros);
			$privilegio=mainModel::limpiar_cadena($privilegio);
			$codigo=mainModel::limpiar_cadena($codigo);
			$busqueda==mainModel::limpiar_cadena($busqueda);
			$tabla="";
			//operador ternario (codicion)  el uno solo muestra el primer paginador 
			$pagina= (isset($pagina) && $pagina>0) ? (int) $pagina: 1;
		//comprobar cuantos registros queremos ver
			$inicio= ($pagina>0) ? (($pagina*$registros)-$registros) : 0 ;
			//validar cuando utilizamos el de buscar
				if (isset($busqueda) && $busqueda!="") {
					$consulta="SELECT SQL_CALC_FOUND_ROWS * FROM discapacidad WHERE (dis_codigo LIKE '%$busqueda%' OR dis_nombre LIKE '%$busqueda%')  ORDER BY dis_nombre ASC LIMIT $inicio,$registros";
					//validar el directorio cuando es busqueda
					$paginaUrl="discapacidadsearch";
				}else{
					$consulta="SELECT SQL_CALC_FOUND_ROWS * FROM discapacidad WHERE dis_id != '$codigo'  ORDER BY dis_nombre ASC LIMIT $inicio,$registros";
					//validar el directorio cuando es lista
					$paginaUrl="discapacidadlist";
				}

			//1*5-5     0-4 5-9
			// hereda una conexion a la BD
			$conexion = mainModel::conectar();
			// CALCULAR LOS REGISTROS DE LA TABLA
			$datos= $conexion->query($consulta);
			// toma los valores de la consulta
			$datos=$datos->fetchAll();
			//SELECCION TODAS FILA ENCOONTRADAS
			$total=$conexion->query("SELECT FOUND_ROWS()");
			$total= (int) $total->fetchColumn();

			//total de paginas o paginador
			//ceil toma los enteros 
			//100reg / 15 = 6.66 paginas pero ceil redondea
			$Npaginas=ceil($total/$registros);
				//empiesa la tabla 
			$tabla.='<div class="table-responsive">
				<table class="table table-hover text-center">
					<thead>
					<tr>
					<th class="text-center">#</th>
					 <th class="text-center">Codigo</th>
					<th class="text-center">Nombre</th>
					<th class="text-center">Descripcion</th>
					<th class="text-center">Carnet</th>
					<th class="text-center">Porcentaje</th>	
					<th class="text-center">Estado</th>							
					';	
					if ($privilegio<=2) {
						# code...
						$tabla.='											
					<th class="text-center">Actualizar </th>';
					}if ($privilegio==1) {
						# code...
						$tabla.='
						
					<th class="text-center">Delete</th>';
					}
					
					$tabla.='</tr>
					</thead>
				<tbody>
			';
			if ($total>=1 && $pagina<=$Npaginas) {
				# code...
				$contador=$inicio+1;
				foreach ($datos as $rows) {
					$tabla.='
							<tr>
							<td>'.$contador.'</td>
							<td>'.$rows['dis_codigo'].'</td>
							<td>'.$rows['dis_nombre'].'</td>
							<td>'.$rows['dis_descripcion'].'</td>
							<td>'.$rows['dis_carnetconadis'].'</td>
							<td>'.$rows['Porcetaje_por_id'].'</td>							
							<td>'.$rows['dis_estado'].'</td>					
							';
								if ($privilegio<=2) {
									# code...
								
							$tabla.='
							<td><a href="'.SERVERURL.'discapacidadUp/discapacidad/'.mainModel::encryption($rows['dis_codigo']).'/" class="btn btn-success btn-raised btn-xs"><i class="zmdi zmdi-refresh"></i></a></td>							
							';
							}
							if ($privilegio==1) {
								# code...
							
							$tabla.='<td>
								<form action="'.SERVERURL.'ajax/discapacidadAjax.php" method="POST" class="FormularioAjax" data-form="delete" entype="multipart/form-data" autocomplete="off">
									<input type="hidden" name="codigo-del" value="'.mainModel::encryption($rows['dis_codigo']).'"> 
									<input type="hidden" name="privilegio-admin" value="'.mainModel::encryption($privilegio).'"> 
									<button type="submit" class="btn btn-danger btn-raised btn-xs">
										<i class="zmdi zmdi-delete"></i>
									</button>
									<div class="RespuestaAjax"></div>
								</form>
							</td>';
							}
							$tabla.='</tr>';
				$contador++;	
				}
			}else{
				if ($total>=1) {
					# code...
						$tabla.='
					<tr>
						<td colspan="5">
							<a href="'.SERVERURL.$paginaUrl.'/" class="btn btn-sm btn-info btn-raised">
							  Haga click aqui para recargar listado
							</a>
						</td>
					</tr>
				';
				}else{
					$tabla.='
					<tr>
					   <td colspan="5">No hay registro en el sistema</td>
					</tr>
				';	
				}
				
			}
			// termina la tabla 
       $tabla.='</tbody></table></div>	
			';	
			if ($total>=1 && $pagina<=$Npaginas) {
				$tabla.='
				<nav class="text-center">
					<ul class="pagination pagination-sm">
				';
				if ($pagina==1) {
					$tabla.='
			<li class="disabled"><a><i class = "zmdi zmdi-arrow-left"> </i></a></li>';
				}else{
					// validar los paginadores
					$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.($pagina-1).'/"><i class = "zmdi zmdi-arrow-left"> </i></a></li>';

				}
				// numeros de la paginacion del medio 123
					for($i=1; $i<=$Npaginas; $i++){
						if ($pagina==$i) {
							$tabla.='
			<li class="active"><a href="'.SERVERURL.$paginaUrl.'/'.$i.'/">'.$i.'</a></li>';
						}else{
							$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.$i.'/">'.$i.'</a></li>';

						}
					}
				// valida el ultimo paginador
				if ($pagina==$Npaginas) {
					$tabla.='
			<li class="disabled"><a><i class = "zmdi zmdi-arrow-right"> </i></a></li>';
				}else{
					// validar los paginadores
					$tabla.='
			<li><a href="'.SERVERURL.$paginaUrl.'/'.($pagina+1).'/"><i class = "zmdi zmdi-arrow-right"> </i></a></li>';

				}
				$tabla.='
					</ul>
				</nav>
				';
			}
			return $tabla;
		}
	


	public function eliminar_discapacidad_controlador(){
			$codigo=mainModel::decryption($_POST['codigo-del']);
			$adminprivilegio=mainModel::decryption($_POST['privilegio-admin']);

			$codigo=mainModel::limpiar_cadena($codigo);
			$adminprivilegio=mainModel::limpiar_cadena($adminprivilegio);
				// 1 si tiene control total
			if ($adminprivilegio==1) {
				# code...
				//valida que no se elimine el primer administrador
				$query1=mainModel::ejecutar_consulta_simple("SELECT dis_id FROM discapacidad WHERE dis_id='$codigo'");
				//array de datos de admin para ver el id
				$datosAdmin=$query1->fetch();
				if ($datosAdmin['dis_id']!=1) {
					# code...
					$DelIdio=discapacidadModelo::eliminar_discapacidad_modelo($codigo);
					
					if ($DelIdio->rowCount()>=1) {
						# code...
						//$DelCuenta=mainModel::eliminar_cuenta($codigo);
						$alerta=[
					"Alerta"=> "recargar",
					"Titulo"=> "DISCAPACIDAD ELIMINADO",
					"Texto"=> "DISCAPACIDAD Eliminado",
					"Tipo"=> "success"
				];
						
					}else{
							$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "No podemos eliminar este DISCAPACIDAD en este momento",
					"Tipo"=> "error"
				];

					}
				}else{
					$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "No poedemos eliminar el DISCAPACIDAD principal del sistema",
					"Tipo"=> "error"
				];

				}

			}else{
				$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "Tu no tienes los permisos necesarios para realizar esta operacion",
					"Tipo"=> "error"
				];

			}
			return mainModel::sweet_alert($alerta);
	}
	public function datos_discapacidad_controlador($tipo,$codigo){
			$codigo=mainModel::decryption($codigo);
			$tipo=mainModel::limpiar_cadena($tipo);

			return discapacidadModelo::datos_discapacidad_modelo($tipo,$codigo);
		}
		public function actualizar_discapacidad_controlador(){
		//codigo de la cuenta
		//$codigo=
		$cuenta=mainModel::decryption($_POST['codigo-up']);
		$nombre=mainModel::limpiar_cadena($_POST['nombre-up']);
		$descripcion=mainModel::limpiar_cadena($_POST['desc-up']);
		$carnet=mainModel::limpiar_cadena($_POST['canet-up']);
		$porcentaje=mainModel::limpiar_cadena($_POST['porcentaje-up']);
		$estado=mainModel::limpiar_cadena($_POST['optionsEstado-up']);
			
		$query1=mainModel::ejecutar_consulta_simple("SELECT * FROM discapacidad WHERE dis_codigo='$cuenta'");
			//tiene todos los datos del discapacidad
			$datosdiscapacidad=$query1->fetch();		
			if ($nombre!=$datosdiscapacidad['dis_nombre']) {
		$consulta1=mainModel::ejecutar_consulta_simple("SELECT dis_nombre FROM discapacidad WHERE dis_nombre='$nombre'");
				//cuantos registros se afectan
				if ($consulta1->rowCount()==1) {
					$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado". $nombre,
					"Texto"=> "NOMBRE QUE ACABA DE INGRESAR YA SE ENCUENTRA REGISTRADO",
					"Tipo"=> "error"
				];
				return mainModel::sweet_alert($alerta);
				//detiene la ejecucion de la consultas
				exit();
				}
			}
			$dataDis=[				
				"Nombre"=>$nombre,
				"Descripcion"=>$descripcion,
				"Carnet"=>$carnet,
				"Estado"=>$estado,
				"Porcentaje"=>$porcentaje,
				"Codigo"=>$cuenta
			];
			if (discapacidadModelo::actualizar_discapacidad_modelo($dataDis)) {
				$alerta=[
					"Alerta"=> "recargar",
					"Titulo"=> "DATOS ACTUALIZADOS!",
					"Texto"=> "DATOS ACTUALIZADOS CON EXITO!", 
					"Tipo"=> "success"
				];
			}else{
					$alerta=[
					"Alerta"=> "simple",
					"Titulo"=> "Ocurrio un error inesperado",
					"Texto"=> "NO HEMOS PODIDO ACTUALIZAR, por favor intente nuevamente",
					"Tipo"=> "error"
				];

			}
			return mainModel::sweet_alert($alerta);

	}
	
}