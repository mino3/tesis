<?php 

	/**
	 * 
	 AQUI ESTAN TODAS LAS FUNCIONES QUE SE EJECUTAN REPETIDAMENTE A LO LARGO DEL SISTEMA
	 */
	 if ($peticionAjax) {
		# code...
	 	require_once "../core/configAPP.php";

	 }else{
	 	require_once "./core/configAPP.php";
	 }
	 class mainModel{
	 	protected function validarTercerDigito($numero, $tipo)
	 	{
	 		switch ($tipo) {
	 			case 'cedula':
	 			case 'ruc_natural':
	 			if ($numero < 0 OR $numero > 5) {
	 				throw new Exception('Tercer dígito debe ser mayor o igual a 0 y menor a 6 para cédulas y RUC de persona natural');
	 			}
	 			break;
	 			case 'ruc_privada':
	 			if ($numero != 9) {
	 				throw new Exception('Tercer dígito debe ser igual a 9 para sociedades privadas');
	 			}
	 			break;

	 			case 'ruc_publica':
	 			if ($numero != 6) {
	 				throw new Exception('Tercer dígito debe ser igual a 6 para sociedades públicas');
	 			}
	 			break;
	 			default:
	 			throw new Exception('Tipo de Identificación no existe.');
	 			break;
	 		}

	 		return true;
	 	}

	 	protected function algoritmoModulo10($digitosIniciales, $digitoVerificador)
	 	{
	 		$arrayCoeficientes = array(2,1,2,1,2,1,2,1,2);

	 		$digitoVerificador = (int)$digitoVerificador;
	 		$digitosIniciales = str_split($digitosIniciales);

	 		$total = 0;
	 		foreach ($digitosIniciales as $key => $value) {

	 			$valorPosicion = ( (int)$value * $arrayCoeficientes[$key] );

	 			if ($valorPosicion >= 10) {
	 				$valorPosicion = str_split($valorPosicion);
	 				$valorPosicion = array_sum($valorPosicion);
	 				$valorPosicion = (int)$valorPosicion;
	 			}

	 			$total = $total + $valorPosicion;
	 		}

	 		$residuo =  $total % 10;

	 		if ($residuo == 0) {
	 			$resultado = 0;
	 		} else {
	 			$resultado = 10 - $residuo;
	 		}

	 		if ($resultado != $digitoVerificador) {
	 			throw new Exception('Dígitos iniciales no validan contra Dígito Idenficador');
	 		}

	 		return true;
	 	}



	 	protected function validarCodigoProvincia($numero)
	 	{
	 		if ($numero < 0 OR $numero > 24) {
	 			throw new Exception('Codigo de Provincia (dos primeros dígitos) no deben ser mayor a 24 ni menores a 0');
	 		}

	 		return true;
	 	}
	 	protected function validarInicial($numero, $caracteres)
	 	{
	 		if (empty($numero)) {
	 			throw new Exception('Valor no puede estar vacio');
	 		}

	 		if (!ctype_digit($numero)) {
	 			throw new Exception('Valor ingresado solo puede tener dígitos');
	 		}

	 		if (strlen($numero) != $caracteres) {
	 			throw new Exception('Valor ingresado debe tener '.$caracteres.' caracteres');
	 		}

	 		return true;
	 	}
	 	protected function validarCedula($numero){
        // fuerzo parametro de entrada a string
	 		$numero = (string)$numero;

        // borro por si acaso errores de llamadas anteriores.
	 		//$this->setError('');

        // validaciones
	 		try {
	 			$this->validarInicial($numero, '10');
	 			$this->validarCodigoProvincia(substr($numero, 0, 2));
	 			$this->validarTercerDigito($numero[2], 'cedula');
	 			$this->algoritmoModulo10(substr($numero, 0, 9), $numero[9]);
	 		} catch (Exception $e) {
	 			$e->getMessage();
	 			return false;
	 		}

	 		return true;
	 	}

	 	protected function conectar(){
	 		$enlace = new PDO(SGBD,USER,PASS);
	 		return $enlace;
	 	}

	 	protected function ejecutar_consulta_simple($consulta){
	 		$respuesta=self::conectar()->prepare($consulta);
	 		$respuesta->execute();

	 		return $respuesta;

	 	}

	 	protected function agregar_cuenta($datos){
	 		$sql=self::conectar()->prepare('INSERT INTO cuenta(CuentaCodigo,CuentaPrivilegio,CuentaUsuario,CuentaClave,CuentaEmail,CuentaEstado,CuentaTipo,CuentaGenero,CuentaFoto) VALUES (:Codigo,:Privilegio,:Usuario,:Clave,:Email,:Estado,:Tipo,:Genero,:Foto)');

	 		$sql->bindParam(':Codigo',$datos['Codigo']);
	 		$sql->bindParam(':Privilegio',$datos['Privilegio']);
	 		$sql->bindParam(':Usuario',$datos['Usuario']);
	 		$sql->bindParam(':Clave',$datos['Clave']);
	 		$sql->bindParam(':Email',$datos['Email']);
	 		$sql->bindParam(':Estado',$datos['Estado']);
	 		$sql->bindParam(':Tipo',$datos['Tipo']);
	 		$sql->bindParam(':Genero',$datos['Genero']);
	 		$sql->bindParam(':Foto',$datos['Foto']);

	 		$sql->execute();

	 		return $sql;
	 	}

	 	protected function eliminar_cuenta($codigo){
	 		$sql=self::conectar()->prepare('DELETE FROM cuenta WHERE CuentaCodigo=:Codigo ');
	 		$sql->bindParam(':Codigo',$codigo);

	 		$sql->execute();
	 		return $sql;
	 	}
		//seleccion de datos 
	 	protected function datos_cuenta($codigo,$tipo){
	 		$query=self::conectar()->prepare("SELECT * FROM cuenta WHERE CuentaCodigo=:Codigo AND CuentaTipo=:Tipo");
	 		$query->bindParam(":Codigo",$codigo);
	 		$query->bindParam(":Tipo",$tipo);
	 		$query->execute();
	 		return $query;
	 	}
	 	protected function actualizar_cuenta($datos){
	 		$query=self::conectar()->prepare("UPDATE cuenta SET CuentaPrivilegio=:Privilegio,CuentaUsuario=:Usuario,CuentaClave=:Clave,CuentaEmail=:Email,CuentaEstado=:Estado,CuentaGenero=:Genero,CuentaFoto=:Foto WHERE CuentaCodigo=:Codigo");
	 		$query->bindParam(":Privilegio",$datos['CuentaPrivilegio']);
	 		$query->bindParam(":Usuario",$datos['CuentaUsuario']);
	 		$query->bindParam(":Clave",$datos['CuentaClave']);
	 		$query->bindParam(":Email",$datos['CuentaEmail']);
	 		$query->bindParam(":Estado",$datos['CuentaEstado']);
	 		$query->bindParam(":Genero",$datos['CuentaGenero']);
	 		$query->bindParam(":Foto",$datos['CuentaFoto']);
	 		$query->bindParam(":Codigo",$datos['CuentaCodigo']);
	 		$query->execute();
	 		return $query;
	 	}

	 	protected function guardar_bitacora($datos){
	 		$sql=self::conectar()->prepare('INSERT INTO bitacora(BitacoraCodigo,BitacoraFecha,BitacoraHoraInicio,BitacoraHoraFinal,BitacoraTipo,BitacoraYear,CuentaCodigo) VALUES (:Codigo,:Fecha,:HoraIncio,:HoraFinal,:Tipo,:Year,:Cuenta)');
	 		$sql->bindParam(':Codigo',$datos['Codigo']);
	 		$sql->bindParam(':Fecha',$datos['Fecha']);
	 		$sql->bindParam(':HoraIncio',$datos['HoraIncio']);
	 		$sql->bindParam(':HoraFinal',$datos['HoraFinal']);
	 		$sql->bindParam(':Tipo',$datos['Tipo']);
	 		$sql->bindParam(':Year',$datos['Year']);
	 		$sql->bindParam(':Cuenta',$datos['Cuenta']);
	 		$sql->execute();

	 		return $sql;

	 	}

	 	protected function actualizar_bitacora($codigo,$hora){
	 		$sql=self::conectar()->prepare("UPDATE bitacora SET BitacoraHoraFinal=:Hora WHERE BitacoraCodigo=:Codigo");
	 		$sql->bindParam(":Hora",$hora);
	 		$sql->bindParam(":Codigo",$codigo);

	 		$sql->execute();

	 		return $sql;


	 	}	  
	 	protected function eliminar_bitacora($codigo){
	 		$sql=self::conectar()->prepare("DELETE FROM bitacora WHERE CuentaCodigo=:Codigo");
	 		$sql->bindParam(':Codigo',$codigo);
	 		$sql->execute();

	 		return $sql;


	 	}

	 	public function encryption($string){
	 		$output=FALSE;
	 		$key=hash('sha256', SECRET_KEY);
	 		$iv=substr(hash('sha256', SECRET_IV), 0,16);
	 		$output=openssl_encrypt($string,METHOD, $key,0,$iv);
	 		$output=base64_encode($output);
	 		return $output;
	 	}
	 	public function decryption($string){
	 		$key=hash('sha256', SECRET_KEY);
	 		$iv=substr(hash('sha256', SECRET_IV), 0,16);
	 		$output=openssl_decrypt(base64_decode($string),METHOD,$key,0,$iv);
	 		return $output;
	 	}
	 	protected function generrar_codigo_aleatorio($letra,$longitud,$num){
	 		for ($i=1; $i<=$longitud ; $i++) { 
	 			$numero = rand(0,9);
	 			$letra.=$numero;
	 		}
	 		return $letra."-".$num;

	 	}

	 	protected function limpiar_cadena($cadena){
	 		$cadena=trim($cadena);
	 		$cadena=stripcslashes($cadena);
	 		$cadena=str_ireplace("<script>", "", $cadena);
	 		$cadena=str_ireplace("</script>", "", $cadena);
	 		$cadena=str_ireplace("<script src", "", $cadena);
	 		$cadena=str_ireplace("<script type=", "", $cadena);
	 		$cadena=str_ireplace("SELECT * FROM", "", $cadena);
	 		$cadena=str_ireplace("DELETE FROM", "", $cadena);
	 		$cadena=str_ireplace("INSERT INTO", "", $cadena);
	 		$cadena=str_ireplace("--", "", $cadena);
	 		$cadena=str_ireplace("^", "", $cadena);
	 		$cadena=str_ireplace("[", "", $cadena);
	 		$cadena=str_ireplace("]", "", $cadena);
	 		$cadena=str_ireplace("==", "", $cadena);
	 		$cadena=str_ireplace(";", "", $cadena);
	 		return $cadena;

	 	}

	 	protected function sweet_alert($datos){
	 		if ($datos['Alerta']=="simple") {
		 		# code... alaryea que sale 
	 			$alerta="
	 			<script>
	 			swal(
	 			'".$datos['Titulo']."',
	 			'".$datos['Texto']."',
	 			'".$datos['Tipo']."'
	 			);
	 			</script>
	 			";
	 		}elseif ($datos['Alerta']=="recargar") {
				# code...
	 			$alerta="
	 			<script>
	 			swal({
	 				title: '".$datos['Titulo']."',
	 				text: '".$datos['Texto']."',
	 				type: '".$datos['Tipo']."',
	 				confirmButtonText: 'Aceptar'
	 				}).then( function()
	 				{
	 					location.reload();

	 					});
	 					</script>
	 					";
	 				}elseif ($datos['Alerta']=="limpiar") {
				# code...
	 					$alerta="
	 					<script>
	 					swal({
	 						title: '".$datos['Titulo']."',
	 						text: '".$datos['Texto']."',
	 						type: '".$datos['Tipo']."',
	 						confirmButtonText: 'Aceptar'
	 						}).then( function()
	 						{
	 							$('.FormularioAjax')[0].reset();

	 							});
	 							</script>
	 							";
	 						}
	 						return $alerta;

	 					}

	 				}