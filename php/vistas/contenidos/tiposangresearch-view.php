<div class="container-fluid">
	<div class="page-header">
		<h1 class="text-titles"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Users <small>Etnia</small></h1>
	</div>
	<p class="lead">TIPOS DE SANGRE </p>
</div>
<div class="container-fluid">
	<ul class="breadcrumb breadcrumb-tabs">
		<!-- BOTON NUEVO -->
		<li>
			<a href="<?php echo SERVERURL;?>tiposangre/" class="btn btn-info">
				<i class="zmdi zmdi-plus"></i> &nbsp; NUEVO TIPO SANGRE
			</a>
		</li>
		<!-- BOTON LISTAR -->
		<li>
			<a href="<?php echo SERVERURL;?>tiposangrelist/" class="btn btn-success">
				<i class="zmdi zmdi-format-list-bulleted"></i> &nbsp; LISTA TIPO SANGRE
			</a>
		</li>
		<!-- BOTON BUSCAR -->
		<li>
			<a href="<?php echo SERVERURL;?>tiposangresearch/" class="btn btn-primary">
				<i class="zmdi zmdi-search"></i> &nbsp; BUSCAR TIPO SANGRE
			</a>
		</li>
	</ul>
</div>

<?php 
		require_once "./controladores/tiposangreControlador.php";
		$insAdmin= new tiposangreControlador();
		if (isset($_POST['busqueda_inicial_tiposangre'])) {
			//almacenar el valor que enviar el formulario en una variable de session
			$_SESSION['busqueda_tiposangre']=$_POST['busqueda_inicial_tiposangre'];
		}
		//elimina la variable de session 
		if (isset($_POST['eliminar_busqueda_tiposangre'])) {
			# code...
			unset($_SESSION['busqueda_tiposangre']);
		}
		//para evaluar si esta lleno el campo a buscar del formulario
		if(!isset($_SESSION['busqueda_tiposangre']) && empty($_SESSION['busqueda_tiposangre'])):
 ?>
<div class="container-fluid">
	<form class="well" method="POST" action="" autocomplete="off">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2">
				<div class="form-group label-floating">
					<span class="control-label">
						A quien estas Buscando?
					</span>
					<input class="form-control" type="text" name="busqueda_inicial_tiposangre" required="" >				
				</div>				
			</div>
			<div class="col-xs-12">
				<p class="text-center">
					<button type="submit" class="btn btn-primary btn-raised btn-sm"> <i class="zmdi zmdi-search">&nbsp; Buscar</i> </button>
				</p>
				
			</div>
			
		</div>
	</form>
</div>

<?php 
	else:
 ?>


 <div class="container-fluid">
 	<form class="well" method="POST" action="">
 		<p class="lead text-center">Su ultima busqueda fue <strong>"<?php echo $_SESSION['busqueda_tiposangre'] ?>"</strong> </p>
 		<div class="row">
 			<input class="form-control" type="hidden" name="eliminar_busqueda_tiposangre" value="1">
 			<div class="col-xs-12">
 				<p class="text-center">
 					<button type="submit" class="btn btn-danger btn-raised btn-sm">
 						<i class="zmdi zmdi-delete"></i>&nbsp; Eliminar Busqueda
 					</button>
 				</p>
 			</div>
 		</div>
 	</form>
 	
 </div>

 <div class="container-fluid">
 	<div class="panel panel-primary">
 		<div class="panel-heading">
 			<h3 class="panel-title"><i class="zmdi zmdi-search"></i>&nbsp; BUSCAR IDIOMA</h3>
 		</div>
 		<div class="panel-body">
 			<?php 
			//cortar el string views viene de htaccess
				$pagina = explode("/",$_GET['views']);
				echo $insAdmin->paginador_tiposangre_controlador($pagina[1],3,$_SESSION['privilegio_se'],$_SESSION['codigo_cuenta_se'],$_SESSION['busqueda_tiposangre']);

			 ?>
 		</div>
 	</div>
 </div>

 <?php 
endif;
  ?>
