<div class="container-fluid">
			<div class="page-header">
			  <h1 class="text-titles"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Users <small>Cuenta</small></h1>
			</div>
			<p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse voluptas reiciendis tempora voluptatum eius porro ipsa quae voluptates officiis sapiente sunt dolorem, velit quos a qui nobis sed, dignissimos possimus!</p>
</div>
	<div class="container-fluid">
		<ul class="breadcrumb breadcrumb-tabs">
			<!-- BOTON NUEVO -->
			<li>
				<a href="<?php echo SERVERURL;?>cuenta/" class="btn btn-info">
					<i class="zmdi zmdi-plus"></i> &nbsp; NUEVO Cuenta
				</a>
			</li>
			<!-- BOTON LISTAR -->
			<li>
				<a href="<?php echo SERVERURL;?>cuentalist/" class="btn btn-success">
					<i class="zmdi zmdi-format-list-bulleted"></i> &nbsp; LISTA Cuenta
				</a>
			</li>
			<!-- BOTON BUSCAR -->
			<li>
				<a href="<?php echo SERVERURL;?>cuentasearch/" class="btn btn-primary">
					<i class="zmdi zmdi-search"></i> &nbsp; BUSCAR Cuenta
				</a>
			</li>
		</ul>
	</div>

		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">
					
					<div id="myTabContent" class="tab-content">
						<div class="tab-pane fade active in" id="new">
							<div class="container-fluid">
								<div class="row">
									<div class="col-xs-12 col-md-10 col-md-offset-1">
										<!-- aqui se pego ///////////////////////////////////////////-->
									    <form action="<?php echo SERVERURL ; ?>ajax/administradorAjax.php" method="POST" data-form="save" class="FormularioAjax" autocomplete="off" enctype="multipart/form-data"> 	
									    	<div class="form-group label-floating">
											  <label class="control-label">Priviledio</label>
											  <input class="form-control" type="text">
											</div>
											<div class="form-group label-floating">
											  <label class="control-label">Usuario</label>
											  <input class="form-control" type="text">
											</div>
											<div class="form-group label-floating">
											  <label class="control-label">Clave</label>
											  <textarea class="form-control"></textarea>
											</div>
											<div class="form-group label-floating">
											  <label class="control-label">Email</label>
											  <input class="form-control" type="text">
											</div>
											<div class="form-group">
										      <label class="control-label">Estado</label>
										        <select class="form-control">
										          <option>Active</option>
										          <option>Disable</option>
										        </select>
										    </div>
										    <div class="form-group label-floating">
											  <label class="control-label">Tipo</label>
											  <input class="form-control" type="text">
											</div>

											<div class="form-group label-floating">
											  <label class="control-label">Genero</label>
											  <input class="form-control" type="text">
											</div>

											<div class="form-group">
										      <label class="control-label">Photo</label>
										      <div>
										        <input type="text" readonly="" class="form-control" placeholder="Browse...">
										        <input type="file" >
										      </div>
										    </div>
										    <p class="text-center">
										    	<button href="#!" class="btn btn-info btn-raised btn-sm"><i class="zmdi zmdi-floppy"></i> Save
										    	</button>
										    </p>
										   
									    </form>
									    <!-- aqui se pego ///////////////////////////////////////////-->
									</div>
								</div>
							</div>
						</div>

						<!-- ////////////////TABLA////////////////////////////////////////////////////// -->
					  
					</div>
				</div>
			</div>
		</div>