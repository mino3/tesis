<div class="container-fluid">
	<?php 
		$datos=explode("/", $_GET['views']);
		if ($datos[1]=="canton") {
			# code...
		}elseif ($datos[1]=="canton") {
			# code...
		}

	 ?>
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="zmdi zmdi-plus"></i> &nbsp; NUEVA CANTON</h3>
		</div>
		<div class="panel-body">
			<form action="<?php echo SERVERURL;?>/ajax/cantonAjax.php" method="POST" data-form="save" class="FormularioAjax" autocomplete="off" enctype="multipart/form-data"> 	
				<fieldset>
					<legend><i class="zmdi zmdi-account-box"></i>&nbsp;INFORMACION CANTON </legend>
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-12 col-sm-6">
									<div class="form-group label-floating">
										<label class="control-label">CIUDAD</label>
										<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;" name="nombre-reg" required="" maxlength="30">
									</div>
								</div>

								<div class="col-xs-12 col-sm-6">
									<div class="form-group label-floating">
										<label class="control-label">DESCRIPCION</label>
										<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;"name="desc-reg" required="" maxlength="30">
									</div>
								</div>

								<div class="col-xs-12 col-sm-6">
									<label class="control-label">
										Estado
									</label>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsEstado" id="optionsRadios1" value="1" checked><i class="zmdi zmdi-start" ></i> &nbsp; Activo
									</label>
								</div>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsEstado" id="optionsRadios2" value="2"><i class="zmdi zmdi-start"></i> &nbsp; Inactivo
									</label>
								</div>
							</div>
								
									<div class="col-xs-12">
									<div class="form-group label-floating">
										<label class="control-label">FECHA </label>
										<input pattern="[0-9-]{1,30}" class="form-control" type="date" name="fecha-reg" required="" maxlength="30">
									</div>
								</div>


																
							</div>
						</div>
				</fieldset>
			
				<p class="text-center" style="margin-top: 20px ">
					<button type="submit" class="btn btn-info btn-raised btn-sm"><i class="zmdi zmdi-floppy"></i> Save
					</button>
				</p>
				<div class="RespuestaAjax"></div>
			</form>									
		</div>
	</div>
</div>