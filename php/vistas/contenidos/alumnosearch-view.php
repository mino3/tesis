<div class="container-fluid">
	<div class="page-header">
		<h1 class="text-titles"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Users <small>Alumno</small></h1>
	</div>
	<p class="lead">INGRESO ALUMNO  </p>
</div>
<div class="container-fluid">
	<ul class="breadcrumb breadcrumb-tabs">
		<!-- BOTON NUEVO -->
		<li>
			<a href="<?php echo SERVERURL;?>alumno/" class="btn btn-info">
				<i class="zmdi zmdi-plus"></i> &nbsp; NUEVO ALUMNO
			</a>
		</li>
		<!-- BOTON LISTAR -->
		<li>
			<a href="<?php echo SERVERURL;?>alumnolist/" class="btn btn-success">
				<i class="zmdi zmdi-format-list-bulleted"></i> &nbsp; LISTA ALUMNO
			</a>
		</li>
		<!-- BOTON BUSCAR -->
		<li>
			<a href="<?php echo SERVERURL;?>alumnosearch/" class="btn btn-primary">
				<i class="zmdi zmdi-search"></i> &nbsp; BUSCAR ALUMNO
			</a>
		</li>
	</ul>
</div>