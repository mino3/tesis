<div class="container-fluid">
	<div class="page-header">
		<h1 class="text-titles"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Users <small>Discapacidad</small></h1>
	</div>
	<p class="lead">LISTA DISCAPACIDAD</p>
</div>
<div class="container-fluid">
	<ul class="breadcrumb breadcrumb-tabs">
		<!-- BOTON NUEVO -->
		<li>
			<a href="<?php echo SERVERURL;?>discapacidad/" class="btn btn-info">
				<i class="zmdi zmdi-plus"></i> &nbsp; NUEVO DISCAPACIDAD
			</a>
		</li>
		<!-- BOTON LISTAR -->
		<li>
			<a href="<?php echo SERVERURL;?>discapacidadlist/" class="btn btn-success">
				<i class="zmdi zmdi-format-list-bulleted"></i> &nbsp; LISTA DISCAPACIDAD
			</a>
		</li>
		<!-- BOTON BUSCAR -->
		<li>
			<a href="<?php echo SERVERURL;?>discapacidadsearch/" class="btn btn-primary">
				<i class="zmdi zmdi-search"></i> &nbsp; BUSCAR DISCAPACIDAD
			</a>
		</li>
	</ul>
</div>

<?php 
		require_once "./controladores/discapacidadControlador.php";
		$insDis= new discapacidadControlador();
 ?>
<div class="container-fluid">
	<div class="panel panel-success">
		<div class="panel-heading">
			<h3 class="panel-title">
				<i class="zmdi zmdi-format-list-bulleted"></i> &nbsp; LISTA DISCAPACIDADES
			</h3>
		</div>
		<div class="panel-body">
			<?php 
			//cortar el string views viene de htaccess
				$pagina = explode("/",$_GET['views']);
				echo $insDis->paginador_discapacidad_controlador($pagina[1],3,$_SESSION['privilegio_se'],$_SESSION['codigo_cuenta_se'],"");
			 ?>							
		</div>			
	</div>		
</div>
