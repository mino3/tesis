<div class="container-fluid">
	<div class="page-header">
		<h1 class="text-titles"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Users <small>Idioma</small></h1>
	</div>
	<p class="lead">REGISTRAR NUEVO IDIOMA</p>
</div>
<div class="container-fluid">
	<ul class="breadcrumb breadcrumb-tabs">
		<!-- BOTON NUEVO -->
		<li>
			<a href="<?php echo SERVERURL;?>idioma/" class="btn btn-info">
				<i class="zmdi zmdi-plus"></i> &nbsp; NUEVO IDIOMA
			</a>
		</li>
		<!-- BOTON LISTAR -->
		<li>
			<a href="<?php echo SERVERURL;?>idiomalist/" class="btn btn-success">
				<i class="zmdi zmdi-format-list-bulleted"></i> &nbsp; LISTA IDIOMA
			</a>
		</li>
		<!-- BOTON BUSCAR -->
		<li>
			<a href="<?php echo SERVERURL;?>idiomasearch/" class="btn btn-primary">
				<i class="zmdi zmdi-search"></i> &nbsp; BUSCAR IDIOMA
			</a>
		</li>
	</ul>
</div>

<!-- panel nuevo administrador
 -->
 <div class="container-fluid">
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="zmdi zmdi-plus"></i> &nbsp; NUEVO IDIOMA</h3>
		</div>
		<div class="panel-body">
			<form action="<?php echo SERVERURL;?>ajax/idiomaAjax.php" method="POST" data-form="save" class="FormularioAjax" autocomplete="off" enctype="multipart/form-data"> 	
				<fieldset>
					<legend><i class="zmdi zmdi-account-box"></i>&nbsp;INFORMACION DEL IDIOMA</legend>
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group label-floating">
										<label class="control-label">ID </label>
										<input pattern="[0-9-]{1,30}" class="form-control" type="text" name="id-reg" required="" maxlength="30">
									</div>
								</div>
									<div class="col-xs-12 col-sm-6">
									<div class="form-group label-floating">
										<label class="control-label">NOMBRE</label>
										<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" name="nombre-reg" required="" maxlength="30">
									</div>
								</div>

								<div class="col-xs-12 col-sm-6">
									<div class="form-group label-floating">
										<label class="control-label">DESCRIPCION</label>
										<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" name="desc-reg" required="" maxlength="30">
									</div>
								</div>

								<div class="col-xs-12 col-sm-6">
									<label class="control-label">
										Estado
									</label>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsEstado" id="optionsRadios1" value="1"><i class="zmdi zmdi-start"></i> &nbsp; Activo
									</label>
								</div>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsEstado" id="optionsRadios2" value="0"><i class="zmdi zmdi-start"></i> &nbsp; Inactivo
									</label>
								</div>
							</div>
								

									<div class="col-xs-12">
									<div class="form-group label-floating">
										<label class="control-label">FECHA </label>
										<input pattern="[0-9-]{1,30}" class="form-control" type="date" name="fecha-reg" required="" maxlength="30">
									</div>
								</div>


																
							</div>
						</div>
				</fieldset>
			
				<p class="text-center" style="margin-top: 20px ">
					<button type="submit" class="btn btn-info btn-raised btn-sm"><i class="zmdi zmdi-floppy"></i> Save
					</button>
				</p>
				<div class="RespuestaAjax"></div>
			</form>									
		</div>
	</div>
</div>