<?php 
	if ($_SESSION['tipo_se']!="Administrador") {
		echo $lc->forzar_cierre_session_controlador();
	}
 ?>

<div class="container-fluid">
	<div class="page-header">
		<h1 class="text-titles"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Users <small>Admin</small></h1>
	</div>
	<p class="lead">LISTA DE ADMINISTRADORES</p>
</div>
<div class="container-fluid">
	<ul class="breadcrumb breadcrumb-tabs">
		<!-- BOTON NUEVO -->
		<li>
			<a href="<?php echo SERVERURL;?>admin/" class="btn btn-info">
				<i class="zmdi zmdi-plus"></i> &nbsp; NUEVO ADMINISTRADOR
			</a>
		</li>
		<!-- BOTON LISTAR -->
		<li>
			<a href="<?php echo SERVERURL;?>adminlist/" class="btn btn-success">
				<i class="zmdi zmdi-format-list-bulleted"></i> &nbsp; LISTA ADMINISTRADOR
			</a>
		</li>
		<!-- BOTON BUSCAR -->
		<li>
			<a href="<?php echo SERVERURL;?>adminserach/" class="btn btn-primary">
				<i class="zmdi zmdi-search"></i> &nbsp; BUSCAR ADMINISTRADOR
			</a>
		</li>
	</ul>
</div>


<!-- aqui esta la tabla -->
<?php 
		require_once "./controladores/administradorControlador.php";
		$insAdmin= new administradorControlador();
 ?>
<div class="container-fluid">
	<div class="panel panel-success">
		<div class="panel-heading">
			<h3 class="panel-title">
				<i class="zmdi zmdi-format-list-bulleted"></i> &nbsp; LISTA ADMINISTRADOR
			</h3>
		</div>
		<div class="panel-body">
			<?php 
			//cortar el string views viene de htaccess
				$pagina = explode("/",$_GET['views']);
				echo $insAdmin->paginador_administrador_controlador($pagina[1],3,$_SESSION['privilegio_se'],$_SESSION['codigo_cuenta_se'],"")

			 ?>

			
				
		</div>			
	</div>		
</div>


<!-- <div class="tab-pane fade" id="list">
						<div class="table-responsive">
							

						</div>
				  	</div> -->