<div class="container-fluid">
	<div class="page-header">
		<h1 class="text-titles"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Users <small>Familia</small></h1>
	</div>
	<p class="lead">INGRESO FAMILIA</p>
</div>
<div class="container-fluid">
	<ul class="breadcrumb breadcrumb-tabs">
		<!-- BOTON NUEVO -->
		<li>
			<a href="<?php echo SERVERURL;?>familia/" class="btn btn-info">
				<i class="zmdi zmdi-plus"></i> &nbsp; NUEVA FAMILIA
			</a>
		</li>
		<!-- BOTON LISTAR -->
		<li>
			<a href="<?php echo SERVERURL;?>familialist/" class="btn btn-success">
				<i class="zmdi zmdi-format-list-bulleted"></i> &nbsp; LISTA FAMILIA
			</a>
		</li>
		<!-- BOTON BUSCAR -->
		<li>
			<a href="<?php echo SERVERURL;?>familiasearch/" class="btn btn-primary">
				<i class="zmdi zmdi-search"></i> &nbsp; BUSCAR FAMILIA
			</a>
		</li>
	</ul>
</div>
<?php 
		require_once "./controladores/familiaControlador.php";
		$insPerso= new familiaControlador();
 ?>
<div class="container-fluid">
	<div class="panel panel-success">
		<div class="panel-heading">
			<h3 class="panel-title">
				<i class="zmdi zmdi-format-list-bulleted"></i> &nbsp; LISTA REPRESENTANTES
			</h3>
		</div>
		<div class="panel-body">
			<?php 
			//cortar el string views viene de htaccess
				$pagina = explode("/",$_GET['views']);
				echo $insPerso->paginador_persona_controlador($pagina[1],5,$_SESSION['privilegio_se'],$_SESSION['codigo_cuenta_se'],"");
			 ?>							
		</div>			
	</div>		
</div>
