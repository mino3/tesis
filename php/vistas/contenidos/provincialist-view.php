<div class="container-fluid">
	<div class="page-header">
		<h1 class="text-titles"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Users <small>Provincia</small></h1>
	</div>
	<p class="lead">LISTAS PROVINCIA </p>
</div>
<div class="container-fluid">
	<ul class="breadcrumb breadcrumb-tabs">
		<!-- BOTON NUEVO -->
		<li>
			<a href="<?php echo SERVERURL;?>provincia/" class="btn btn-info">
				<i class="zmdi zmdi-plus"></i> &nbsp; NUEVO PROVINCIA
			</a>
		</li>
		<!-- BOTON LISTAR -->
		<li>
			<a href="<?php echo SERVERURL;?>provincialist/" class="btn btn-success">
				<i class="zmdi zmdi-format-list-bulleted"></i> &nbsp; LISTA PROVINCIA
			</a>
		</li>
		<!-- BOTON BUSCAR -->
		<li>
			<a href="<?php echo SERVERURL;?>provinciasearch/" class="btn btn-primary">
				<i class="zmdi zmdi-search"></i> &nbsp; BUSCAR PROVINCIA
			</a>
		</li>
	</ul>
</div>

<?php 
		require_once "./controladores/provinciaControlador.php";
		$insProv= new provinciaControlador();
 ?>
<div class="container-fluid">
	<div class="panel panel-success">
		<div class="panel-heading">
			<h3 class="panel-title">
				<i class="zmdi zmdi-format-list-bulleted"></i> &nbsp; LISTA PROVINCIA
			</h3>
		</div>
		<div class="panel-body">
			<?php 
			//cortar el string views viene de htaccess
				$pagina = explode("/",$_GET['views']);
				echo $insProv->paginador_provincia_controlador($pagina[1],2,$_SESSION['privilegio_se'],$_SESSION['codigo_cuenta_se'],"");
			 ?>							
		</div>			
	</div>		
</div>
