<div class="container-fluid">
	<div class="page-header">
		<h1 class="text-titles"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Users <small>ESTADO CIVIL</small></h1>
	</div>
	<p class="lead">ESTADO CIVIL DE PERSONAS</p>
</div>
<div class="container-fluid">
	<ul class="breadcrumb breadcrumb-tabs">
		<!-- BOTON NUEVO -->
		<li>
			<a href="<?php echo SERVERURL;?>estadocivil/" class="btn btn-info">
				<i class="zmdi zmdi-plus"></i> &nbsp; NUEVO ESTADO CIVIL
			</a>
		</li>
		<!-- BOTON LISTAR -->
		<li>
			<a href="<?php echo SERVERURL;?>estadocivillist/" class="btn btn-success">
				<i class="zmdi zmdi-format-list-bulleted"></i> &nbsp; LISTA ESTADO CIVIL
			</a>
		</li>
		<!-- BOTON BUSCAR -->
		<li>
			<a href="<?php echo SERVERURL;?>estadocivilsearch/" class="btn btn-primary">
				<i class="zmdi zmdi-search"></i> &nbsp; BUSCAR ESTADO CIVIL
			</a>
		</li>
	</ul>
</div>


<div class="container-fluid">
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="zmdi zmdi-plus"></i> &nbsp; NUEVA ESTADO CIVIL</h3>
		</div>
		<div class="panel-body">
			<form action="<?php echo SERVERURL;?>/ajax/estadocivilAjax.php" method="POST" data-form="save" class="FormularioAjax" autocomplete="off" enctype="multipart/form-data"> 	
				<fieldset>
					<legend><i class="zmdi zmdi-account-box"></i>&nbsp;INFORMACION ESTADO CIVIL</legend>
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-12 col-sm-6">
								<div class="form-group label-floating">
									<label class="control-label">NOMBRE</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" name="nombre-reg" required="" maxlength="30">
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<label class="control-label">
									Estado
								</label>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsEstado" id="optionsRadios1" value="1"><i class="zmdi zmdi-start"></i> &nbsp; Activo
									</label>
								</div>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsEstado" id="optionsRadios2" value="0"><i class="zmdi zmdi-start"></i> &nbsp; Inactivo
									</label>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="form-group label-floating">
									<label class="control-label">FECHA </label>
									<input pattern="[0-9-]{1,30}" class="form-control" type="date" name="fecha-reg" required="" maxlength="30">
								</div>
							</div>


							
						</div>
					</div>
				</fieldset>
				
				<p class="text-center" style="margin-top: 20px ">
					<button type="submit" class="btn btn-info btn-raised btn-sm"><i class="zmdi zmdi-floppy"></i> Save
					</button>
				</p>
				<div class="RespuestaAjax"></div>
			</form>									
		</div>
	</div>
</div>