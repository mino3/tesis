 <div class="container-fluid">
	<div class="page-header">
		<h1 class="text-titles"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Users <small>REPRESENTANTE</small></h1>
	</div>
	<p class="lead">REPRESENTANTE</p>
</div>

 <div class="container-fluid">
 	<?php 
 	// viene del htacces
 		$datos = explode("/",$_GET['views']);

 		//mydata/admin/codcuenta
 		//administrador
 		if ($datos[1]=="familia") {			
	if ($_SESSION['tipo_se']!="Administrador") {
		echo $lc->forzar_cierre_session_controlador();
	}
	require_once "./controladores/familiaControlador.php";
	$classAdmin= new familiaControlador();
	//hace el select para seleccionar los del familia
 		$filesA=$classAdmin->datos_familia_controlador("Unico",$datos[2]);
 		if ($filesA->rowCount()==1) {
 			//campos tine todos los datos del administrador
 			$campos=$filesA->fetch();
 			
 				if ($_SESSION['privilegio_se']<1 || $_SESSION['privilegio_se']>2) {
 					echo $lc->forzar_cierre_session_controlador();
 				} 			
?>
   <div class="panel panel-success">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="zmdi zmdi-plus"></i> &nbsp; MIS DATOS</h3>
		</div>
		<div class="panel-body">
			<form action="<?php echo SERVERURL;?>ajax/familiaAjax.php" method="POST" data-form="update" class="FormularioAjax" autocomplete="off" enctype="multipart/form-data"> 	
				<input type="hidden" name="cuenta-up" value="<?php echo $datos[2]; ?>">
				<fieldset>
					<legend><i class="zmdi zmdi-account-box"></i>&nbsp;INFORMACION PERSONA </legend>
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group label-floating">
									<label class="control-label">CEDULA </label>
									<input pattern="[0-9-]{1,30}" class="form-control" type="text" name="dni-up" value="<?php echo $campos['per_codigo'] ?>" required="" maxlength="10">
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="form-group label-floating">
									<label class="control-label">PRIMER NOMBRE</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;" name="1nombre-up" value="<?php echo $campos['per_primernombre']?>"  required="" maxlength="30">
								</div>
							</div>

							<div class="col-xs-12 col-sm-6">
								<div class="form-group label-floating">
									<label class="control-label">SEGUNDO NOMBRE</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;" name="2nombre-up" value="<?php echo $campos['per_segundonombre']?>" required="" maxlength="30">
								</div>
							</div>

							<div class="col-xs-12 col-sm-6">
								<div class="form-group label-floating">
									<label class="control-label">APELLIDO PATERNO</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;" name="1ape-up" value="<?php echo $campos['per_primerapellido']?>" required="" maxlength="30">
								</div>
							</div>

							<div class="col-xs-12 col-sm-6">
								<div class="form-group label-floating">
									<label class="control-label">APELLIDO MATERNO</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;" name="2ape-up" value="<?php echo $campos['per_segundoapellido']?>" required="" maxlength="30">
								</div>
							</div>

							<div class="col-xs-12">
								<div class="form-group label-floating">
									<label class="control-label">FECHA NACIMIENTO </label>
									<input pattern="[0-9-]{1,30}" class="form-control" type="date" name="fechaN-up" value="<?php echo $campos['per_fechanacimiento']?>" required="" maxlength="30">
								</div>
							</div>

							<div class="col-xs-12">
								<div class="form-group label-floating">
									<label class="control-label">TELEFONO </label>
									<input pattern="[0-9-]{1,30}" class="form-control" type="text" name="telf-up" value="<?php echo $campos['per_telefono']?>" required="" maxlength="30">
								</div>
							</div>

							<div class="col-xs-12">
								<div class="form-group label-floating">
									<label class="control-label">CELULAR </label>
									<input pattern="[0-9-]{1,30}" class="form-control" type="text" name="celu-up" value="<?php echo $campos['per_celular']?>" required="" maxlength="30">
								</div>
							</div>

							<div class="col-xs-6"> 
								<div class="form-group label-floating">
									<label class="control-label">
										CORREO
									</label>
									<input  class="form-control" type="email" name="email-up" value="<?php echo $campos['per_correo']?>" required="" maxlength="50">
								</div>
							</div>
							<br>

					<?php if ($_SESSION['tipo_se']=="Administrador" && $_SESSION['privilegio_se']==1):?>
								<div class="col-xs-12 col-sm-6">
									<label class="control-label">
										Estado
									</label>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsEstado-up" id="optionsRadios1" value="1" <?php if ($campos['per_estado']=="1") {echo'checked=""';} ?>><i class="zmdi zmdi-start"></i> &nbsp; Activo
									</label>
								</div>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsEstado-up" id="optionsRadios2" value="0" <?php if ($campos['per_estado']=="0") {echo'checked=""';} ?>><i class="zmdi zmdi-start"></i> &nbsp; Inactivo
									</label>
								</div>
							</div>
						<?php endif; ?>

							<div class="col-xs-12 col-sm-6">
								<label class="control-label">
									TIENE DISCAPACIDAD
								</label>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsDiscapacidad-up" id="optionsRadios1" value="SI" <?php if ($campos['per_discapacidad']=="SI") {echo'checked=""';} ?>><i class = "zmdi zmdi-flower-alt mdc-text-red" > </i> &nbsp; SI
									</label>
								</div>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsDiscapacidad-up" id="optionsRadios2" value="NO" <?php if ($campos['per_discapacidad']=="NO") {echo'checked=""';} ?>><i class = "zmdi zmdi-flower-alt mdc-text-green"> </i> &nbsp; NO
									</label>
								</div>
							</div>

							<!-- <div class="col-xs-12">
								<div class="form-group label-floating">
									<label class="control-label">FECHA </label>
									<input pattern="[0-9-]{1,30}" class="form-control" type="date" name="fecha-up" required="" maxlength="30">
								</div>
							</div> -->
						</div>
					</div>
				</fieldset>	

				<!-- <fieldset>
					<legend>
						<i class="zmdi zmdi-account-box"></i> &nbsp; QUE TIPO DE FAMILIAR ES PARA EL ALUMNO
					</legend>
					<p class="lead">Seleccionar</p>
					<div class="panel-body">
						<div class="row">
							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT * from tipofamilia");

								?>
								<select name="asTipofamilia" class="form-control" >
									<option>Seleccionar Tipo Familia</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[2].'</option>';
									}?>
								</select>
							</div>

						</div>
					</div>		
				</fieldset>	 -->	
				<!-- discapacidad -->	
				<fieldset>
					<legend>
						<i class="zmdi zmdi-account-box"></i> &nbsp; INFORMACION SI TIENE ALGUNA DISCAPACIDAD
					</legend>
					<p class="lead"> En caso de tener discapacidad escoger</p>
					<div class="panel-body">
						<div class="row">
							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT * from discapacidad");
								?>
								<select name="asDiscapa-up" class="form-control" >
									<option>Seleccionar Discapacidad</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[2].'</option>';
									}?>
								</select>
							</div>

						</div>
					</div>
				</fieldset>	

				<br>
				<!-- <fieldset>
					<legend>
						<i class="zmdi zmdi-account-box"></i> &nbsp; INFORMACION DOMICILIARIA
					</legend>
					<div class="panel-body">
						<div class="row">
							
							<div class="col-xs-12 col-sm-6">
								<div class="form-group label-floating">
									<label class="control-label">CALLE PRINCIPAL</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;" name="CPrinci-up" required="" maxlength="30">
								</div>
							</div>

							<div class="col-xs-12 col-sm-6">
								<div class="form-group label-floating">
									<label class="control-label">CALLE SECUNDARIA</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;" name="CSecu-up" required="" maxlength="30">
								</div>
							</div>
							<div class="col-xs-12">
								<div class="form-group label-floating">
									<label class="control-label">NUMERO DE CASA </label>
									<input pattern="[0-9-]{1,30}" class="form-control" type="text" name="numcasa-up" required="" maxlength="30">
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="form-group label-floating">
									<label class="control-label">SECTOR</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;" name="sector-up" required="" maxlength="30">
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="form-group label-floating">
									<label class="control-label">REFERENCIA</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;" name="refer-up" required="" maxlength="30">
								</div>
							</div>


							
						</div>
					</div>						
				</fieldset> -->
				
				<fieldset>
					<legend>
						<i class="zmdi zmdi-account-box"></i> &nbsp; INFORMACION

					</legend>
					<div class="container-fluid">
						<div class="row">					
							<div class="form-group label-floating">
								<?php												
								$con=mysqli_connect(SERVER,USER,PASS,DB);
							$res=$con->query("SELECT * from etnia WHERE et_estado!='0'");							
									//$idetnia
								?>
								<select name="asetnia-up" class="form-control" >
								<option>Seleccione su Etnia</option>		
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[2].'</option>';
									}?>
								</select>								
							</div>
							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT *from estadocivil");

								?>
								<select name="ascivil-up" class="form-control" >
									<option>Seleccione Estado Civil</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[0]." ".$r[2].'</option>';

									}?>
								</select>
							</div>

							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT *from tiposangre");

								?>
								<select name="asSangre-up" class="form-control" >
									<option>Seleccione su Tipo Sangre</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[0]."".$r[2].'</option>';

									}?>
								</select>
							</div>

							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT *from nacionalidad");

								?>
								<select name="asNacion-up" class="form-control" >
									<option>Seleccione su Nacionalidad</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[0]."".$r[2].'</option>';

									}?>
								</select>
							</div>

							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT *from provincia");

								?>
								<select name="asProvinc-up" class="form-control" >
									<option>Seleccione Provincia</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[0]."".$r[2].'</option>';

									}?>
								</select>
							</div>

							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT *from canton");

								?>
								<select name="asCanton-up" class="form-control" >
									<option>Seleccionar Ciudad</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[0]."".$r[2].'</option>';

									}?>
								</select>
							</div>



						</div>
					</div>
				</fieldset>
			
			<p class="text-center" style="margin-top: 20px; ">
					<button type="submit" class="btn btn-info btn-raised btn-sm"><i class="zmdi zmdi-refresh"></i> UPDATE
					</button>
				</p>
				<div class="RespuestaAjax"></div>
				</form>
				
	   </div>
   </div>
	

<?php }else{ ?>
		<h4>Lo sentimos</h4>
		<p>No podemos mostrar la informacion solicitada 1</p>
<?php }
 		//usuario normal	
 		}elseif ($datos[1]=="user") {
 			echo "usuario";
 		
 		//error	
 		}else{
?>
<h4>Lo sentimos</h4>
<p>No podemos mostrar la informacion solicitada 2</p>
<?php
        //termina 			
 		}
?>
	 	
</div>			



