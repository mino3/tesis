<div class="container-fluid">
	<div class="page-header">
		<h1 class="text-titles"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Users <small>Modalidad</small></h1>
	</div>
	<p class="lead">MODALIDAD DE ESTUDIO</p>
</div>
<div class="container-fluid">
	<ul class="breadcrumb breadcrumb-tabs">
		<!-- BOTON NUEVO -->
		<li>
			<a href="<?php echo SERVERURL;?>modalidad/" class="btn btn-info">
				<i class="zmdi zmdi-plus"></i> &nbsp; NUEVO MODALIDAD
			</a>
		</li>
		<!-- BOTON LISTAR -->
		<li>
			<a href="<?php echo SERVERURL;?>modalidadlist/" class="btn btn-success">
				<i class="zmdi zmdi-format-list-bulleted"></i> &nbsp; LISTA MODALIDAD
			</a>
		</li>
		<!-- BOTON BUSCAR -->
		<li>
			<a href="<?php echo SERVERURL;?>modalidadsearch/" class="btn btn-primary">
				<i class="zmdi zmdi-search"></i> &nbsp; BUSCAR MODALIDAD
			</a>
		</li>
	</ul>
</div>

<?php 
		require_once "./controladores/modalidadControlador.php";
		$insAdmin= new modalidadControlador();
		if (isset($_POST['busqueda_inicial_modalidad'])) {
			//almacenar el valor que enviar el formulario en una variable de session
			$_SESSION['busqueda_modalidad']=$_POST['busqueda_inicial_modalidad'];
		}
		//elimina la variable de session 
		if (isset($_POST['eliminar_busqueda_modalidad'])) {
			# code...
			unset($_SESSION['busqueda_modalidad']);
		}
		//para evaluar si esta lleno el campo a buscar del formulario
		if(!isset($_SESSION['busqueda_modalidad']) && empty($_SESSION['busqueda_modalidad'])):
 ?>
<div class="container-fluid">
	<form class="well" method="POST" action="" autocomplete="off">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2">
				<div class="form-group label-floating">
					<span class="control-label">
						A quien estas Buscando?
					</span>
					<input class="form-control" type="text" name="busqueda_inicial_modalidad" required="" >				
				</div>				
			</div>
			<div class="col-xs-12">
				<p class="text-center">
					<button type="submit" class="btn btn-primary btn-raised btn-sm"> <i class="zmdi zmdi-search">&nbsp; Buscar</i> </button>
				</p>
				
			</div>
			
		</div>
	</form>
</div>

<?php 
	else:
 ?>


 <div class="container-fluid">
 	<form class="well" method="POST" action="">
 		<p class="lead text-center">Su ultima busqueda fue <strong>"<?php echo $_SESSION['busqueda_modalidad'] ?>"</strong> </p>
 		<div class="row">
 			<input class="form-control" type="hidden" name="eliminar_busqueda_modalidad" value="1">
 			<div class="col-xs-12">
 				<p class="text-center">
 					<button type="submit" class="btn btn-danger btn-raised btn-sm">
 						<i class="zmdi zmdi-delete"></i>&nbsp; Eliminar Busqueda
 					</button>
 				</p>
 			</div>
 		</div>
 	</form>
 	
 </div>

 <div class="container-fluid">
 	<div class="panel panel-primary">
 		<div class="panel-heading">
 			<h3 class="panel-title"><i class="zmdi zmdi-search"></i>&nbsp; BUSCAR IDIOMA</h3>
 		</div>
 		<div class="panel-body">
 			<?php 
			//cortar el string views viene de htaccess
				$pagina = explode("/",$_GET['views']);
				echo $insAdmin->paginador_modalidad_controlador($pagina[1],3,$_SESSION['privilegio_se'],$_SESSION['codigo_cuenta_se'],$_SESSION['busqueda_modalidad']);

			 ?>
 		</div>
 	</div>
 </div>

 <?php 
endif;
  ?>

