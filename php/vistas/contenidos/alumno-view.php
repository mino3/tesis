<div class="container-fluid">
	<div class="page-header">
		<h1 class="text-titles"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Users <small>Alumno</small></h1>
	</div>
	<p class="lead">INGRESO ALUMNO  </p>
</div>
<div class="container-fluid">
	<ul class="breadcrumb breadcrumb-tabs">
		<!-- BOTON NUEVO -->
		<li>
			<a href="<?php echo SERVERURL;?>alumno/" class="btn btn-info">
				<i class="zmdi zmdi-plus"></i> &nbsp; NUEVO ALUMNO
			</a>
		</li>
		<!-- BOTON LISTAR -->
		<li>
			<a href="<?php echo SERVERURL;?>alumnolist/" class="btn btn-success">
				<i class="zmdi zmdi-format-list-bulleted"></i> &nbsp; LISTA ALUMNO
			</a>
		</li>
		<!-- BOTON BUSCAR -->
		<li>
			<a href="<?php echo SERVERURL;?>alumnosearch/" class="btn btn-primary">
				<i class="zmdi zmdi-search"></i> &nbsp; BUSCAR ALUMNO
			</a>
		</li>
	</ul>
</div>
<div class="container-fluid">
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="zmdi zmdi-plus"></i> &nbsp; NUEVO ALUMNO</h3>
		</div>
		<div class="panel-body">
			<form data-form="save" method="POST" action="<?php echo SERVERURL;?>ajax/alumnoAjax.php"   class="FormularioAjax" autocomplete="off" enctype="multipart/form-data"> 	
				<fieldset>
					<legend><i class="zmdi zmdi-account-box"></i>&nbsp;INFORMACION PERSONAL </legend>
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group label-floating">
									<label class="control-label">CEDULA </label>
									<input pattern="[0-9-]{1,30}" class="form-control" type="text" name="dni-reg" required="" maxlength="10">
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="form-group label-floating">
									<label class="control-label">PRIMER NOMBRE</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;" name="1nombre-reg" required="" maxlength="30">
								</div>
							</div>

							<div class="col-xs-12 col-sm-6">
								<div class="form-group label-floating">
									<label class="control-label">SEGUNDO NOMBRE</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;" name="2nombre-reg" required="" maxlength="30">
								</div>
							</div>

							<div class="col-xs-12 col-sm-6">
								<div class="form-group label-floating">
									<label class="control-label">APELLIDO PATERNO</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;" name="1ape-reg" required="" maxlength="30">
								</div>
							</div>

							<div class="col-xs-12 col-sm-6">
								<div class="form-group label-floating">
									<label class="control-label">APELLIDO MATERNO</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;" name="2ape-reg" required="" maxlength="30">
								</div>
							</div>

							<div class="col-xs-12">
								<div class="form-group label-floating">
									<label class="control-label">FECHA NACIMIENTO </label>
									<input pattern="[0-9-]{1,30}" class="form-control" type="date" name="fechaN-reg" required="" maxlength="30">
								</div>
							</div>

							<div class="col-xs-12">
								<div class="form-group label-floating">
									<label class="control-label">TELEFONO </label>
									<input pattern="[0-9-]{1,30}" class="form-control" type="text" name="telf-reg" required="" maxlength="30">
								</div>
							</div>

							<div class="col-xs-12">
								<div class="form-group label-floating">
									<label class="control-label">CELULAR </label>
									<input pattern="[0-9-]{1,30}" class="form-control" type="text" name="celu-reg" required="" maxlength="30">
								</div>
							</div>

							<div class="col-xs-6"> 
								<div class="form-group label-floating">
									<label class="control-label">
										CORREO
									</label>
									<input  class="form-control" type="email" name="email-reg" required="" maxlength="50">
								</div>
							</div>
							<br>
							<!-- <div class="col-xs-12">
								<div class="form-group label-floating">
									<label class="control-label">FECHA REGISTRO </label>
									<input pattern="[0-9-]{1,30}" class="form-control" type="date" name="fecha-reg" required="" maxlength="30">
								</div>
							</div> -->

							<div class="col-xs-12 col-sm-6">
								<label class="control-label">
									Estado
								</label>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsEstado" id="optionsRadios1" value="1" checked="" ><i class="zmdi zmdi-lock-open"></i> &nbsp; Activo
									</label>
								</div>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsEstado" id="optionsRadios2" value="2"><i class="zmdi zmdi-lock"></i> &nbsp; Inactivo
									</label>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6">
								<label class="control-label">
									TIENE DISCAPACIDAD
								</label>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsDiscapacidad" id="optionsRadios1" value="SI"><i class = "zmdi zmdi-flower-alt mdc-text-red" > </i> &nbsp; SI
									</label>
								</div>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsDiscapacidad" id="optionsRadios2" value="NO"><i class = "zmdi zmdi-flower-alt mdc-text-green"> </i> &nbsp; NO
									</label>
								</div>
							</div>

							
						</div>
					</div>
				</fieldset>	
				<fieldset>
					<legend>
						<i class="zmdi zmdi-account-box"></i> &nbsp; INFORMACION SI TIENE ALGUNA DISCAPACIDAD
					</legend>
					<div class="container-fluid">
						  <div class="row">
						     <div class="col-xs-12">
						     	<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT * from discapacidad");
								?>
								<select name="asTipofamilia" class="form-control" >
									<option>Seleccionar Discapacidad</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[2].'</option>';
									}?>
								</select>
							</div>

                            
						     </div>
						   </div>
					    </div>

				</fieldset>
				
				 <fieldset>

					<legend><i class="zmdi zmdi-key"></i> &nbsp; Datos del Representante</legend>
					<section>
						<h1>Busqueda en tiempo real</h1>
						<div>
							<label for="caja_busqueda">Buscar:</label>
							<input type="text" name="caja_busqueda" id="caja_busqueda" value=""></input>
						</div>
						<div id="datos">

						</div>

					</section>
				</fieldset>
					<br>
				

				<!-- discapacidad -->	
					

				<br>
				<!-- <fieldset>
					<legend>
						<i class="zmdi zmdi-account-box"></i> &nbsp; INFORMACION DOMICILIARIA
					</legend>
					<div class="panel-body">
						<div class="row">
							
							<div class="col-xs-12 col-sm-6">
								<div class="form-group label-floating">
									<label class="control-label">CALLE PRINCIPAL</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;" name="CPrinci-reg" required="" maxlength="30">
								</div>
							</div>

							<div class="col-xs-12 col-sm-6">
								<div class="form-group label-floating">
									<label class="control-label">CALLE SECUNDARIA</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;" name="CSecu-reg" required="" maxlength="30">
								</div>
							</div>
							<div class="col-xs-12">
								<div class="form-group label-floating">
									<label class="control-label">NUMERO DE CASA </label>
									<input pattern="[0-9-]{1,30}" class="form-control" type="text" name="numcasa-reg" required="" maxlength="30">
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="form-group label-floating">
									<label class="control-label">SECTOR</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;" name="sector-reg" required="" maxlength="30">
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="form-group label-floating">
									<label class="control-label">REFERENCIA</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" style="text-transform: uppercase;" name="refer-reg" required="" maxlength="30">
								</div>
							</div>


							
						</div>
					</div>						
				</fieldset> -->
				<fieldset>
					<legend>
						<i class="zmdi zmdi-account-box"></i> &nbsp; INFORMACION ESTUDIANTIL
					</legend>
					<div class="container-fluid">
						<div class="row">					
							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT * from modalidad");

								?>
								<select name="asmodalidad" class="form-control" >
									<option>Seleccionar Modalidad</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[2].'</option>';

									}?>
								</select>
							</div>
							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT *from jornadas");

								?>
								<select name="asjornada" class="form-control" >
									<option>Seleccione Jornada</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[0]." ".$r[2].'</option>';

									}?>
								</select>
							</div>

							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT *from beca");

								?>
								<select name="asbeca" class="form-control" >
									<option>Seleccione Beca</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[0]."".$r[2].'</option>';

									}?>
								</select>
							</div>

							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT *from tipocolegio");

								?>
								<select name="ascolegio" class="form-control" >
									<option>Seleccione Tipo Colegio</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[0]."".$r[2].'</option>';

									}?>
								</select>
							</div>

							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT *from tipobachillerato");

								?>
								<select name="asbachillerato" class="form-control" >
									<option>Seleccione Bachillerato</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[0]."".$r[2].'</option>';

									}?>
								</select>
							</div>

							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT *from nivelestudio");

								?>
								<select name="asnivel" class="form-control" >
									<option>Seleccionar Nivel Estudio </option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[0]."".$r[2].'</option>';

									}?>
								</select>
							</div>
							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT * from sectoreconomico");

								?>
								<select name="aseconomico" class="form-control" >
									<option>Seleccionar Sector Economico</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[0]."".$r[2].'</option>';

									}?>
								</select>
							</div>
						</div>
					</div>
					
				</fieldset>
				
				<fieldset>
					<legend>
						<i class="zmdi zmdi-account-box"></i> &nbsp; INFORMACION
					</legend>
					<div class="container-fluid">
						<div class="row">					
							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT *from etnia WHERE et_estado!='0'");

								?>
								<select name="asetnia" class="form-control" >
									<option>Seleccionar la Etnia</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[2].'</option>';

									}?>
								</select>
							</div>
							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT *from estadocivil");

								?>
								<select name="ascivil" class="form-control" >
									<option>Seleccione Estado Civil</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[0]." ".$r[2].'</option>';

									}?>
								</select>
							</div>

							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT *from tiposangre");

								?>
								<select name="asSangre" class="form-control" >
									<option>Seleccione su Tipo Sangre</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[0]."".$r[2].'</option>';

									}?>
								</select>
							</div>

							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT *from nacionalidad");

								?>
								<select name="asNacion" class="form-control" >
									<option>Seleccione su Nacionalidad</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[0]."".$r[2].'</option>';

									}?>
								</select>
							</div>

							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT *from provincia");

								?>
								<select name="asProvinc" class="form-control" >
									<option>Seleccione Provincia</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[0]."".$r[2].'</option>';

									}?>
								</select>
							</div>

							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT *from canton");

								?>
								<select name="asCanton" class="form-control" >
									<option>Seleccionar Ciudad</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[0]."".$r[2].'</option>';

									}?>
								</select>
							</div>



						</div>
					</div>
				</fieldset>

				<p class="text-center" style="margin-top: 20px ">
					<button type="submit" class="btn btn-info btn-raised btn-sm"><i class="zmdi zmdi-floppy"></i> GUARDAR
					</button>
				</p>
				<div class="RespuestaAjax"></div>
			</form>									
		</div>
	</div>
</div>