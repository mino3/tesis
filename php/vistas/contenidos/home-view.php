<?php 
	if ($_SESSION['tipo_se']!="Administrador") {
		echo $lc->forzar_cierre_session_controlador();
		//echo $lc->redireccionar_usuario_controlador($_SESSION['tipo_se']);
	}
 ?>

<div class="container-fluid">
	<div class="page-header">
		<h1 class="text-titles">Sistemas <small>Registros</small></h1>
	</div>
</div>
<div class="full-box text-center" style="padding: 30px 10px;">
	<?php 
 		require "./controladores/administradorControlador.php";
 		$InsAdmin=new administradorControlador();
 			//conteo
 			$Contadmin=$InsAdmin->datos_administrador_controlador("Conteo",0);
	 ?>
	<article class="full-box tile">
		<div class="full-box tile-title text-center text-titles text-uppercase">
			Administradores
		</div>
		<div class="full-box tile-icon text-center">
			<i class="zmdi zmdi-account"></i>
		</div>
		<div class="full-box tile-number text-titles">
			<p class="full-box"><?php echo $Contadmin->rowCount(); ?></p>
			<small>Registrados</small>
		</div>
	</article>
	<?php 
 		require "./controladores/idiomaControlador.php";
 		$InsIdio=new idiomaControlador();
 			//conteo
 			$ContaIdio=$InsIdio->datos_idioma_controlador("Conteo",0);
	 ?>
	 <article class="full-box tile">
		<div class="full-box tile-title text-center text-titles text-uppercase">
			Idioma
		</div>
		<div class="full-box tile-icon text-center">
			<i class = "zmdi zmdi-language-css3"> </i>
		</div>
		<div class="full-box tile-number text-titles">
			<p class="full-box"><?php echo $ContaIdio->rowCount(); ?></p>
			<small>Registrados</small>
		</div>
	</article>
	<?php 
 		require "./controladores/etniaControlador.php";
 		$InsEtn=new etniaControlador();
 			//conteo
 			$ContaEt=$InsEtn->datos_etnia_controlador("Conteo",0);
	 ?>
	 <article class="full-box tile">
		<div class="full-box tile-title text-center text-titles text-uppercase">
			Etnia
		</div>
		<div class="full-box tile-icon text-center">
			<i class = "zmdi zmdi-male-female"> </i>
		</div>
		<div class="full-box tile-number text-titles">
			<p class="full-box"><?php echo $ContaEt->rowCount(); ?></p>
			<small>Registrados</small>
		</div>
	</article>
		<?php 
 		require "./controladores/estadocivilControlador.php";
 		$InsEstCi=new estadocivilControlador();
 			//conteo
 			$ContaEstC=$InsEstCi->datos_estadocivil_controlador("Conteo",0);
	 ?>
	 <article class="full-box tile">
		<div class="full-box tile-title text-center text-titles text-uppercase">
			Estado Civil
		</div>
		<div class="full-box tile-icon text-center">
			<i class = "zmdi zmdi-male-female"> </i>
		</div>
		<div class="full-box tile-number text-titles">
			<p class="full-box"><?php echo $ContaEstC->rowCount(); ?></p>
			<small>Registrados</small>
		</div>
	</article>
	<?php 
 		require "./controladores/tiposangreControlador.php";
 		$InsTiSan=new tiposangreControlador();
 			//conteo
 			$ContaTipo=$InsTiSan->datos_tiposangre_controlador("Conteo",0);
	 ?>
	 <article class="full-box tile">
		<div class="full-box tile-title text-center text-titles text-uppercase">
			Tipo sangre
		</div>
		<div class="full-box tile-icon text-center">
			<i class = "zmdi zmdi-grain"> </i>
		</div>
		<div class="full-box tile-number text-titles">
			<p class="full-box"><?php echo $ContaTipo->rowCount(); ?></p>
			<small>Registrados</small>
		</div>
	</article>
	<?php 
 		require "./controladores/nacionalidadControlador.php";
 		$InsNac=new nacionalidadControlador();
 			//conteo
 			$ContaNac=$InsNac->datos_nacionalidad_controlador("Conteo",0);
	 ?>
	 <article class="full-box tile">
		<div class="full-box tile-title text-center text-titles text-uppercase">
			Nacionalidad
		</div>
		<div class="full-box tile-icon text-center">
			<i class = "zmdi zmdi-pin"> </i>
		</div>
		<div class="full-box tile-number text-titles">
			<p class="full-box"><?php echo $ContaNac->rowCount(); ?></p>
			<small>Registrados</small>
		</div>
	</article>
	<?php 
 		require "./controladores/cantonControlador.php";
 		$InsCan=new cantonControlador();
 			//conteo
 			$ContaCan=$InsCan->datos_canton_controlador("Conteo",0);
	 ?>
	 <article class="full-box tile">
		<div class="full-box tile-title text-center text-titles text-uppercase">
			Canton
		</div>
		<div class="full-box tile-icon text-center">
			<i class = "zmdi zmdi-ticket-star"> </i>
		</div>
		<div class="full-box tile-number text-titles">
			<p class="full-box"><?php echo $ContaCan->rowCount(); ?></p>
			<small>Registrados</small>
		</div>
	</article>
	<?php 
 		require "./controladores/provinciaControlador.php";
 		$InsProv=new provinciaControlador();
 			//conteo
 			$ContaProv=$InsProv->datos_provincia_controlador("Conteo",0);
	 ?>
	 <article class="full-box tile">
		<div class="full-box tile-title text-center text-titles text-uppercase">
			Provincia
		</div>
		<div class="full-box tile-icon text-center">
			<i class = "zmdi zmdi-pin-drop"> </i>
		</div>
		<div class="full-box tile-number text-titles">
			<p class="full-box"><?php echo $ContaProv->rowCount(); ?></p>
			<small>Registrados</small>
		</div>
	</article>



	<!-- <article class="full-box tile">
		<div class="full-box tile-title text-center text-titles text-uppercase">
			Teacher
		</div>
		<div class="full-box tile-icon text-center">
			<i class="zmdi zmdi-male-alt"></i>
		</div>
		<div class="full-box tile-number text-titles">
			<p class="full-box">10</p>
			<small>Registrados</small>
		</div>
	</article>
	<article class="full-box tile">
		<div class="full-box tile-title text-center text-titles text-uppercase">
			Student
		</div>
		<div class="full-box tile-icon text-center">
			<i class="zmdi zmdi-face"></i>
		</div>
		<div class="full-box tile-number text-titles">
			<p class="full-box">70</p>
			<small>Registrados</small>
		</div>
	</article>
	<article class="full-box tile">
		<div class="full-box tile-title text-center text-titles text-uppercase">
			Representative
		</div>
		<div class="full-box tile-icon text-center">
			<i class="zmdi zmdi-male-female"></i>
		</div>
		<div class="full-box tile-number text-titles">
			<p class="full-box">70</p>
			<small>Registrados</small>
		</div>
	</article> -->
</div>
<!-- <div class="container-fluid">
	<div class="page-header">
		<h1 class="text-titles">System <small>TimeLine</small></h1>
	</div>
	<section id="cd-timeline" class="cd-container">
		<div class="cd-timeline-block">
			<div class="cd-timeline-img">
				<img src="<?php echo SERVERURL;?>vistas/assets/img/avatar.jpg" alt="user-picture">
			</div>
			<div class="cd-timeline-content">
				<h4 class="text-center text-titles">1 - Name (Admin)</h4>
				<p class="text-center">
					<i class="zmdi zmdi-timer zmdi-hc-fw"></i> Start: <em>7:00 AM</em> &nbsp;&nbsp;&nbsp; 
					<i class="zmdi zmdi-time zmdi-hc-fw"></i> End: <em>7:17 AM</em>
				</p>
				<span class="cd-date"><i class="zmdi zmdi-calendar-note zmdi-hc-fw"></i> 07/07/2016</span>
			</div>
		</div>  
		<div class="cd-timeline-block">
			<div class="cd-timeline-img">
				<img src="<?php echo SERVERURL;?>vistas/assets/img/avatar.jpg" alt="user-picture">
			</div>
			<div class="cd-timeline-content">
				<h4 class="text-center text-titles">2 - Name (Teacher)</h4>
				<p class="text-center">
					<i class="zmdi zmdi-timer zmdi-hc-fw"></i> Start: <em>7:00 AM</em> &nbsp;&nbsp;&nbsp; 
					<i class="zmdi zmdi-time zmdi-hc-fw"></i> End: <em>7:17 AM</em>
				</p>
				<span class="cd-date"><i class="zmdi zmdi-calendar-note zmdi-hc-fw"></i> 07/07/2016</span>
			</div>
		</div>
		<div class="cd-timeline-block">
			<div class="cd-timeline-img">
				<img src="<?php echo SERVERURL;?>vistas/assets/img/avatar.jpg" alt="user-picture">
			</div>
			<div class="cd-timeline-content">
				<h4 class="text-center text-titles">3 - Name (Student)</h4>
				<p class="text-center">
					<i class="zmdi zmdi-timer zmdi-hc-fw"></i> Start: <em>7:00 AM</em> &nbsp;&nbsp;&nbsp; 
					<i class="zmdi zmdi-time zmdi-hc-fw"></i> End: <em>7:17 AM</em>
				</p>
				<span class="cd-date"><i class="zmdi zmdi-calendar-note zmdi-hc-fw"></i> 07/07/2016</span>
			</div>
		</div>
		<div class="cd-timeline-block">
			<div class="cd-timeline-img">
				<img src="<?php echo SERVERURL;?>vistas/assets/img/avatar.jpg" alt="user-picture">
			</div>
			<div class="cd-timeline-content">
				<h4 class="text-center text-titles">4 - Name (Personal Ad.)</h4>
				<p class="text-center">
					<i class="zmdi zmdi-timer zmdi-hc-fw"></i> Start: <em>7:00 AM</em> &nbsp;&nbsp;&nbsp; 
					<i class="zmdi zmdi-time zmdi-hc-fw"></i> End: <em>7:17 AM</em>
				</p>
				<span class="cd-date"><i class="zmdi zmdi-calendar-note zmdi-hc-fw"></i> 07/07/2016</span>
			</div>
		</div>   
	</section>
</div> -->