<div class="container-fluid">
	<div class="page-header">
		<h1 class="text-titles"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Users <small>Sector Economico</small></h1>
	</div>
	<p class="lead">ACTUALIZAR SECTOR ECONOMICO</p>
</div>
<div class="container-fluid">
 	<?php 
 	// viene del htacces
 		$datos = explode("/",$_GET['views']);

 		//mydata/admin/codcuenta
 		//administrador
 		if ($datos[1]=="sectoreconomico") {
 			
	if ($_SESSION['tipo_se']!="Administrador") {
		echo $lc->forzar_cierre_session_controlador();
	}
	require_once "./controladores/sectorEconomicoControlador.php";
	$classsectorEconomico= new sectorEconomicoControlador();
	//hace el select para seleccionar los del administrador
 		$filesA=$classsectorEconomico->datos_sectorEconomico_controlador("Unico",$datos[2]);
 		if ($filesA->rowCount()==1) {
 			//campos tine todos los datos del administrador
 			$campos=$filesA->fetch();
 			
 				if ($_SESSION['privilegio_se']<1 || $_SESSION['privilegio_se']>2) {
 					echo $lc->forzar_cierre_session_controlador();
 				}
 			
?>
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="zmdi zmdi-refresh"></i> &nbsp; ACTUALIZAR TIPO BACHILLERATO</h3>
		</div>
		<div class="panel-body">
			<form action="<?php echo SERVERURL;?>ajax/sectoreconomicoAjax.php" method="POST" data-form="update" class="FormularioAjax" autocomplete="off" enctype="multipart/form-data"> 	
				<input type="hidden" name="codigo-up" value="<?php echo $datos[2]; ?>">
				<fieldset>
					<legend><i class="zmdi zmdi-account-box"></i>&nbsp;INFORMACION SECTOR ECONOMICO</legend>
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-12 col-sm-6">
									<div class="form-group label-floating">
										<label class="control-label">NOMBRE</label>
										<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" name="nombre-up" value="<?php echo $campos['sec_nombre'] ?>" required="" maxlength="30">
									</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="form-group label-floating">
										<label class="control-label">DESCRIPCION</label>
										<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" name="desc-up" value="<?php echo $campos['sec_descripcion'] ?>" required="" maxlength="30">
									</div>
								</div>							
		<?php if ($_SESSION['tipo_se']=="Administrador" && $_SESSION['privilegio_se']==1):?>
								<div class="col-xs-12 col-sm-6">
									<label class="control-label">
										Estado
									</label>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsEstado-up" id="optionsRadios1" value="1" <?php if ($campos['sec_estado']=="1") {echo'checked=""';} ?>><i class="zmdi zmdi-start"></i> &nbsp; Activo
									</label>
								</div>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsEstado-up" id="optionsRadios2" value="0" <?php if ($campos['sec_estado']=="0") {echo'checked=""';} ?>><i class="zmdi zmdi-start"></i> &nbsp; Inactivo
									</label>
								</div>
							</div>
						<?php endif; ?>
																													
							</div>
						</div>
				</fieldset>
			
				<p class="text-center" style="margin-top: 20px ">
					<button type="submit" class="btn btn-info btn-raised btn-sm"><i class="zmdi zmdi-refresh"></i> ACTUALIZAR
					</button>
				</p>
				<div class="RespuestaAjax"></div>
			</form>									
		</div>
	</div>
	<?php }else{ ?>
		<h4>Lo sentimos</h4>
		<p>No podemos mostrar la informacion solicitada 1</p>
<?php }
 		//usuario normal	
 		}elseif ($datos[1]=="user") {
 			echo "usuario";
 		
 		//error	
 		}else{
?>
<h4>Lo sentimos</h4>
<p>No podemos mostrar la informacion solicitada 2</p>
<?php
        //termina 			
 		}
?>
</div>