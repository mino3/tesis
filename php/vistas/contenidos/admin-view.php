<?php 
	if ($_SESSION['tipo_se']!="Administrador") {
		echo $lc->forzar_cierre_session_controlador();
	}
 ?>

<div class="container-fluid">
	<div class="page-header">
		<h1 class="text-titles"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Users <small>Admin</small></h1>
	</div>
	<p class="lead">ADMINISTRADORES</p>
</div>
<div class="container-fluid">
	<ul class="breadcrumb breadcrumb-tabs">
		<!-- BOTON NUEVO -->
		<li>
			<a href="<?php echo SERVERURL;?>admin/" class="btn btn-info">
				<i class="zmdi zmdi-plus"></i> &nbsp; NUEVO ADMINISTRADOR
			</a>
		</li>
		<!-- BOTON LISTAR -->
		<li>
			<a href="<?php echo SERVERURL;?>adminlist/" class="btn btn-success">
				<i class="zmdi zmdi-format-list-bulleted"></i> &nbsp; LISTA ADMINISTRADOR
			</a>
		</li>
		<!-- BOTON BUSCAR -->
		<li>
			<a href="<?php echo SERVERURL;?>adminserach/" class="btn btn-primary">
				<i class="zmdi zmdi-search"></i> &nbsp; BUSCAR ADMINISTRADOR
			</a>
		</li>
	</ul>
</div>

<!-- panel nuevo administrador
 -->
 <div class="container-fluid">
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="zmdi zmdi-plus"></i> &nbsp; NUEVO ADMINISTRADOR</h3>
		</div>
		<div class="panel-body">
			<form action="<?php echo SERVERURL;?>ajax/administradorAjax.php" method="POST" data-form="save" class="FormularioAjax" autocomplete="off" enctype="multipart/form-data"> 	
				<fieldset>
					<legend><i class="zmdi zmdi-account-box"></i>&nbsp;INFORMACION PERSONAL</legend>
						<div class="container-fluid">
							<div class="row">

								<div class="col-xs-12">
									<div class="form-group label-floating">
										<label class="control-label">DNI </label>
										<input pattern="[0-9-]{1,30}" class="form-control" type="text" name="dni-reg" required="" maxlength="30">
									</div>
								</div>

								<div class="col-xs-12 col-sm-6">
									<div class="form-group label-floating">
										<label class="control-label">Nombres</label>
										<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" name="nombre-reg" required="" maxlength="30">
									</div>
								</div>

								<div class="col-xs-12 col-sm-6">
									<div class="form-group label-floating">
										<label class="control-label">Apellidos</label>
										<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" name="ape-reg" required="" maxlength="30">
									</div>
								</div>

								<div class="col-xs-12 col-sm-6">
									<div class="form-group label-floating">
										<label class="control-label">Telefono </label>
										<input pattern="[0-9-]{1,30}" class="form-control" type="tel" name="tel-reg" required="" maxlength="30">
									</div>
								</div>

								<div class="col-xs-12">
									<div class="form-group label-floating">
										<label class="control-label">Direccion</label>
											<textarea name="direc-reg" class="form-control" rows="2" 			maxlength="100">
											</textarea>										
									</div>									
								</div>								
							</div>
						</div>
				</fieldset>
				<br>
				<fieldset>
					<legend>
						<i class="zmdi zmdi-key"></i> &nbsp DATOS CUENTA
					</legend>
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-12"> 
								<div class="form-group label-floating">
									<label class="control-label">
										Nombre de Usuario
									</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" name="usuario-reg" required="" maxlength="15">
								</div>
							</div>
							<div class="col-xs-6"> 
								<div class="form-group label-floating">
									<label class="control-label">
										Contraseña
									</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="password" name="password1-reg" required="" maxlength="15">
								</div>
							</div>
							<div class="col-xs-6"> 
								<div class="form-group label-floating">
									<label class="control-label">
										Repita Contraseña
									</label>
									<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="password" name="password2-reg" required="" maxlength="15">
								</div>
							</div>
							<div class="col-xs-6"> 
								<div class="form-group label-floating">
									<label class="control-label">
										Email
									</label>
									<input  class="form-control" type="email" name="email-reg" required="" maxlength="50">
								</div>
							</div>

							<div class="col-xs-6"> 
								<div class="form-group label-floating">
									<label class="control-label">
										Genero
									</label>
									<div class="radio radio-primary">
										<label>
											<input type="radio" name="optionsGenero" id="optionsRadios1" value="Masculino" >
											<i class="zmdi zmdi-male-alt"></i> &nbsp; Masculino
										</label>

									</div>
									<div class="radio radio-primary">
										<label>
											<input type="radio" name="optionsGenero" id="optionsRadios2" value="Femenino" >
											<i class="zmdi zmdi-female"></i> &nbsp; Femenino
										</label>

									</div>

								</div>
							</div>

						</div>
					</div>
				</fieldset>
				<br>
				<fieldset>
					<legend>
						<i class="zmdi zmdi-star"></i> &nbsp; Nivel de privilegios
					</legend>
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-12 col-sm-6">
								<p class="text-left">
									<div class="label label-sucess">
										Nivel1
									</div>Control total del sistema 

								</p>
								<p class="text-left">
									<div class="label label-primary">
										Nivel 2
									</div>Permiso registro y actualizacion

								</p>
								<p class="text-left">
									<div class="label label-info">
										Nivel 3
									</div>Permiso registro

								</p>
							</div>
							<div class="col-xs-12 col-sm-6">
					<?php if($_SESSION['privilegio_se']==1):  ?>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsPrivilegio" id="optionsRadios1" value="<?php echo $lc->encryption(1);?>"><i class="zmdi zmdi-star"></i> &nbsp; Nivel 1
									</label>
								</div>
					<?php 
							endif;
				// valida que el nivel que ingreso ingrese del mismo nivel
					if($_SESSION['privilegio_se']<=2): 
					 ?>			
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsPrivilegio" id="optionsRadios2" value="<?php echo $lc->encryption(2);?>"><i class="zmdi zmdi-star"></i> &nbsp; Nivel 2
									</label>
								</div>
					<?php 
						endif;
					 ?>			
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsPrivilegio" id="optionsRadios3" value="<?php echo $lc->encryption(3);?>"><i class="zmdi zmdi-star" ></i> &nbsp; Nivel 3
									</label>
								</div>
							</div>
						</div>
					</div>											
				</fieldset>
				<p class="text-center" style="margin-top: 20px ">
					<button type="submit" class="btn btn-info btn-raised btn-sm"><i class="zmdi zmdi-floppy"></i> GUARDAR
					</button>
				</p>
				<div class="RespuestaAjax"></div>
			</form>									
		</div>
	</div>
</div>