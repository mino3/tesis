<div class="container-fluid">
	<div class="page-header">
		<h1 class="text-titles"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Users <small>Tipo Familia</small></h1>
	</div>
	<p class="lead">ACTUALIZAR TIPO FAMILIA</p>
</div>

<!-- panel nuevo administrador
 -->
 <div class="container-fluid">
 	<?php 
 	// viene del htacces
 		$datos = explode("/",$_GET['views']);

 		//mydata/admin/codcuenta
 		//administrador
 		if ($datos[1]=="tipofamilia") {
 			
	if ($_SESSION['tipo_se']!="Administrador") {
		echo $lc->forzar_cierre_session_controlador();
	}
	require_once "./controladores/familiaControlador.php";
	$classfamilia= new familiaControlador();
	//hace el select para seleccionar los del administrador
 		$filesA=$classfamilia->datos_fam_controlador("Unico",$datos[2]);
 		if ($filesA->rowCount()==1) {
 			//campos tine todos los datos del administrador
 			$campos=$filesA->fetch();
 			
 				if ($_SESSION['privilegio_se']<1 || $_SESSION['privilegio_se']>2) {
 					echo $lc->forzar_cierre_session_controlador();
 				}
 			
?>
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="zmdi zmdi-refresh"></i> &nbsp; ACTUALIZAR TIPO FAMILIA</h3>
		</div>
		<div class="panel-body">
			<form action="<?php echo SERVERURL;?>ajax/direccionAjax.php" method="POST" data-form="update" class="FormularioAjax" autocomplete="off" enctype="multipart/form-data"> 	<input type="hidden" name="codigo-up" value="<?php echo $datos[2]; ?>">
				<fieldset>
					<legend><i class="zmdi zmdi-account-box"></i>&nbsp;INFORMACION DEL TIPO FAMILIA</legend>
						<div class="container-fluid">
							<div class="row">
								<div class="panel-body">
						<div class="row">
							<div class="form-group label-floating">
								<?php
								$con=mysqli_connect(SERVER,USER,PASS,DB);
								$res=$con->query("SELECT * from tipofamilia");

								?>
								<select name="asTipofamilia-up" class="form-control" required="">
									<option>Seleccionar Tipo Familia</option>
									<?php
									while ($r=$res->fetch_row()){
										echo '<option value="'.$r[0].'">'.$r[2].'</option>';
									}?>
								</select>
							</div>

						</div>
					</div>
								
		<?php if ($_SESSION['tipo_se']=="Administrador" && $_SESSION['privilegio_se']==1):?>
								<div class="col-xs-12 col-sm-6">
									<label class="control-label">
										Estado
									</label>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsEstado-up" id="optionsRadios1" value="1" <?php if ($campos['fa_estado']=="1") {echo'checked=""';} ?>><i class="zmdi zmdi-start"></i> &nbsp; Activo
									</label>
								</div>
								<div class="radio radio-primary">
									<label>
										<input type="radio" name="optionsEstado-up" id="optionsRadios2" value="0" <?php if ($campos['fa_estado']=="0") {echo'checked=""';} ?>><i class="zmdi zmdi-start"></i> &nbsp; Inactivo
									</label>
								</div>
							</div>
						<?php endif; ?>
																													
							</div>
						</div>
				</fieldset>
			
				<p class="text-center" style="margin-top: 20px ">
					<button type="submit" class="btn btn-info btn-raised btn-sm"><i class="zmdi zmdi-refresh"></i> ACTUALIZAR
					</button>
				</p>
				<div class="RespuestaAjax"></div>
			</form>									
		</div>
	</div>
	<?php }else{ ?>
		<h4>Lo sentimos</h4>
		<p>No podemos mostrar la informacion solicitada 1</p>
<?php }
 		//usuario normal	
 		}elseif ($datos[1]=="user") {
 			echo "usuario";
 		
 		//error	
 		}else{
?>
<h4>Lo sentimos</h4>
<p>No podemos mostrar la informacion solicitada 2</p>
<?php
        //termina 			
 		}
?>
</div>