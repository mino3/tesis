 <div class="container-fluid">
	<div class="page-header">
		<h1 class="text-titles"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Users <small>MIS DATOS</small></h1>
	</div>
	<p class="lead">MIS DATOS</p>
</div>

 <div class="container-fluid">
 	<?php 
 	// viene del htacces
 		$datos = explode("/",$_GET['views']);

 		//mydata/admin/codcuenta
 		//administrador
 		if ($datos[1]=="admin") {
 			
	if ($_SESSION['tipo_se']!="Administrador") {
		echo $lc->forzar_cierre_session_controlador();
	}
	require_once "./controladores/administradorControlador.php";
	$classAdmin= new administradorControlador();
	//hace el select para seleccionar los del administrador
 		$filesA=$classAdmin->datos_administrador_controlador("Unico",$datos[2]);
 		if ($filesA->rowCount()==1) {
 			//campos tine todos los datos del administrador
 			$campos=$filesA->fetch();
 			if ($campos['CuentaCodigo']!=$_SESSION['codigo_cuenta_se']) {
 				if ($_SESSION['privilegio_se']<1 || $_SESSION['privilegio_se']>2) {
 					echo $lc->forzar_cierre_session_controlador();
 				}
 			}
?>
   <div class="panel panel-success">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="zmdi zmdi-plus"></i> &nbsp; MIS DATOS</h3>
		</div>
		<div class="panel-body">
			<form action="<?php echo SERVERURL;?>ajax/administradorAjax.php" method="POST" data-form="update" class="FormularioAjax" autocomplete="off" enctype="multipart/form-data"> 	
				<input type="hidden" name="cuenta-up" value="<?php echo $datos[2]; ?>">
				<fieldset>
					<legend><i class="zmdi zmdi-account-box"></i>&nbsp;INFORMACION PERSONAL</legend>
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group label-floating">
										<label class="control-label">DNI </label>
										<input pattern="[0-9-]{1,30}" class="form-control" type="text" name="dni-up" value="<?php echo $campos['AdminDNI'];?>" required="" maxlength="30">
									</div>
								</div>

								<div class="col-xs-12 col-sm-6">
									<div class="form-group label-floating">
										<label class="control-label">Nombres</label>
										<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" name="nombre-up" value="<?php echo $campos['AdminNombre'];?>" required="" maxlength="30">
									</div>
								</div>

								<div class="col-xs-12 col-sm-6">
									<div class="form-group label-floating">
										<label class="control-label">Apellidos</label>
										<input pattern="[a-Za-Záéíóú´´ÁÉÍÓÚñÑ ]{1,30}" class="form-control" type="text" name="ape-up" value="<?php echo $campos['AdminApellido'];?>"required="" maxlength="30">
									</div>
								</div>

								<div class="col-xs-12 col-sm-6">
									<div class="form-group label-floating">
										<label class="control-label">Telefono </label>
										<input pattern="[0-9-]{1,30}" class="form-control" type="tel" name="tel-up" value="<?php echo $campos['AdminTelefono'];?>" required="" maxlength="30">
									</div>
								</div>

								<div class="col-xs-12">
									<div class="form-group label-floating">
										<label class="control-label">Direccion</label>
										<textarea name="direc-up"  class="form-control" rows="2" maxlength="100">
												<?php echo $campos['AdminDireccion'];?>
											</textarea>										
									</div>									
								</div>								
							</div>
						</div>
				</fieldset>
			
			<p class="text-center" style="margin-top: 20px; ">
					<button type="submit" class="btn btn-info btn-raised btn-sm"><i class="zmdi zmdi-refresh"></i> UPDATE
					</button>
				</p>
				<div class="RespuestaAjax"></div>
				</form>
				
	   </div>
   </div>
	

<?php }else{ ?>
		<h4>Lo sentimos</h4>
		<p>No podemos mostrar la informacion solicitada</p>
<?php }
 		//usuario normal	
 		}elseif ($datos[1]=="user") {
 			echo "usuario";
 		
 		//error	
 		}else{
?>
<h4>Lo sentimos</h4>
<p>No podemos mostrar la informacion solicitada</p>
<?php
        //termina 			
 		}
?>
	 	
</div>			