<section class="full-box cover dashboard-sideBar">
	<div class="full-box dashboard-sideBar-bg btn-menu-dashboard"></div>
	<div class="full-box dashboard-sideBar-ct">
		<!--SideBar Title -->
		<div class="full-box text-uppercase text-center text-titles dashboard-sideBar-title">
			<?php echo COMPANY ?> <i class="zmdi zmdi-close btn-menu-dashboard visible-xs"></i>
		</div>
		<!-- SideBar User info -->
		<div class="full-box dashboard-sideBar-UserInfo">
			<figure class="full-box">
				<img src="<?php echo SERVERURL;?>vistas/assets/img/<?php echo $_SESSION['foto_se'] ?>" alt="UserIcon">
				<figcaption class="text-center text-titles"><?php echo $_SESSION['nombre_se'] ." ".$_SESSION['apellido_se']; ?></figcaption>
			</figure>
			<?php 
				if ($_SESSION['tipo_se']=="Administrador") {
					# code...
					//inica sesion un administrador
					$tipo="admin";
				}else{
					$tipo="user";
				}
			 ?>
			<ul class="full-box list-unstyled text-center">
				<li>
					<a href="<?php echo SERVERURL;?>mydata/<?php echo $tipo."/". $lc->encryption($_SESSION['codigo_cuenta_se']); ?>/" title="Mis Datos">
						<i class="zmdi zmdi-account-circle"></i>
					</a>
				</li>
				<li>
					<a href="<?php echo SERVERURL;?>myaccount/<?php echo $tipo."/". $lc->encryption($_SESSION['codigo_cuenta_se']); ?>/" title="Mi Cuenta">
						<i class="zmdi zmdi-settings"></i>
					</a>
				</li>
				<!-- boton salir del sistema -->
				<li>
					<a href="<?php echo $lc->encryption($_SESSION['token_se']); ?>" title="Salir del sistema" class="btn-exit-system">
						<i class="zmdi zmdi-power"></i>
					</a>
				</li>
			</ul>
		</div>
		<!-- SideBar Menu -->
		<ul class="list-unstyled full-box dashboard-sideBar-Menu">
			<?php 
				if ($_SESSION['tipo_se']=="Administrador") :
			 ?>
			<li>
				<a href="<?php echo SERVERURL;?>home/">
					<i class="zmdi zmdi-view-dashboard zmdi-hc-fw"></i> Escritorio
				</a>
			</li>
			<li>
				<a href="#!" class="btn-sideBar-SubMenu">
					<i class="zmdi zmdi-case zmdi-hc-fw"></i> Administracion <i class="zmdi zmdi-caret-down pull-right"></i>
				</a>
				<ul class="list-unstyled full-box">
					<li>	
						<a href="<?php echo SERVERURL;?>admin/"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Administradores</a>
					</li>
					<li>
					<a href="<?php echo SERVERURL;?>persona/"><i class = "zmdi zmdi-accounts-add"> </i>   Persona</a>
					</li>
					<li>
					<a href="<?php echo SERVERURL;?>familia/"><i class = "zmdi zmdi-accounts-add"> </i>Representantes</a>
					</li>
					<li>
					<a href="<?php echo SERVERURL;?>alumno/"><i class = "zmdi zmdi-accounts-add"> </i>Alumno</a>
					</li>
					<li>
					<a href="<?php echo SERVERURL;?>tipofamilia/"><i class = "zmdi zmdi-accounts-add"> </i>Tipo Familia</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="#!" class="btn-sideBar-SubMenu">
					<i class="zmdi zmdi-case zmdi-hc-fw"></i> Institucion <i class="zmdi zmdi-caret-down pull-right"></i>
				</a>
				<ul class="list-unstyled full-box">
					<li>	
						<a href="<?php echo SERVERURL;?>jornada/"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Jornada</a>
					</li>
					<li>	
						<a href="<?php echo SERVERURL;?>beca/"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Beca</a>
					</li>
					<li>	
						<a href="<?php echo SERVERURL;?>tipocolegio/"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Tipo Colegio</a>
					</li>
					<li>	
						<a href="<?php echo SERVERURL;?>tipobachillerato/"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Tipo Bachillerato</a>
					</li>
					<li>	
						<a href="<?php echo SERVERURL;?>sectoreconomico/"><i class="zmdi zmdi-account zmdi-hc-fw"></i>Sector Ecomico</a>
					</li>
					<li>	
						<a href="<?php echo SERVERURL;?>modalidad/"><i class="zmdi zmdi-account zmdi-hc-fw"></i>Modalidad</a>
					</li>
					<li>	
						<a href="<?php echo SERVERURL;?>nivelestudio/"><i class="zmdi zmdi-account zmdi-hc-fw"></i>Nivel Estudio</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="#!" class="btn-sideBar-SubMenu">
					<i class="zmdi zmdi-account-add zmdi-hc-fw"></i> Usuarios <i class="zmdi zmdi-caret-down pull-right"></i>
				</a>
				<ul class="list-unstyled full-box">

					<li>
					<a href="<?php echo SERVERURL;?>idioma/"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Idioma</a>
					</li>
					<li>
					<a href="<?php echo SERVERURL;?>discapacidad/"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Discapacidad</a>
					</li>
					
					<li>
					<a href="<?php echo SERVERURL;?>etnia/"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Etnia</a>
					</li>
					<li>
					<a href="<?php echo SERVERURL;?>estadocivil/"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Estado civil</a>
					</li>
					<li>
					<a href="<?php echo SERVERURL;?>tiposangre/"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Tipo Sangre</a>
					</li>
					<li>
					<a href="<?php echo SERVERURL;?>nacionalidad/"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Nacionalidad</a>
					</li>
					<li>
					<a href="<?php echo SERVERURL;?>canton/"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Canton</a>
					</li>
					<li>
					<a href="<?php echo SERVERURL;?>provincia/"><i class="zmdi zmdi-account zmdi-hc-fw"></i> Provincia</a>
					</li>
				</ul>
			</li>

		<?php 	endif; ?>
			<!-- <li>
				<a href="#!" class="btn-sideBar-SubMenu">
					<i class="zmdi zmdi-card zmdi-hc-fw"></i> Payments <i class="zmdi zmdi-caret-down pull-right"></i>
				</a>
				<ul class="list-unstyled full-box">
					<li>
						<a href="<?php echo SERVERURL;?>registro/"><i class="zmdi zmdi-money-box zmdi-hc-fw"></i> Registration</a>
					</li>
					<li>
						<a href="payments.html"><i class="zmdi zmdi-money zmdi-hc-fw"></i> Payments</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="#!" class="btn-sideBar-SubMenu">
					<i class="zmdi zmdi-shield-security zmdi-hc-fw"></i> Settings School <i class="zmdi zmdi-caret-down pull-right"></i>
				</a>
				<ul class="list-unstyled full-box">
					<li>
						<a href="school.html"><i class="zmdi zmdi-balance zmdi-hc-fw"></i> School Data</a>
					</li>
				</ul>
			</li> -->
		</ul>
	</div>
</section> 

